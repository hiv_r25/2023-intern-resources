``` r
library(rbenchmark)
library(microbenchmark)
library(Hmisc)
library(tidyverse)
```

# Introduction

In this seminar we will be going over

-   Some Rstudio overviews
-   General R
-   Tidyverse

Parts of notes adapted from work by Tyler Schappe and Tina Davenport

# General META information on R-studio

-   Snippets
-   Other shortcuts.
-   Commenting

See powerpoint.

# Tidyverse

Tidyverse is very intuitive with advantages, but being comfortable with
both tidyverse and “base R” has advantages:

-   Functions from many packages do not use tidyverse
-   Scripts that you may inherit from other programmers may not use
    tidyverse
-   Base R can be more flexible and can sometimes be faster
    computationally
-   You will have more tools to approach difficult problems in different
    way

Goal of tidyverse is to have a general philosophy/pipelines for working
with datasets.

## Tidy data

What is tidy data?

Tidy data is your friend. If possible get data in a tidy format (will
have to explain to investigator).

| ID  | Age Visit 1 | Age Visit 2 | BMI Visit 1 | BMI Visit 2 | Center |
|-----|-------------|-------------|-------------|-------------|--------|
| 1   | 40          | 44          | 20          | 27          | Duke   |
| 2   | 37          | 40          | 24          | 23          | UNC    |

Non Tidy data

Then Tidy data:

| ID  | Visit | BMI | Age | Center |
|-----|-------|-----|-----|--------|
| 1   | 1     | 20  | 40  | Duke   |
| 1   | 2     | 27  | 44  | Duke   |
| 2   | 1     | 24  | 37  | UNC    |
| 2   | 2     | 23  | 40  | UNC    |

Tidy data

Every observation is a unique row.

# Reading in non R datasets

-   **Hmisc** package

    -   STATA
    -   SPSS
    -   SAS xpt

-   **foreign** package

    -   SPSS
    -   STATA

-   **SASxport** package

    -   SAS xpt

-   **sas7bdat**

    -   SAS 7bdat

-   **haven** package

    -   SAS
    -   STATA
    -   SPSS

-   **readxl** package

    -   Excel
    -   If more than one sheet, specify, otherwise will just read the
        first one.

# Base R

R is open-source and a good chunk of it’s utility comes from R packages.
These are built by either the R team, Rstudio, or other
programmers/investigators (like you!).

We will start with some basic structures and the like

# Some data structures

## Vectors

``` r
a <-  c(4,5,6)

a
```

    ## [1] 4 5 6

``` r
b<-c(1:4)

b
```

    ## [1] 1 2 3 4

``` r
d<-c("a",1,3)

d
```

    ## [1] "a" "1" "3"

``` r
e<-c(TRUE,FALSE,T,F)

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# The length
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
length(a)
```

    ## [1] 3

``` r
length(b)
```

    ## [1] 4

``` r
length(d)
```

    ## [1] 3

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# The Class
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
class(a)
```

    ## [1] "numeric"

``` r
class(b)
```

    ## [1] "integer"

``` r
class(d)
```

    ## [1] "character"

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# If for some reason wanted all to be same line
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

class(a);class(b);class(d)
```

    ## [1] "numeric"

    ## [1] "integer"

    ## [1] "character"

Why did I not call that vector c?

## Matrices

``` r
A<-matrix(c(5,6,7,
            8,9,10),nrow=2,ncol=3)
A
```

    ##      [,1] [,2] [,3]
    ## [1,]    5    7    9
    ## [2,]    6    8   10

``` r
B<-matrix(c(5,6,7,
            8,9,10),nrow=2,ncol=3,byrow = T)
B
```

    ##      [,1] [,2] [,3]
    ## [1,]    5    6    7
    ## [2,]    8    9   10

``` r
# Some operations
A+B
```

    ##      [,1] [,2] [,3]
    ## [1,]   10   13   16
    ## [2,]   14   17   20

``` r
A-B
```

    ##      [,1] [,2] [,3]
    ## [1,]    0    1    2
    ## [2,]   -2   -1    0

``` r
t(A)
```

    ##      [,1] [,2]
    ## [1,]    5    6
    ## [2,]    7    8
    ## [3,]    9   10

``` r
t(B)
```

    ##      [,1] [,2]
    ## [1,]    5    8
    ## [2,]    6    9
    ## [3,]    7   10

``` r
t(A) %*% B
```

    ##      [,1] [,2] [,3]
    ## [1,]   73   84   95
    ## [2,]   99  114  129
    ## [3,]  125  144  163

``` r
crossprod(A,B)
```

    ##      [,1] [,2] [,3]
    ## [1,]   73   84   95
    ## [2,]   99  114  129
    ## [3,]  125  144  163

``` r
A %*% t(B)
```

    ##      [,1] [,2]
    ## [1,]  130  193
    ## [2,]  148  220

``` r
tcrossprod(A,B)
```

    ##      [,1] [,2]
    ## [1,]  130  193
    ## [2,]  148  220

``` r
kronecker(A ,B)
```

    ##      [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8] [,9]
    ## [1,]   25   30   35   35   42   49   45   54   63
    ## [2,]   40   45   50   56   63   70   72   81   90
    ## [3,]   30   36   42   40   48   56   50   60   70
    ## [4,]   48   54   60   64   72   80   80   90  100

``` r
A %x%B
```

    ##      [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8] [,9]
    ## [1,]   25   30   35   35   42   49   45   54   63
    ## [2,]   40   45   50   56   63   70   72   81   90
    ## [3,]   30   36   42   40   48   56   50   60   70
    ## [4,]   48   54   60   64   72   80   80   90  100

``` r
# Other ways to construct matrices 

cbind(c(5,6,7),
      c(8,9,10))
```

    ##      [,1] [,2]
    ## [1,]    5    8
    ## [2,]    6    9
    ## [3,]    7   10

``` r
rbind(c(5,6,7),
      c(8,9,10))
```

    ##      [,1] [,2] [,3]
    ## [1,]    5    6    7
    ## [2,]    8    9   10

## Lists

Store objects of different length and types

``` r
A.list<-list(AB=c(1:5),
             CB="Wow a list",
             DB=cbind(c(5,6,7),
                      c(8,9,10)))

A.list
```

    ## $AB
    ## [1] 1 2 3 4 5
    ## 
    ## $CB
    ## [1] "Wow a list"
    ## 
    ## $DB
    ##      [,1] [,2]
    ## [1,]    5    8
    ## [2,]    6    9
    ## [3,]    7   10

``` r
# Show one item
A.list[[1]];A.list[[2]];A.list[[3]]
```

    ## [1] 1 2 3 4 5

    ## [1] "Wow a list"

    ##      [,1] [,2]
    ## [1,]    5    8
    ## [2,]    6    9
    ## [3,]    7   10

``` r
# Call by name 

A.list[["CB"]]
```

    ## [1] "Wow a list"

## Dataframe

A mix of lists and matrices.

``` r
ADF<-data.frame(A=c(1:3),B=c("A","B","C"))
ADF
```

    ##   A B
    ## 1 1 A
    ## 2 2 B
    ## 3 3 C

``` r
class(ADF[,1])
```

    ## [1] "integer"

``` r
class(ADF[,2])
```

    ## [1] "character"

``` r
class(ADF[[2]])
```

    ## [1] "character"

# Quick note on Logical statements and some set operators

``` r
a <-  c(4,5,6)
b <- c(1:4)


b==a
```

    ## Warning in b == a: longer object length is not a multiple of shorter object
    ## length

    ## [1] FALSE FALSE FALSE  TRUE

``` r
b %in% a
```

    ## [1] FALSE FALSE FALSE  TRUE

``` r
which(b %in% a)
```

    ## [1] 4

``` r
any(b %in%a)
```

    ## [1] TRUE

``` r
all(b %in% a)
```

    ## [1] FALSE

``` r
setdiff(b,a)
```

    ## [1] 1 2 3

``` r
setdiff(a,b)
```

    ## [1] 5 6

``` r
intersect(a,b)
```

    ## [1] 4

``` r
union(a,b)
```

    ## [1] 4 5 6 1 2 3

## More examples

Logical statements in R can be tricky.

Create a toy example

``` r
a <- c(1,2,3)
b <- c(TRUE, NA, FALSE)
```

``` r
is.na(a | b)
```

    ## [1] FALSE FALSE FALSE

To see why this misses the NA, look closely at what logical statements R
is actually testing given the order of evaluation.

R first evaluates the statement in the parentheses, “a \| b”. Since no
condition is given, this implicitly tests whether each element is a
numeric or logical type, then apply the “AND” or “OR” element-wise. The
“OR” tests whether either one or the other element from each a and b is
numeric or logical, and “AND” tests whether both are.

``` r
a | b
```

    ## [1] TRUE TRUE TRUE

``` r
a & b
```

    ## [1]  TRUE    NA FALSE

R then evaluates the is.na() statement using the output, which in the
case of “one \| two” is a string of TRUE values – since none of these
are NA, this doesn’t catch the NA value.

``` r
is.na(a | b)
```

    ## [1] FALSE FALSE FALSE

The correct method is to provide is.na each vector itself, not the
vector of booleans as above

``` r
is.na(a) | is.na(b)
```

    ## [1] FALSE  TRUE FALSE

# Tidyverse

## Pipe Command (`%>%`)

The pipe command is the basis of tidyverse, takes in a dataframe (input)
and then passes along to the functions on the right, then outputting a
new object based on whatever function was used. Can be chained together.

``` r
head(iris)
```

    ##   Sepal.Length Sepal.Width Petal.Length Petal.Width Species
    ## 1          5.1         3.5          1.4         0.2  setosa
    ## 2          4.9         3.0          1.4         0.2  setosa
    ## 3          4.7         3.2          1.3         0.2  setosa
    ## 4          4.6         3.1          1.5         0.2  setosa
    ## 5          5.0         3.6          1.4         0.2  setosa
    ## 6          5.4         3.9          1.7         0.4  setosa

``` r
iris %>% summary()
```

    ##   Sepal.Length    Sepal.Width     Petal.Length    Petal.Width   
    ##  Min.   :4.300   Min.   :2.000   Min.   :1.000   Min.   :0.100  
    ##  1st Qu.:5.100   1st Qu.:2.800   1st Qu.:1.600   1st Qu.:0.300  
    ##  Median :5.800   Median :3.000   Median :4.350   Median :1.300  
    ##  Mean   :5.843   Mean   :3.057   Mean   :3.758   Mean   :1.199  
    ##  3rd Qu.:6.400   3rd Qu.:3.300   3rd Qu.:5.100   3rd Qu.:1.800  
    ##  Max.   :7.900   Max.   :4.400   Max.   :6.900   Max.   :2.500  
    ##        Species  
    ##  setosa    :50  
    ##  versicolor:50  
    ##  virginica :50  
    ##                 
    ##                 
    ## 

``` r
iris %>% summary() %>% class()
```

    ## [1] "table"

``` r
iris %>% str()
```

    ## 'data.frame':    150 obs. of  5 variables:
    ##  $ Sepal.Length: num  5.1 4.9 4.7 4.6 5 5.4 4.6 5 4.4 4.9 ...
    ##  $ Sepal.Width : num  3.5 3 3.2 3.1 3.6 3.9 3.4 3.4 2.9 3.1 ...
    ##  $ Petal.Length: num  1.4 1.4 1.3 1.5 1.4 1.7 1.4 1.5 1.4 1.5 ...
    ##  $ Petal.Width : num  0.2 0.2 0.2 0.2 0.2 0.4 0.3 0.2 0.2 0.1 ...
    ##  $ Species     : Factor w/ 3 levels "setosa","versicolor",..: 1 1 1 1 1 1 1 1 1 1 ...

## Filter/Arrange/Slice

-   Filter: similar to subset

-   arrange: arrange the rows based on a certain column

-   slice: grad certain rows

-   n(): can only be used within dplyr, but refers to the last row
    within a group

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Filter to just setosa Species  
#     and grab the one with the largest Sepal.Length
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

iris %>% 
  dplyr::filter(Species=="setosa") %>% 
  dplyr::arrange(Sepal.Length) %>% 
  dplyr::slice(n())
```

    ##   Sepal.Length Sepal.Width Petal.Length Petal.Width Species
    ## 1          5.8           4          1.2         0.2  setosa

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# For each species grab the largest and smallest sepal width
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
iris %>% 
  dplyr::group_by(Species) %>% 
  dplyr::arrange(Sepal.Width) %>% 
  dplyr::slice(c(1,n()))
```

    ## # A tibble: 6 × 5
    ## # Groups:   Species [3]
    ##   Sepal.Length Sepal.Width Petal.Length Petal.Width Species   
    ##          <dbl>       <dbl>        <dbl>       <dbl> <fct>     
    ## 1          4.5         2.3          1.3         0.3 setosa    
    ## 2          5.7         4.4          1.5         0.4 setosa    
    ## 3          5           2            3.5         1   versicolor
    ## 4          6           3.4          4.5         1.6 versicolor
    ## 5          6           2.2          5           1.5 virginica 
    ## 6          7.9         3.8          6.4         2   virginica

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# or done via summarize 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

iris %>% 
  dplyr::group_by(Species) %>% 
  dplyr::summarise(Min=base::min(Sepal.Width),
                   Max=base::max(Sepal.Width))
```

    ## # A tibble: 3 × 3
    ##   Species      Min   Max
    ##   <fct>      <dbl> <dbl>
    ## 1 setosa       2.3   4.4
    ## 2 versicolor   2     3.4
    ## 3 virginica    2.2   3.8

## mutate/select

-   mutate: add a column or change an existing column of the dataframe.
-   select: select certain columns of a dataframe

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# mutate to calculate the ratio between Sepal.width and length and
# plot
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
iris.with.ratio<-iris %>% 
  dplyr::mutate(Sepal.Ratio=Sepal.Width/Sepal.Length) 


#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# select
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
iris.with.ratio %>% 
  dplyr::select(Species,Sepal.Width,Sepal.Length ) %>% 
  dplyr::slice(1:5)
```

    ##   Species Sepal.Width Sepal.Length
    ## 1  setosa         3.5          5.1
    ## 2  setosa         3.0          4.9
    ## 3  setosa         3.2          4.7
    ## 4  setosa         3.1          4.6
    ## 5  setosa         3.6          5.0

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Dropping columns
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
iris.with.ratio %>% 
   dplyr::select(-Sepal.Length,-Sepal.Width) %>% 
   dplyr::slice(1:5)
```

    ##   Petal.Length Petal.Width Species Sepal.Ratio
    ## 1          1.4         0.2  setosa   0.6862745
    ## 2          1.4         0.2  setosa   0.6122449
    ## 3          1.3         0.2  setosa   0.6808511
    ## 4          1.5         0.2  setosa   0.6739130
    ## 5          1.4         0.2  setosa   0.7200000

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Grabing based on a vector
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

Tograb<-c("Species","Sepal.Ratio","Manual")

iris.with.ratio %>% 
   dplyr::select(dplyr::any_of(Tograb)) %>% 
   dplyr::slice(1:5)
```

    ##   Species Sepal.Ratio
    ## 1  setosa   0.6862745
    ## 2  setosa   0.6122449
    ## 3  setosa   0.6808511
    ## 4  setosa   0.6739130
    ## 5  setosa   0.7200000

## ggplot2 and tidy

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Ploting, you are just passing along
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
iris.with.ratio %>% 
  ggplot2::ggplot(aes(x=Species,y=Sepal.Ratio))+
  ggplot2::geom_boxplot()
```

![](intro_to_r_files/figure-markdown_github/ggplot%202,%20options-1.png)

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Same but now fully with the pipe and reordering the factor of
# species
# 
# fct_reorder: will reorder the variable Species as a factor based on a function (default median) of Sepal ratio. 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

iris %>% 
  dplyr::mutate(Sepal.Ratio=Sepal.Width/Sepal.Length) %>% 
  dplyr::mutate(Species=forcats::fct_reorder(Species,
                                             Sepal.Ratio)) %>% 
  ggplot2::ggplot(aes(x=Species,y=Sepal.Ratio))+
  ggplot2::geom_boxplot()
```

![](intro_to_r_files/figure-markdown_github/ggplot%202,%20options-2.png)

# Base R comparison

## Read in a dataset

``` r
dat <- read.csv(file="datwide.csv", na.strings="")

# This is a looper read.delim, sep="\t". 
dat
```

    ##     SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1    J0001            0         1730         2981 Shared ARIC  58.8  63.5  66.9
    ## 2    J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 3    J0003            0         1526         2739 Shared ARIC  73.1  77.3  80.6
    ## 4    J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 5    J0005            0         1883         2944    JHS-Only  49.7  54.8  57.7
    ## 6    J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ## 7    J0007            0         1642         3303 Shared ARIC  67.7  72.2  76.7
    ## 8    J0008            0         1607         3315    JHS-Only  34.0  38.4  43.1
    ## 9    J0009            0         1966         2933    JHS-Only  50.8  56.2  58.9
    ## 10   J0010            0         1599         2609 Shared ARIC  64.1  68.4  71.2
    ## 11   J0011            0         1596         2716    JHS-Only  47.4  51.7  54.8
    ## 12   J0012            0         1787         3071 Shared ARIC  64.3  69.2  72.8
    ## 13   J0013            0         1648         3162    JHS-Only  55.6  60.1  64.2
    ## 14   J0014            0         1662         2624    JHS-Only  42.4  46.9  49.6
    ## 15   J0015            0         1336         2611    JHS-Only  43.2  46.9  50.4
    ## 16   J0016            0         1621         3296    JHS-Only  52.7  57.2  61.8
    ## 17   J0017            0         1968         3375    JHS-Only  44.0  49.3  53.2
    ## 18   J0018            0         1841         2951    JHS-Only  54.2  59.2  62.2
    ## 19   J0019            0         1554         2907    JHS-Only  50.9  55.2  58.9
    ## 20   J0020            0           NA           NA    JHS-Only  42.3    NA    NA
    ## 21   J0021            0         1549         2727 Shared ARIC  71.9  76.2  79.4
    ## 22   J0022            0           NA           NA    JHS-Only  55.3    NA    NA
    ## 23   J0023            0         1751         3602 Shared ARIC  73.1  77.9  83.0
    ## 24   J0024            0         1617         2583    JHS-Only  57.5  62.0  64.6
    ## 25   J0025            0         1544         2558    JHS-Only  81.4  85.6  88.4
    ## 26   J0026            0         1563         2767 Shared ARIC  75.0  79.2  82.5
    ## 27   J0027            0         1987         3380    JHS-Only  39.8  45.2  49.0
    ## 28   J0028            0         1550         2650    JHS-Only  45.0  49.3  52.3
    ## 29   J0029            0         1771         2968 Shared ARIC  64.1  68.9  72.2
    ## 30   J0030            0         1659         3106    JHS-Only  51.1  55.7  59.7
    ## 31   J0031            0           NA           NA    JHS-Only  51.3    NA    NA
    ## 32   J0032            0         1471         2843 Shared ARIC  61.6  65.6  69.4
    ## 33   J0033            0         1672         2923    JHS-Only  44.1  48.7  52.1
    ## 34   J0034            0         1853         2972    JHS-Only  49.4  54.5  57.6
    ## 35   J0035            0         1571         2940    JHS-Only  57.6  61.9  65.7
    ## 36   J0036            0         1853         2931    JHS-Only  80.5  85.6  88.5
    ## 37   J0037            0           NA           NA Shared ARIC  59.0    NA    NA
    ## 38   J0038            0           NA           NA    JHS-Only  35.2    NA    NA
    ## 39   J0039            0         1350         2651    JHS-Only  66.2  69.9  73.4
    ## 40   J0040            0         1758         3300    JHS-Only  28.4  33.2  37.5
    ## 41   J0041            0         1578         2713    JHS-Only  42.0  46.3  49.4
    ## 42   J0042            0         1580         2678    JHS-Only  54.2  58.5  61.5
    ## 43   J0043            0         1750         2988 Shared ARIC  74.8  79.6  83.0
    ## 44   J0044            0         1856         2926    JHS-Only  39.0  44.0  47.0
    ## 45   J0045            0           NA           NA    JHS-Only  43.9    NA    NA
    ## 46   J0046            0         1392         2583 Shared ARIC  80.0  83.8  87.1
    ## 47   J0047            0           NA         2787    JHS-Only  47.6    NA  55.2
    ## 48   J0048            0         1603         2667    JHS-Only  48.8  53.2  56.1
    ## 49   J0049            0         1774         2979 Shared ARIC  72.6  77.4  80.7
    ## 50   J0050            0         1647         2605    JHS-Only  56.1  60.6  63.2
    ## 51   J0051            0         1785         3004 Shared ARIC  66.9  71.7  75.1
    ## 52   J0052            0         1515         2654 Shared ARIC  64.3  68.4  71.5
    ## 53   J0053            0         1554         2384    JHS-Only  36.7  41.0  43.3
    ## 54   J0054            0         1569         2952    JHS-Only  54.2  58.5  62.3
    ## 55   J0055            0         1791         2999    JHS-Only  49.6  54.5  57.8
    ## 56   J0056            0         1563         2668    JHS-Only  55.7  60.0  63.0
    ## 57   J0057            0         1707         3052 Shared ARIC  75.7  80.4  84.1
    ## 58   J0058            0         1389         2585    JHS-Only  44.2  48.0  51.2
    ## 59   J0059            0         2090         3186 Shared ARIC  63.9  69.6  72.6
    ## 60   J0060            0         1545         2947    JHS-Only  49.4  53.6  57.5
    ## 61   J0061            0         2744         4060    JHS-Only  40.3  47.8  51.4
    ## 62   J0062            0         1769         2927    JHS-Only  51.2  56.0  59.2
    ## 63   J0063            0         1671         2786    JHS-Only  55.0  59.6  62.7
    ## 64   J0064            0         1711         2936    JHS-Only  46.7  51.3  54.7
    ## 65   J0065            0         1385         2574    JHS-Only  31.6  35.4  38.6
    ## 66   J0066            0         1794         3056 Shared ARIC  58.8  63.7  67.2
    ## 67   J0067            0         2918         3658    JHS-Only  38.5  46.5  48.5
    ## 68   J0068            0         1570         2667    JHS-Only  43.3  47.6  50.6
    ## 69   J0069            0         1640         3047    JHS-Only  45.0  49.5  53.3
    ## 70   J0070            0         1528         2928    JHS-Only  50.1  54.3  58.1
    ## 71   J0071            0         1599         2707 Shared ARIC  66.6  71.0  74.0
    ## 72   J0072            0         1574         3022    JHS-Only  42.6  46.9  50.9
    ## 73   J0073            0         1453         2576    JHS-Only  53.2  57.2  60.3
    ## 74   J0074            0         1808         2691    JHS-Only  48.9  53.9  56.3
    ## 75   J0075            0         1594           NA    JHS-Only  38.8  43.2    NA
    ## 76   J0076            0         1479         2576 Shared ARIC  60.7  64.8  67.8
    ## 77   J0077            0         1513         2697    JHS-Only  47.7  51.8  55.0
    ## 78   J0078            0         1541         2643    JHS-Only  43.0  47.2  50.3
    ## 79   J0079            0         1522         2757    JHS-Only  45.1  49.2  52.6
    ## 80   J0080            0         1667         2767 Shared ARIC  78.2  82.8  85.8
    ## 81   J0081            0         1607         2730    JHS-Only  63.1  67.5  70.6
    ## 82   J0082            0         1720         2981 Shared ARIC  61.5  66.2  69.6
    ## 83   J0083            0         1612         2843    JHS-Only  42.3  46.7  50.1
    ## 84   J0084            0         2011         2964    JHS-Only  28.6  34.1  36.7
    ## 85   J0085            0         1603         2706    JHS-Only  40.3  44.6  47.7
    ## 86   J0086            0         1730         2935    JHS-Only  43.5  48.2  51.5
    ## 87   J0087            0         1601         2941    JHS-Only  56.0  60.3  64.0
    ## 88   J0088            0         1571         2670    JHS-Only  46.5  50.8  53.8
    ## 89   J0089            0         1558         2654    JHS-Only  57.4  61.7  64.7
    ## 90   J0090            0         1585         2697 Shared ARIC  65.3  69.7  72.7
    ## 91   J0091            0         1791         2913    JHS-Only  57.4  62.3  65.4
    ## 92   J0092            0         1848         2986    JHS-Only  40.2  45.3  48.4
    ## 93   J0093            0         1810         3310    JHS-Only  50.1  55.0  59.1
    ## 94   J0094            0         1561         2558    JHS-Only  55.5  59.8  62.6
    ## 95   J0095            0         2171         3235    JHS-Only  45.7  51.7  54.6
    ## 96   J0096            0         1763         2947    JHS-Only  55.5  60.3  63.5
    ## 97   J0097            0         1622         2924    JHS-Only  50.3  54.7  58.3
    ## 98   J0098            0         2145         3321    JHS-Only  33.0  38.9  42.1
    ## 99   J0099            0         1609         3205    JHS-Only  62.0  66.4  70.8
    ## 100  J0100            0           NA         3014    JHS-Only  63.6    NA  71.9
    ## 101  J0101            0         2076         3375    JHS-Only  52.8  58.5  62.0
    ## 102  J0102            0         1470         2583    JHS-Only  44.6  48.6  51.6
    ## 103  J0103            0           NA           NA Shared ARIC  76.8    NA    NA
    ## 104  J0104            0         1735         2833 Shared ARIC  64.1  68.8  71.8
    ## 105  J0105            0         1756           NA Shared ARIC  63.3  68.1    NA
    ## 106  J0106            0         1637         2921    JHS-Only  38.8  43.3  46.8
    ## 107  J0107            0         1928         3652    JHS-Only  55.4  60.7  65.4
    ## 108  J0108            0         1727         2784    JHS-Only  48.7  53.4  56.3
    ## 109  J0109            0         1938         3393 Shared ARIC  63.8  69.1  73.1
    ## 110  J0110            0         1499         2766    JHS-Only  48.7  52.8  56.2
    ## 111  J0111            0         1841         2940    JHS-Only  78.7  83.7  86.7
    ## 112  J0112            0         1725         2999    JHS-Only  42.8  47.5  51.0
    ## 113  J0113            0         1562         2661    JHS-Only  60.6  64.9  67.9
    ## 114  J0114            0         1707         3304    JHS-Only  50.3  55.0  59.3
    ## 115  J0115            0         1624         2725 Shared ARIC  61.0  65.5  68.5
    ## 116  J0116            0         2240         2947    JHS-Only  53.4  59.6  61.5
    ## 117  J0117            0         1585         2936    JHS-Only  49.9  54.3  58.0
    ## 118  J0118            0         1510         2650    JHS-Only  43.8  47.9  51.0
    ## 119  J0119            0         1703         3265    JHS-Only  33.6  38.2  42.5
    ## 120  J0120            0         1568         2669 Shared ARIC  66.4  70.6  73.7
    ## 121  J0121            0         1589         2690 Shared ARIC  63.6  67.9  70.9
    ## 122  J0122            0         1675         2799    JHS-Only  50.4  55.0  58.0
    ## 123  J0123            0         1686         3030 Shared ARIC  64.8  69.4  73.1
    ## 124  J0124            0         1653         2574    JHS-Only  47.1  51.6  54.2
    ## 125  J0125            0         1646         2599    JHS-Only  49.1  53.6  56.2
    ## 126  J0126            0         1935         3192 Shared ARIC  62.3  67.6  71.0
    ## 127  J0127            0         1885         2978    JHS-Only  47.2  52.3  55.3
    ## 128  J0128            0         1577         2871    JHS-Only  56.0  60.4  63.9
    ## 129  J0129            0         1407         2583    JHS-Only  45.4  49.2  52.4
    ## 130  J0130            0         1639         3317    JHS-Only  63.3  67.8  72.4
    ## 131  J0131            0         1994         3288    JHS-Only  29.5  35.0  38.5
    ## 132  J0132            0         1592         2703 Shared ARIC  75.4  79.8  82.8
    ## 133  J0133            0         2059         3333 Shared ARIC  58.4  64.0  67.5
    ## 134  J0134            0         1449         2525    JHS-Only  47.2  51.1  54.1
    ## 135  J0135            0         1846         2762    JHS-Only  40.4  45.5  48.0
    ## 136  J0136            0         1824         2904    JHS-Only  55.7  60.7  63.7
    ## 137  J0137            0         1656         2634    JHS-Only  43.4  48.0  50.6
    ## 138  J0138            0         1704         2856    JHS-Only  39.9  44.6  47.7
    ## 139  J0139            0         1841         2938    JHS-Only  54.8  59.8  62.8
    ## 140  J0140            0         1612         2507    JHS-Only  44.7  49.1  51.6
    ## 141  J0141            0         1863         3024    JHS-Only  48.4  53.5  56.7
    ## 142  J0142            0         1517         2633    JHS-Only  54.2  58.4  61.4
    ## 143  J0143            0         1671         2947 Shared ARIC  72.6  77.2  80.7
    ## 144  J0144            0         1603         2903 Shared ARIC  68.0  72.3  75.9
    ## 145  J0145            0         1845         2967    JHS-Only  41.9  47.0  50.1
    ## 146  J0146            0         1597         2695    JHS-Only  54.0  58.4  61.4
    ## 147  J0147            0           NA           NA    JHS-Only  46.3    NA    NA
    ## 148  J0148            0         1606         2706 Shared ARIC  71.5  75.9  78.9
    ## 149  J0149            0         1881         3265    JHS-Only  45.9  51.0  54.8
    ## 150  J0150            0         1845         2570    JHS-Only  39.7  44.7  46.7
    ## 151  J0151            0         1547         2632    JHS-Only  45.8  50.0  53.0
    ## 152  J0152            0         1785         3149 Shared ARIC  65.1  70.0  73.7
    ## 153  J0153            0         1613           NA Shared ARIC  76.4  80.8    NA
    ## 154  J0154            0         1631         3288    JHS-Only  43.4  47.8  52.4
    ## 155  J0155            0           NA         3118 Shared ARIC  63.7    NA  72.2
    ## 156  J0156            0         1714         2903 Shared ARIC  67.0  71.7  74.9
    ## 157  J0157            0         1492         2636    JHS-Only  53.6  57.6  60.8
    ## 158  J0158            0         1594         2690 Shared ARIC  59.3  63.7  66.7
    ## 159  J0159            0         1637         2952    JHS-Only  43.0  47.5  51.1
    ## 160  J0160            0         1483         2582    JHS-Only  70.7  74.8  77.8
    ## 161  J0161            0         1509         2605    JHS-Only  40.8  45.0  48.0
    ## 162  J0162            0         1382         2564    JHS-Only  60.7  64.5  67.7
    ## 163  J0163            0         1610         2556    JHS-Only  55.3  59.7  62.3
    ## 164  J0164            0         1671           NA    JHS-Only  70.3  74.9    NA
    ## 165  J0165            0         1750           NA    JHS-Only  58.9  63.7    NA
    ## 166  J0166            0         1702         3002    JHS-Only  46.6  51.2  54.8
    ## 167  J0167            0         1600         2561    JHS-Only  39.4  43.7  46.4
    ## 168  J0168            0         1637         2634 Shared ARIC  61.5  66.0  68.7
    ## 169  J0169            0         2028         3381    JHS-Only  38.4  43.9  47.6
    ## 170  J0170            0         1914         2990    JHS-Only  32.5  37.7  40.7
    ## 171  J0171            0         1821         2920    JHS-Only  54.2  59.2  62.2
    ## 172  J0172            0         1647         2661    JHS-Only  41.9  46.4  49.2
    ## 173  J0173            0         1481         2586 Shared ARIC  61.2  65.3  68.3
    ## 174  J0174            0         1576         2674    JHS-Only  64.4  68.7  71.7
    ## 175  J0175            0         1626         2653    JHS-Only  51.0  55.5  58.3
    ## 176  J0176            0         2182         2902 Shared ARIC  66.9  72.9  74.9
    ## 177  J0177            0         1028         2967    JHS-Only  44.4  47.2  52.5
    ## 178  J0178            0         1576         2675    JHS-Only  60.7  65.0  68.1
    ## 179  J0179            0           NA         4085    JHS-Only  55.1    NA  66.2
    ## 180  J0180            0         2023         3198    JHS-Only  56.0  61.5  64.7
    ## 181  J0181            0         2053         2913    JHS-Only  52.1  57.8  60.1
    ## 182  J0182            0         1595         2800    JHS-Only  55.5  59.9  63.2
    ## 183  J0183            0         1634         2937    JHS-Only  36.9  41.4  44.9
    ## 184  J0184            0         1824         3162 Shared ARIC  70.4  75.4  79.0
    ## 185  J0185            0         2105         3246    JHS-Only  33.6  39.4  42.5
    ## 186  J0186            0         1603         2715 Shared ARIC  63.4  67.8  70.8
    ## 187  J0187            0         2559         3350 Shared ARIC  63.2  70.2  72.4
    ## 188  J0188            0         1579         2717 Shared ARIC  66.2  70.5  73.6
    ## 189  J0189            0         1523         2621    JHS-Only  58.5  62.7  65.7
    ## 190  J0190            0           NA           NA Shared ARIC  74.8    NA    NA
    ## 191  J0191            0         1588         3116 Shared ARIC  78.7  83.0  87.2
    ## 192  J0192            0         1728         3423    JHS-Only  40.3  45.1  49.7
    ## 193  J0193            0         1694         2940    JHS-Only  45.0  49.6  53.0
    ## 194  J0194            0         1583         2681    JHS-Only  43.1  47.4  50.4
    ## 195  J0195            0         2259         3667    JHS-Only  59.2  65.4  69.3
    ## 196  J0196            0         1616         2690    JHS-Only  44.0  48.4  51.3
    ## 197  J0197            0         1379         2479 Shared ARIC  64.4  68.1  71.1
    ## 198  J0198            0         1629         2870    JHS-Only  70.7  75.1  78.5
    ## 199  J0199            0         1883         2632    JHS-Only  45.0  50.2  52.2
    ## 200  J0200            0         2674         3712 Shared ARIC  66.6  73.9  76.7
    ## 201  J0201            0         1659         2702 Shared ARIC  58.3  62.8  65.7
    ## 202  J0202            0         1816         3045    JHS-Only  51.4  56.3  59.7
    ## 203  J0203            0         1715         2788    JHS-Only  45.9  50.6  53.6
    ## 204  J0204            0         1813         2940    JHS-Only  52.4  57.4  60.5
    ## 205  J0205            0         1743         4028 Shared ARIC  65.0  69.8  76.0
    ## 206  J0206            0         1703         3771 Shared ARIC  61.0  65.7  71.3
    ## 207  J0207            0         1493         2670    JHS-Only  64.1  68.2  71.4
    ## 208  J0208            0         1713         2952 Shared ARIC  62.6  67.3  70.7
    ## 209  J0209            0         1708         2808    JHS-Only  70.5  75.2  78.2
    ## 210  J0210            0         1659         2826 Shared ARIC  69.8  74.4  77.5
    ## 211  J0211            0           NA           NA    JHS-Only  40.1    NA    NA
    ## 212  J0212            0         1774         2748    JHS-Only  21.6  26.5  29.1
    ## 213  J0213            0         1598         3412 Shared ARIC  63.1  67.5  72.5
    ## 214  J0214            0         1687         2800    JHS-Only  36.9  41.5  44.5
    ## 215  J0215            0         2032         3134    JHS-Only  40.6  46.1  49.1
    ## 216  J0216            0         1865         2940    JHS-Only  36.3  41.4  44.4
    ## 217  J0217            0         1726         2981    JHS-Only  30.5  35.2  38.7
    ## 218  J0218            0         1926         2927    JHS-Only  45.4  50.7  53.4
    ## 219  J0219            0         1734         2798    JHS-Only  66.5  71.2  74.2
    ## 220  J0220            0         1806         3031    JHS-Only  39.2  44.1  47.5
    ## 221  J0221            0         1607         2703 Shared ARIC  68.4  72.8  75.8
    ## 222  J0222            0         1711         3049    JHS-Only  32.8  37.5  41.2
    ## 223  J0223            0         1675         3263 Shared ARIC  64.3  68.9  73.2
    ## 224  J0224            0         1474         2530    JHS-Only  38.5  42.5  45.4
    ## 225  J0225            0           NA           NA Shared ARIC  62.5    NA    NA
    ## 226  J0226            0           NA           NA    JHS-Only  25.2    NA    NA
    ## 227  J0227            0         1543         3174    JHS-Only  54.5  58.7  63.2
    ## 228  J0228            0         1672         2580    JHS-Only  50.6  55.2  57.7
    ## 229  J0229            0         1596         2944    JHS-Only  55.5  59.9  63.6
    ## 230  J0230            0         2498         3828 Shared ARIC  66.6  73.5  77.1
    ## 231  J0231            0         1891         2987    JHS-Only  44.5  49.7  52.7
    ## 232  J0232            0         1833         2968 Shared ARIC  72.4  77.4  80.5
    ## 233  J0233            0         1792         3304 Shared ARIC  58.9  63.8  67.9
    ## 234  J0234            0         1603         2952    JHS-Only  31.2  35.6  39.3
    ## 235  J0235            0         1519         2609    JHS-Only  56.1  60.3  63.3
    ## 236  J0236            0         1519         2674    JHS-Only  38.0  42.2  45.4
    ## 237  J0237            0         1710         3033    JHS-Only  41.8  46.5  50.2
    ## 238  J0238            0         1622         2924    JHS-Only  50.7  55.1  58.7
    ## 239  J0239            0         2120         3340 Shared ARIC  60.0  65.8  69.1
    ## 240  J0240            0         1605         2702    JHS-Only  59.5  63.9  66.9
    ##     maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1     Male      94   91.44   97.79 110.99 109.16   116     8.3     7.4     8.2
    ## 2   Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 3     Male     114  106.68  101.70 132.08 122.00   130     6.0     6.0     5.9
    ## 4     Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 5   Female     153  109.22  118.00 114.66 108.00   116     7.3     7.2     7.4
    ## 6   Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ## 7   Female     118  116.84  126.30 141.25 154.00   143     6.9     7.2     8.2
    ## 8     Male     111  105.41  111.00 133.00 175.18   114     5.9     6.6    10.2
    ## 9     Male      80   85.09   88.00 107.33 119.00   130     8.8     8.4     8.5
    ## 10  Female      97   93.98      NA 124.75 117.00   123     5.7     6.1     6.4
    ## 11  Female     137  140.97  139.70 113.74 115.58   104     6.2     5.9     6.4
    ## 12    Male     102  104.14  102.87 129.33 143.08   140     6.1      NA     5.9
    ## 13    Male     108  106.68  110.40 100.91 116.00   123     6.0     5.9     6.2
    ## 14    Male     128  127.00  121.92 124.75 154.00   138     5.8     6.2     6.3
    ## 15  Female      85  111.76  111.30 110.99 109.00   139     5.1     4.8     5.4
    ## 16  Female     116   88.90  109.20 110.99 122.00   121     8.9    11.5     6.5
    ## 17    Male      85   88.90   91.50 116.49 124.00   113     5.4     5.8     6.1
    ## 18  Female     132  129.54  134.00 133.00 110.00   141     5.6     5.6     5.6
    ## 19    Male     122  119.38  124.40 136.67  93.00   148     6.5     6.4    10.1
    ## 20  Female      91      NA      NA 120.16     NA    NA     4.8      NA      NA
    ## 21  Female      91   91.44   93.00 117.41 139.00   135     5.8     6.1     6.3
    ## 22    Male     115      NA      NA 116.49     NA    NA     4.7      NA      NA
    ## 23  Female     107  101.60   97.00 142.17 141.25   177     5.5     6.0     6.1
    ## 24  Female      96  109.22  109.22 111.91 122.00   111     5.4     6.7     5.5
    ## 25    Male      96   99.06   95.25 133.92 127.00   145     5.5     5.7     5.8
    ## 26    Male     103  101.60   99.06 122.91 129.33   139     6.0      NA     5.6
    ## 27  Female      89  114.30   94.00 122.91 130.00   110     4.9     6.2     6.2
    ## 28  Female      72   81.28   83.82 118.33 123.00   138     5.3     5.7     5.9
    ## 29  Female      93   91.44   88.90 122.00 120.16   113     7.8      NA     6.8
    ## 30  Female      91   96.52   97.00 122.00 105.00   104     6.2     6.4     6.5
    ## 31    Male      94      NA      NA 124.75     NA    NA     8.4      NA      NA
    ## 32  Female      70   73.66   77.50 127.50 118.00   126     5.6     5.9     6.0
    ## 33    Male      89   86.36   88.90 133.00 115.00   139     4.4     4.6     4.7
    ## 34  Female      86   86.36   82.55 117.41 143.00   109     6.3     6.7     6.6
    ## 35    Male     115  123.19  120.65 133.00 126.58   129     5.8      NA     6.1
    ## 36  Female     112  109.22  106.00 137.58 144.00   149     5.8     5.9     5.9
    ## 37    Male      76      NA      NA 133.00     NA    NA     4.8      NA      NA
    ## 38    Male      97      NA      NA 125.66     NA    NA     5.3      NA      NA
    ## 39  Female     127  132.08  134.60 110.99 139.00   122     6.3     7.6     6.7
    ## 40  Female     144  134.62  139.70 132.08 118.00   131     6.7     5.9     6.3
    ## 41  Female      90   96.52  101.60 133.92 120.16   122     5.1     5.8     5.7
    ## 42    Male      95   97.79  101.60 113.74 136.67   147     6.1     6.2     6.1
    ## 43  Female     107  106.68  109.20 138.50 126.00   117     7.2     7.7    10.0
    ## 44  Female      90   88.90   93.90 118.33 105.00   100     4.5     5.3     5.3
    ## 45  Female     103      NA      NA 134.83     NA    NA      NA      NA      NA
    ## 46    Male      91   93.98   78.74 155.00 155.00   151     5.8     5.7     5.5
    ## 47    Male     104      NA      NA 168.76     NA   195     9.5      NA     7.1
    ## 48  Female     108  106.68  116.84 125.66  89.00    93     5.3     5.6     5.6
    ## 49    Male      96   90.17   92.50 153.17 113.00   132     5.6     6.1     6.0
    ## 50    Male     107  109.22  106.68 132.08 132.00   137     5.6     5.8     5.7
    ## 51  Female     114  101.60  111.76 136.67 133.92   121     6.2      NA     6.3
    ## 52  Female      95  101.60  106.70 162.34 151.00   140     5.2     5.6     5.7
    ## 53  Female      96   99.06   91.44 117.41 114.00   121     5.8     6.3     6.1
    ## 54    Male     113  109.22  111.76 152.25 144.00   174     6.1     6.9     7.6
    ## 55    Male      97  101.60   93.98 141.25 125.66   134     4.7      NA     5.0
    ## 56  Female      84   86.36   87.63 108.24 122.91   112     5.4     5.4     5.6
    ## 57  Female      79   86.36   81.28 156.84 153.17   183    10.8      NA     9.0
    ## 58  Female     102  106.68  115.50 122.00 134.00   130     5.5     6.0     6.5
    ## 59    Male      91   90.17   87.63 118.33 115.58   116     7.3     5.3     5.1
    ## 60    Male     104  109.22  106.60 114.66 129.00   124     5.4     5.6     5.7
    ## 61  Female      81   86.36   91.40 111.91 145.00   139     4.8     5.6     5.8
    ## 62  Female      98   95.76   86.36 139.42 122.00   121     6.7     5.7     6.0
    ## 63    Male      96   96.52   91.44 133.92 153.17   140     8.3     7.4     7.2
    ## 64  Female     106  119.38  114.30 110.99 127.00   112     5.4     5.4     5.6
    ## 65  Female     135  127.00  130.00 140.33 136.67   126     6.6     8.4     6.8
    ## 66  Female     118  116.84  111.76 161.42 128.41   115     6.9     7.3     6.2
    ## 67    Male      77   80.01   72.39 119.25 134.00   152     4.9     5.4     5.3
    ## 68  Female      89   88.90   83.82 113.74 113.74   112     5.2     5.6     5.9
    ## 69    Male     112  113.03  114.30 134.83 110.00   133     6.1     6.1     6.3
    ## 70    Male     113  119.38  120.60 128.41 142.00   132     5.6     5.9     5.9
    ## 71  Female     106   97.79  102.87 141.25 126.00   138     6.2     6.3     6.2
    ## 72    Male     107  110.49  116.00 122.91 121.00   107     5.4     5.7     6.0
    ## 73    Male     105  104.14  105.41  99.99  96.32   101     7.5     7.2     7.9
    ## 74    Male     117  116.84  128.00 116.49 120.00   136     5.1     5.3     5.7
    ## 75  Female     106  121.92      NA 131.17 134.83    NA     5.0     5.3      NA
    ## 76    Male      94   93.98   93.98 124.75 121.08   121     5.2     4.7     5.2
    ## 77  Female     109  111.76  118.00 126.58 132.00   115     5.5     7.2     7.2
    ## 78  Female     102  104.14  102.87 133.00 129.00   132     5.2     5.6     5.6
    ## 79    Male     103  106.68  111.00 110.08 114.00   114     5.6     6.0     6.5
    ## 80  Female     106  111.76  114.30 130.25 148.59   141     5.9      NA     6.0
    ## 81    Male     107  124.46   96.52 144.92 139.00   120     5.2     5.4     5.5
    ## 82  Female      81   96.52   99.06 111.91 118.33   110     5.2     5.7     5.7
    ## 83  Female      94   88.90   93.98 119.25 126.58   162     5.2     5.8     6.0
    ## 84  Female      79   86.36   91.00 104.58 104.00   122     4.9     5.2     5.5
    ## 85    Male     129  121.92  129.54 118.33 106.00   114     7.1     9.8     9.6
    ## 86    Male      89   96.52   90.00 108.24 145.00   130     5.4     5.4     5.4
    ## 87  Female     106   93.98  106.70 106.41 115.00   130     5.2     5.3     5.9
    ## 88  Female     102  100.33  101.60 120.16 132.00   121     7.8     6.9     7.7
    ## 89  Female     106  106.68  106.68 127.50 156.00   143     5.6     5.6     5.6
    ## 90    Male     116  114.30  128.27 134.83 151.34   123     5.1     7.5     7.5
    ## 91    Male     114  121.92  125.73 125.66 121.00    95     5.3     5.6     6.1
    ## 92  Female      90   80.01   90.00 117.41 139.00   135     5.1     5.4     5.3
    ## 93  Female      90   91.44   88.00 120.16 160.00   124     6.7     7.1     6.5
    ## 94  Female     106  109.22  104.14 116.49 123.00   124     4.7     5.5     6.4
    ## 95  Female      95  106.68  116.84  98.16  98.00   113     4.8     5.4     6.1
    ## 96  Female     107  109.22  116.84 122.91 132.08   122     6.8     6.3     6.4
    ## 97  Female      82   81.28   78.74  99.07 131.17   134     6.3     6.4     6.6
    ## 98  Female     122  121.92  132.00 108.24 142.00   107     4.7     5.2     5.6
    ## 99    Male     113  116.84  125.00 139.42 142.00   132    11.8    13.7     8.4
    ## 100 Female     149      NA  108.00 125.66     NA   137     6.3      NA     6.1
    ## 101 Female     106  119.38  118.00 160.51 135.00   172     5.5     5.6     5.8
    ## 102   Male      98  101.60  111.70 125.66 131.00   133     5.6     5.6     5.7
    ## 103 Female      78      NA      NA 141.25     NA    NA     5.5      NA      NA
    ## 104 Female     131  149.86  152.40 147.67 152.25   128     5.5     5.8     5.6
    ## 105 Female      79   91.44      NA 137.58 124.75    NA     6.5     6.0      NA
    ## 106 Female      98   93.98   93.98 135.75 114.00   133     4.9     5.4     5.1
    ## 107 Female      87   86.36   91.00 124.75 129.33   139     4.9     5.3     5.3
    ## 108   Male      97  100.33  100.40 109.16 114.00   126     5.6     5.6     5.8
    ## 109   Male      91  106.68  102.00 100.91 142.00   131     6.8     6.8     6.4
    ## 110   Male      82   86.36   83.80 113.74 108.00   112     5.4     5.5     5.3
    ## 111 Female     102  104.14   93.98 142.17 149.50   170     5.7     6.4     7.0
    ## 112 Female      93   93.98   88.90 138.50 135.75   158     5.8     6.2     6.0
    ## 113   Male     104  104.14  106.68 132.08 119.00   131     5.7     5.7     5.9
    ## 114   Male      87   91.44   97.79 129.33 130.25   128     5.9      NA     6.4
    ## 115 Female      91   81.28   86.36 121.08 119.25   121     5.6      NA     6.1
    ## 116   Male      87   93.98  100.33 125.66 120.00   120     5.0     5.3     5.4
    ## 117 Female      86   83.82   90.17 133.00 138.50   123     5.5     6.1     6.2
    ## 118 Female     121  101.60  116.84 116.49 109.00   132     5.5     5.7     6.2
    ## 119 Female     107  124.46  119.20 110.99 118.00   118     5.1     5.2     5.2
    ## 120 Female      82   86.36   86.36 145.84 139.00   117     5.5     5.7     5.8
    ## 121   Male      81   88.90   83.82 122.91 131.17   137     5.5     5.4     5.6
    ## 122 Female     118   99.06  119.38 130.25 130.25   117     5.4     5.6     5.7
    ## 123 Female      87   85.09   84.50 128.41 130.00   123     6.0     6.1     6.4
    ## 124 Female     101  104.14  113.03 119.25 119.00   117     5.5     6.6     6.5
    ## 125   Male     119  127.00  127.00 140.33 105.00   135     5.7     6.3     6.5
    ## 126 Female      78   85.09   83.80 120.16 122.00   117     5.6     6.0     5.9
    ## 127 Female     123  116.84  115.57 132.08 100.00   116     5.4     5.7     5.8
    ## 128   Male     105  115.57  115.00 110.99 128.00   118     6.9     7.7     7.7
    ## 129 Female      86   93.98   98.50 110.08 123.00   142     4.6     5.0     5.2
    ## 130 Female      95   91.44   89.30 148.59 149.50   146     5.3     5.6     5.4
    ## 131   Male     109   95.25  107.00 118.33 119.00   127     4.4     4.5     4.7
    ## 132 Female      84   86.36   91.44 130.25 135.75   147     5.0     5.9     6.0
    ## 133 Female     132  121.92  116.80 126.58 158.00   126     7.0     7.4     7.7
    ## 134   Male     108  106.68  110.49 122.00 121.00   133     5.8     6.0     6.5
    ## 135   Male      90   96.52   85.00 125.66 104.00   131     4.9     5.1     5.0
    ## 136   Male     102   91.44   99.80 113.74 124.00   124     5.5     5.3     5.3
    ## 137 Female     108  120.65  121.92 105.49 106.00   114     5.5     5.9     5.9
    ## 138 Female     121  114.30  113.03 122.00 128.41   123     5.8     5.8     5.4
    ## 139 Female     114  116.84  119.38 109.16 143.08   110     5.7      NA     6.0
    ## 140 Female      80   86.36   88.90 121.08 117.00   137     5.4     5.7     5.4
    ## 141 Female      83   93.98   90.17 110.08 117.00   119     5.7     5.7     5.8
    ## 142   Male      96   99.06   99.06 108.24 112.00   102     5.8     5.6     5.8
    ## 143 Female     101  104.14   99.06 123.83 131.17   140     6.4     5.8     6.0
    ## 144 Female     104   99.06  101.60 126.58 121.00   132     7.5     7.8     7.8
    ## 145 Female     118  121.92  121.92 144.92 109.00   102     5.5     6.1     6.3
    ## 146   Male      98  110.49  115.57 119.25 117.41   146     9.2     9.5     7.9
    ## 147   Male      96      NA      NA 149.50     NA    NA     6.0      NA      NA
    ## 148 Female     119  114.30  114.30 113.74 118.33   124     6.1     6.3     6.3
    ## 149 Female      76   85.09   86.00 144.92 139.00   132     5.5     5.8     6.1
    ## 150   Male      83   88.90   93.98 110.08 109.00   117     6.1     6.6     6.6
    ## 151   Male      84   83.82   87.90 117.41 114.00   108     4.7     4.8     5.0
    ## 152 Female      84   90.17   83.82 163.26 166.01   129     5.0     5.8     5.6
    ## 153 Female     123  119.38      NA 137.58 149.50    NA     8.5      NA      NA
    ## 154 Female      97  101.60  100.30 117.41  97.00   126     5.9     6.1     6.0
    ## 155   Male      81      NA   99.06 123.83     NA   128     5.6      NA     7.6
    ## 156 Female      76   78.74   73.66 113.74 110.08   105     5.4      NA      NA
    ## 157   Male      70   78.74   78.74 138.50 172.00   116     6.0     6.0     6.2
    ## 158 Female     116  147.32  137.16 123.83 121.08   128     6.3     8.3     8.7
    ## 159 Female      90   96.52   97.79 174.26 231.11   166     5.6      NA     5.5
    ## 160 Female      81   76.20   81.28 107.33 149.50   113     5.5     5.6     5.8
    ## 161 Female     108  106.68  114.30 125.66 107.00   116     5.7     5.8     6.1
    ## 162 Female      81  106.68   83.00 134.83 170.00   133     6.0     5.8     6.3
    ## 163 Female     113  111.76  121.92 113.74 122.00   127     7.8     8.3     9.6
    ## 164   Male      93   97.79      NA 138.50 127.50    NA     7.7      NA      NA
    ## 165   Male      89   81.28      NA 136.67 130.00    NA     5.8     6.0      NA
    ## 166 Female      76   78.74   93.00  93.57 105.00   102     5.3     5.5     5.6
    ## 167   Male     102  107.95  107.00 124.75 108.00   109     4.6     4.7     4.8
    ## 168 Female      80   83.82   76.20 130.25 128.00   124     5.8     5.9     6.0
    ## 169 Female      85   81.28   84.00 105.49 110.00   104     5.6     5.5     5.3
    ## 170 Female      94  104.14  104.20 111.91 106.00   123     5.5     5.5     5.5
    ## 171 Female      87   93.98   91.44 127.50 131.17   106     5.5     5.8     5.7
    ## 172   Male      93   99.06  103.00 110.99 109.00   108     5.6     5.8     5.9
    ## 173 Female     101  101.60  106.68 131.17 122.00   109     5.7     5.9     6.1
    ## 174   Male      91   88.90   83.82 149.50 101.00   114     5.1     5.8     5.7
    ## 175 Female      84   81.28   98.00 119.25 143.00   125     4.6     5.3     5.6
    ## 176 Female     129  132.08  130.81 152.25 136.00   140     5.7     6.7     7.2
    ## 177 Female      75   83.82   85.00 109.16 116.49   140     5.3     5.9      NA
    ## 178 Female     102  106.68  105.41 135.75 163.26   123     5.7     5.6     5.9
    ## 179 Female     110      NA  114.00 112.83     NA   116     8.9      NA     6.7
    ## 180 Female     104  105.41  113.03 122.91 155.00   120     5.9     6.7     6.4
    ## 181   Male     104   91.44  104.14 166.92 133.00   143     5.7     5.6     5.5
    ## 182   Male     103  109.22  114.30 128.41 108.00   116     5.4     5.5     5.9
    ## 183   Male     127  127.00  127.00 133.92 118.00   116     6.7     7.9    12.9
    ## 184 Female     110  106.68  101.60 155.92 147.00   137     7.0     8.1     6.4
    ## 185   Male      98  101.60  101.60 129.33 124.00   117     5.4     5.4     5.7
    ## 186 Female      90   99.06   96.52 146.75 146.00   143     6.0     5.8     6.2
    ## 187   Male     103   99.06  109.00 130.25 108.00   135     5.3     6.4     5.8
    ## 188 Female     122  119.38  101.60 130.25 133.92   113     5.7     6.1     6.1
    ## 189 Female      90   90.17   93.98 135.75 122.00    96     5.5     5.5     5.7
    ## 190 Female     107      NA      NA 140.33     NA    NA     6.6      NA      NA
    ## 191 Female      91  106.68   98.00 115.58 114.66   126     5.4     5.5     5.4
    ## 192 Female     113  109.22  117.00 142.17 139.00   157     5.1     5.0     5.1
    ## 193 Female      99   93.98   97.80 108.24 147.00   127     5.6     6.0     6.1
    ## 194   Male     107   99.06   96.52 131.17 130.25   124     6.8     6.9     6.6
    ## 195   Male     103   97.79   95.30 131.17 127.00   168     7.0     5.3     6.2
    ## 196 Female      87   83.82   88.40 102.74 112.00   106     5.8     5.6     5.6
    ## 197 Female     103   97.79   96.52 124.75 112.83   102     5.3     5.6     5.8
    ## 198 Female     114  116.84  121.92 116.49 164.17   138     5.4     5.5     5.4
    ## 199   Male      87   88.90   93.98 111.91 107.00   126     5.1     5.4     5.4
    ## 200 Female      85   71.12   89.00 136.67 180.00   136     5.4     5.7     5.4
    ## 201 Female     101  106.68  105.41 110.99 122.00   121     5.6     5.9     6.0
    ## 202 Female      82   81.28   88.90 120.16 127.50   129     5.8     6.3     6.3
    ## 203   Male      99  101.60  106.68 136.67 123.00   145     5.4     5.7     5.8
    ## 204   Male     105  104.14  112.00 112.83 116.00   128     4.8     4.8     5.0
    ## 205 Female      89   92.71   90.10 116.49 119.25   139     5.8      NA     6.6
    ## 206 Female     137  160.02  147.00 108.24 110.99    98     5.8      NA     5.8
    ## 207 Female     116   93.98  105.70 135.75 123.00   134     4.6     4.5     4.6
    ## 208 Female      78   83.82   81.28 121.08 128.41   133     5.2     5.5     5.3
    ## 209 Female     106  104.14  111.76 163.26 134.83   139     7.7     7.8     8.9
    ## 210 Female     110  127.00  119.38 127.50 118.33   123     6.7     7.0     6.5
    ## 211 Female      86      NA      NA 101.82     NA    NA     5.2      NA      NA
    ## 212 Female      78   78.74   86.30 101.82 130.00   119     5.2     5.3     5.2
    ## 213   Male     125  129.03  118.50 128.41 124.75   121     5.4      NA     5.6
    ## 214   Male      71   73.66   79.00 107.33 108.00   125     4.8     4.9     4.9
    ## 215 Female      79   78.74   88.00 132.08 102.00   105     4.7     5.4     5.6
    ## 216 Female     126  127.00  111.76 120.16  95.00   106     5.7     5.5     5.9
    ## 217   Male     107  111.76  109.20 117.41 112.00   121     5.2     5.6     5.5
    ## 218 Female      93   90.17   86.36 120.16 153.00   127     5.9     6.3     6.5
    ## 219 Female     103  109.22  106.68 135.75 157.00   165     5.7     6.0     6.1
    ## 220 Female      75   76.20   83.82 126.58 138.50   112     4.9      NA     5.4
    ## 221 Female     114  104.14   99.06 144.92 150.00   181     6.0     6.1     6.0
    ## 222 Female     108  111.76  115.00 104.58  93.00    97     5.3     5.6     5.9
    ## 223   Male      90   95.25   96.52 127.50 128.41   134     6.4     6.2     6.3
    ## 224 Female      99  106.68  109.50 125.66 123.00   121     5.6     6.0     6.5
    ## 225   Male      95      NA      NA 145.84     NA    NA     5.6      NA      NA
    ## 226   Male     132      NA      NA 118.33     NA    NA     6.0      NA      NA
    ## 227 Female     114  121.92  135.00 123.83 111.00   121     6.1     6.1     6.4
    ## 228 Female     109   99.06  113.03 127.50 140.00   115     5.7     5.9     6.2
    ## 229 Female      90  106.68  101.60 112.83 129.33   141     5.7     6.0     6.2
    ## 230   Male      99   96.52   97.20 120.16 128.00   136     5.7     5.9     6.2
    ## 231   Male      80   85.09   88.90 122.00 121.00   126     5.0     5.3     5.5
    ## 232   Male      98  101.60   97.79 130.25 125.66   129     5.8     6.0     6.1
    ## 233 Female      83   96.52   93.98 144.92 144.00   135     5.2     5.6     5.8
    ## 234   Male     112  111.76  114.30 132.08 148.59   134     5.7     6.4     6.5
    ## 235   Male     109  106.68  111.76 128.41 111.00   117     5.4     5.5     6.0
    ## 236   Male      78   86.36   86.40 103.66 120.00   110     5.1     5.4     5.5
    ## 237   Male     129  114.30  111.76 109.16 124.75   114     5.5     5.7     5.8
    ## 238   Male      97   96.52   97.79 146.75 115.58   134     6.2     6.7     6.8
    ## 239   Male     108  109.22  114.30 110.99 141.00   125     5.5     5.8     6.0
    ## 240 Female     109  115.57  106.68 164.17 118.33   119     6.6     6.1     6.2
    ##     eGFRV1 eGFRV3
    ## 1   102.16  75.13
    ## 2    92.67  79.14
    ## 3    92.36  98.37
    ## 4   101.83  99.44
    ## 5   120.07 111.42
    ## 6   108.67 101.31
    ## 7    47.57  33.20
    ## 8    96.99  82.75
    ## 9    96.00  88.68
    ## 10   73.95  56.54
    ## 11   94.80  84.69
    ## 12   87.30  78.63
    ## 13  113.41  84.42
    ## 14  101.86  93.59
    ## 15  113.03 119.68
    ## 16   91.27  79.55
    ## 17   90.46  81.05
    ## 18  104.66  51.79
    ## 19   95.94  73.35
    ## 20  113.72     NA
    ## 21   92.39  84.88
    ## 22   63.44     NA
    ## 23   79.09  64.19
    ## 24  113.63  87.27
    ## 25   87.17  66.71
    ## 26   55.26  52.73
    ## 27  115.79 119.02
    ## 28   96.36  91.12
    ## 29   53.46  34.76
    ## 30  106.90 112.11
    ## 31   95.70     NA
    ## 32   85.77  91.06
    ## 33  113.26 103.52
    ## 34   93.43  97.42
    ## 35   67.97  68.60
    ## 36  101.95  92.49
    ## 37  102.04     NA
    ## 38   96.23     NA
    ## 39   72.87  58.80
    ## 40  125.40 129.10
    ## 41   98.46  83.41
    ## 42   93.76  95.70
    ## 43   78.17  81.49
    ## 44  100.55  94.58
    ## 45  112.49     NA
    ## 46   58.08  48.99
    ## 47   88.20  81.59
    ## 48  120.82  88.71
    ## 49   74.00  60.55
    ## 50   83.07  78.76
    ## 51  106.43  52.76
    ## 52   84.17  85.64
    ## 53  118.28 118.64
    ## 54   76.24  76.81
    ## 55  109.00  90.39
    ## 56   89.38  84.49
    ## 57   68.15  40.57
    ## 58  112.28 100.28
    ## 59   98.56  72.96
    ## 60   71.99  72.66
    ## 61  128.25 108.40
    ## 62  106.89  68.02
    ## 63  104.88 107.21
    ## 64  110.33 114.93
    ## 65  122.67 109.59
    ## 66  101.28  24.89
    ## 67  117.79  93.22
    ## 68  112.98  62.67
    ## 69   89.81  86.26
    ## 70  108.58  98.01
    ## 71   82.79  77.10
    ## 72   75.50  88.69
    ## 73  115.30 112.30
    ## 74   87.36  83.57
    ## 75  116.55     NA
    ## 76   72.86  69.60
    ## 77  121.79 114.65
    ## 78   76.19  77.82
    ## 79   89.77  83.97
    ## 80   76.32  71.00
    ## 81   79.07  89.79
    ## 82   66.92  52.63
    ## 83   98.22  93.87
    ## 84  125.23  93.59
    ## 85  103.39  99.32
    ## 86  123.48 109.38
    ## 87  103.35 106.61
    ## 88   83.69  74.08
    ## 89  102.28  94.10
    ## 90   86.69  71.40
    ## 91   82.32  65.58
    ## 92   87.43  88.57
    ## 93   93.01  89.38
    ## 94  103.65  84.78
    ## 95   74.76  71.09
    ## 96   78.55  75.51
    ## 97   92.88 110.48
    ## 98  134.99 115.72
    ## 99   99.88  84.44
    ## 100 108.88  84.18
    ## 101  80.05  72.54
    ## 102 112.89  97.74
    ## 103  89.29     NA
    ## 104  97.61  89.50
    ## 105  98.17     NA
    ## 106 116.57 100.37
    ## 107  78.60  81.93
    ## 108  87.49  91.31
    ## 109  71.30  61.55
    ## 110 109.69  98.11
    ## 111  76.08  61.75
    ## 112  97.89  93.27
    ## 113  80.46  80.45
    ## 114 108.44  72.41
    ## 115  86.10 102.81
    ## 116  64.29  71.31
    ## 117  93.10  78.54
    ## 118 112.60 117.38
    ## 119 134.45 123.41
    ## 120  82.95  75.19
    ## 121  71.42  66.75
    ## 122 107.49 110.67
    ## 123  73.56  70.60
    ## 124  83.31  73.88
    ## 125  87.28  74.76
    ## 126 109.91 101.99
    ## 127 122.21 112.79
    ## 128  83.11  74.42
    ## 129  96.12  82.71
    ## 130  84.74  75.86
    ## 131 100.11  85.45
    ## 132  68.28  59.69
    ## 133  87.74  47.66
    ## 134  98.50  90.68
    ## 135 103.28  89.56
    ## 136 104.39  90.90
    ## 137  97.45  90.88
    ## 138 115.69 116.95
    ## 139  89.98  93.88
    ## 140 111.86  91.58
    ## 141  73.36  74.39
    ## 142  76.25  69.33
    ## 143  91.94  64.43
    ## 144 111.34 101.05
    ## 145  76.77  96.66
    ## 146 105.66  81.52
    ## 147 121.05     NA
    ## 148  70.18  62.08
    ## 149  74.67  72.66
    ## 150  93.24  76.90
    ## 151  89.31  87.40
    ## 152  96.93  84.35
    ## 153  77.32     NA
    ## 154  97.49  84.98
    ## 155 107.13  86.62
    ## 156  82.60     NA
    ## 157 105.98  83.66
    ## 158  76.44  58.99
    ## 159  76.17  95.94
    ## 160  93.16  62.58
    ## 161 114.95 111.07
    ## 162 117.14  92.12
    ## 163  56.87  28.46
    ## 164  37.78     NA
    ## 165 102.08     NA
    ## 166  95.31  90.81
    ## 167 104.05 116.42
    ## 168  99.39  88.69
    ## 169  88.58 101.30
    ## 170 121.87 101.74
    ## 171 104.64  85.00
    ## 172 102.23  86.94
    ## 173  75.43  70.27
    ## 174  98.21  93.69
    ## 175  92.39 112.08
    ## 176 106.37  95.02
    ## 177 124.61     NA
    ## 178 111.10  99.69
    ## 179  62.87  39.65
    ## 180 103.35  74.90
    ## 181  85.41 102.96
    ## 182  83.43  71.88
    ## 183 105.87 107.52
    ## 184  62.86  36.52
    ## 185  88.13  79.95
    ## 186  59.29  55.50
    ## 187  99.02  58.61
    ## 188  96.20  75.23
    ## 189 118.96  92.00
    ## 190  78.16     NA
    ## 191  76.09  76.73
    ## 192 135.16 127.01
    ## 193  96.40  71.86
    ## 194  82.48  87.11
    ## 195  67.19  51.26
    ## 196 112.43 116.00
    ## 197  97.43  95.93
    ## 198  70.59  50.96
    ## 199 100.00 100.91
    ## 200  72.67  62.30
    ## 201 101.67  98.06
    ## 202 106.74 109.38
    ## 203 121.38 102.47
    ## 204 106.84  92.96
    ## 205  58.62  50.32
    ## 206  99.76  85.78
    ## 207  97.61 101.71
    ## 208  98.62  81.21
    ## 209  93.33 101.63
    ## 210 109.89  96.49
    ## 211 128.43     NA
    ## 212 131.56 137.55
    ## 213  99.09 102.05
    ## 214 105.89  93.77
    ## 215 128.02 113.85
    ## 216 131.88  86.40
    ## 217 124.62 107.06
    ## 218 111.28 121.58
    ## 219  72.70  74.93
    ## 220 100.39 104.56
    ## 221  94.71  72.09
    ## 222 121.59  99.94
    ## 223  98.27  82.02
    ## 224 100.87  96.99
    ## 225 120.19     NA
    ## 226 129.36     NA
    ## 227  90.16  80.93
    ## 228  81.29  84.14
    ## 229  78.52  85.37
    ## 230  69.90  61.53
    ## 231 100.33 114.96
    ## 232  82.49  63.62
    ## 233  76.70 101.48
    ## 234 110.17 111.86
    ## 235  92.49  90.07
    ## 236  94.31  85.64
    ## 237 102.25 110.43
    ## 238 117.37 117.26
    ## 239  80.82  68.92
    ## 240 100.80 106.01

``` r
head(dat)
```

    ##   SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1  J0001            0         1730         2981 Shared ARIC  58.8  63.5  66.9
    ## 2  J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 3  J0003            0         1526         2739 Shared ARIC  73.1  77.3  80.6
    ## 4  J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 5  J0005            0         1883         2944    JHS-Only  49.7  54.8  57.7
    ## 6  J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ##   maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1   Male      94   91.44   97.79 110.99 109.16   116     8.3     7.4     8.2
    ## 2 Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 3   Male     114  106.68  101.70 132.08 122.00   130     6.0     6.0     5.9
    ## 4   Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 5 Female     153  109.22  118.00 114.66 108.00   116     7.3     7.2     7.4
    ## 6 Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ##   eGFRV1 eGFRV3
    ## 1 102.16  75.13
    ## 2  92.67  79.14
    ## 3  92.36  98.37
    ## 4 101.83  99.44
    ## 5 120.07 111.42
    ## 6 108.67 101.31

``` r
summary(dat)
```

    ##     SUBJID           DaysFromV1V1  DaysFromV1V2   DaysFromV1V3 
    ##  Length:240         Min.   :0     Min.   :1028   Min.   :2384  
    ##  Class :character   1st Qu.:0     1st Qu.:1579   1st Qu.:2676  
    ##  Mode  :character   Median :0     Median :1650   Median :2921  
    ##                     Mean   :0     Mean   :1709   Mean   :2921  
    ##                     3rd Qu.:0     3rd Qu.:1792   3rd Qu.:3032  
    ##                     Max.   :0     Max.   :2918   Max.   :4085  
    ##                                   NA's   :16     NA's   :17    
    ##     ARICV1              ageV1           ageV2           ageV3      
    ##  Length:240         Min.   :21.60   Min.   :26.50   Min.   :29.10  
    ##  Class :character   1st Qu.:44.08   1st Qu.:48.55   1st Qu.:52.15  
    ##  Mode  :character   Median :54.10   Median :58.50   Median :61.50  
    ##                     Mean   :53.63   Mean   :58.38   Mean   :61.59  
    ##                     3rd Qu.:63.33   3rd Qu.:67.95   3rd Qu.:71.25  
    ##                     Max.   :81.40   Max.   :85.60   Max.   :88.50  
    ##                                     NA's   :16      NA's   :17     
    ##     maleV1             waistV1         waistV2          waistV3      
    ##  Length:240         Min.   : 70.0   Min.   : 71.12   Min.   : 72.39  
    ##  Class :character   1st Qu.: 89.0   1st Qu.: 90.17   1st Qu.: 91.40  
    ##  Mode  :character   Median : 99.0   Median :101.60   Median :101.60  
    ##                     Mean   :100.2   Mean   :101.84   Mean   :103.19  
    ##                     3rd Qu.:109.0   3rd Qu.:109.54   3rd Qu.:114.30  
    ##                     Max.   :153.0   Max.   :160.02   Max.   :152.40  
    ##                                     NA's   :16       NA's   :19      
    ##      sbpV1            sbpV2           sbpV3          HbA1cV1      
    ##  Min.   : 93.57   Min.   : 89.0   Min.   : 93.0   Min.   : 4.000  
    ##  1st Qu.:114.66   1st Qu.:115.6   1st Qu.:116.0   1st Qu.: 5.300  
    ##  Median :124.75   Median :124.8   Median :126.0   Median : 5.600  
    ##  Mean   :126.17   Mean   :127.5   Mean   :127.5   Mean   : 5.847  
    ##  3rd Qu.:134.83   3rd Qu.:138.6   3rd Qu.:136.5   3rd Qu.: 6.050  
    ##  Max.   :174.26   Max.   :231.1   Max.   :195.0   Max.   :11.800  
    ##                   NA's   :16      NA's   :17      NA's   :1       
    ##     HbA1cV2         HbA1cV3           eGFRV1           eGFRV3      
    ##  Min.   : 4.50   Min.   : 4.600   Min.   : 37.78   Min.   : 24.89  
    ##  1st Qu.: 5.50   1st Qu.: 5.600   1st Qu.: 82.57   1st Qu.: 73.35  
    ##  Median : 5.80   Median : 6.000   Median : 96.96   Median : 86.26  
    ##  Mean   : 6.06   Mean   : 6.188   Mean   : 95.40   Mean   : 85.79  
    ##  3rd Qu.: 6.20   3rd Qu.: 6.400   3rd Qu.:107.73   3rd Qu.: 99.69  
    ##  Max.   :13.70   Max.   :12.900   Max.   :135.16   Max.   :137.55  
    ##  NA's   :35      NA's   :19                        NA's   :19

``` r
dim(dat)
```

    ## [1] 240  20

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# tidyverse
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
dat %>% head()
```

    ##   SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1  J0001            0         1730         2981 Shared ARIC  58.8  63.5  66.9
    ## 2  J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 3  J0003            0         1526         2739 Shared ARIC  73.1  77.3  80.6
    ## 4  J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 5  J0005            0         1883         2944    JHS-Only  49.7  54.8  57.7
    ## 6  J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ##   maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1   Male      94   91.44   97.79 110.99 109.16   116     8.3     7.4     8.2
    ## 2 Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 3   Male     114  106.68  101.70 132.08 122.00   130     6.0     6.0     5.9
    ## 4   Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 5 Female     153  109.22  118.00 114.66 108.00   116     7.3     7.2     7.4
    ## 6 Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ##   eGFRV1 eGFRV3
    ## 1 102.16  75.13
    ## 2  92.67  79.14
    ## 3  92.36  98.37
    ## 4 101.83  99.44
    ## 5 120.07 111.42
    ## 6 108.67 101.31

``` r
dat %>% dim()
```

    ## [1] 240  20

``` r
dat %>% summary()
```

    ##     SUBJID           DaysFromV1V1  DaysFromV1V2   DaysFromV1V3 
    ##  Length:240         Min.   :0     Min.   :1028   Min.   :2384  
    ##  Class :character   1st Qu.:0     1st Qu.:1579   1st Qu.:2676  
    ##  Mode  :character   Median :0     Median :1650   Median :2921  
    ##                     Mean   :0     Mean   :1709   Mean   :2921  
    ##                     3rd Qu.:0     3rd Qu.:1792   3rd Qu.:3032  
    ##                     Max.   :0     Max.   :2918   Max.   :4085  
    ##                                   NA's   :16     NA's   :17    
    ##     ARICV1              ageV1           ageV2           ageV3      
    ##  Length:240         Min.   :21.60   Min.   :26.50   Min.   :29.10  
    ##  Class :character   1st Qu.:44.08   1st Qu.:48.55   1st Qu.:52.15  
    ##  Mode  :character   Median :54.10   Median :58.50   Median :61.50  
    ##                     Mean   :53.63   Mean   :58.38   Mean   :61.59  
    ##                     3rd Qu.:63.33   3rd Qu.:67.95   3rd Qu.:71.25  
    ##                     Max.   :81.40   Max.   :85.60   Max.   :88.50  
    ##                                     NA's   :16      NA's   :17     
    ##     maleV1             waistV1         waistV2          waistV3      
    ##  Length:240         Min.   : 70.0   Min.   : 71.12   Min.   : 72.39  
    ##  Class :character   1st Qu.: 89.0   1st Qu.: 90.17   1st Qu.: 91.40  
    ##  Mode  :character   Median : 99.0   Median :101.60   Median :101.60  
    ##                     Mean   :100.2   Mean   :101.84   Mean   :103.19  
    ##                     3rd Qu.:109.0   3rd Qu.:109.54   3rd Qu.:114.30  
    ##                     Max.   :153.0   Max.   :160.02   Max.   :152.40  
    ##                                     NA's   :16       NA's   :19      
    ##      sbpV1            sbpV2           sbpV3          HbA1cV1      
    ##  Min.   : 93.57   Min.   : 89.0   Min.   : 93.0   Min.   : 4.000  
    ##  1st Qu.:114.66   1st Qu.:115.6   1st Qu.:116.0   1st Qu.: 5.300  
    ##  Median :124.75   Median :124.8   Median :126.0   Median : 5.600  
    ##  Mean   :126.17   Mean   :127.5   Mean   :127.5   Mean   : 5.847  
    ##  3rd Qu.:134.83   3rd Qu.:138.6   3rd Qu.:136.5   3rd Qu.: 6.050  
    ##  Max.   :174.26   Max.   :231.1   Max.   :195.0   Max.   :11.800  
    ##                   NA's   :16      NA's   :17      NA's   :1       
    ##     HbA1cV2         HbA1cV3           eGFRV1           eGFRV3      
    ##  Min.   : 4.50   Min.   : 4.600   Min.   : 37.78   Min.   : 24.89  
    ##  1st Qu.: 5.50   1st Qu.: 5.600   1st Qu.: 82.57   1st Qu.: 73.35  
    ##  Median : 5.80   Median : 6.000   Median : 96.96   Median : 86.26  
    ##  Mean   : 6.06   Mean   : 6.188   Mean   : 95.40   Mean   : 85.79  
    ##  3rd Qu.: 6.20   3rd Qu.: 6.400   3rd Qu.:107.73   3rd Qu.: 99.69  
    ##  Max.   :13.70   Max.   :12.900   Max.   :135.16   Max.   :137.55  
    ##  NA's   :35      NA's   :19                        NA's   :19

## Subsetting the data

``` r
dat$SUBJID %in% c("J0050", "J0100", "J0200")
```

    ##   [1] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [13] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [25] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [37] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [49] FALSE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [61] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [73] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [85] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [97] FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [109] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [121] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [133] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [145] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [157] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [169] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [181] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [193] FALSE FALSE FALSE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE
    ## [205] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [217] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [229] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE

``` r
dat[dat$SUBJID %in% c("J0050", "J0100", "J0200"),]
```

    ##     SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 50   J0050            0         1647         2605    JHS-Only  56.1  60.6  63.2
    ## 100  J0100            0           NA         3014    JHS-Only  63.6    NA  71.9
    ## 200  J0200            0         2674         3712 Shared ARIC  66.6  73.9  76.7
    ##     maleV1 waistV1 waistV2 waistV3  sbpV1 sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 50    Male     107  109.22  106.68 132.08   132   137     5.6     5.8     5.7
    ## 100 Female     149      NA  108.00 125.66    NA   137     6.3      NA     6.1
    ## 200 Female      85   71.12   89.00 136.67   180   136     5.4     5.7     5.4
    ##     eGFRV1 eGFRV3
    ## 50   83.07  78.76
    ## 100 108.88  84.18
    ## 200  72.67  62.30

``` r
# Be careful of the order you are using to make sure we gat the right thing
c("J0050", "J0100", "J0200") %in% dat$SUBJID 
```

    ## [1] TRUE TRUE TRUE

``` r
dat[c("J0050", "J0100", "J0200") %in% dat$SUBJID,]
```

    ##     SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1    J0001            0         1730         2981 Shared ARIC  58.8  63.5  66.9
    ## 2    J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 3    J0003            0         1526         2739 Shared ARIC  73.1  77.3  80.6
    ## 4    J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 5    J0005            0         1883         2944    JHS-Only  49.7  54.8  57.7
    ## 6    J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ## 7    J0007            0         1642         3303 Shared ARIC  67.7  72.2  76.7
    ## 8    J0008            0         1607         3315    JHS-Only  34.0  38.4  43.1
    ## 9    J0009            0         1966         2933    JHS-Only  50.8  56.2  58.9
    ## 10   J0010            0         1599         2609 Shared ARIC  64.1  68.4  71.2
    ## 11   J0011            0         1596         2716    JHS-Only  47.4  51.7  54.8
    ## 12   J0012            0         1787         3071 Shared ARIC  64.3  69.2  72.8
    ## 13   J0013            0         1648         3162    JHS-Only  55.6  60.1  64.2
    ## 14   J0014            0         1662         2624    JHS-Only  42.4  46.9  49.6
    ## 15   J0015            0         1336         2611    JHS-Only  43.2  46.9  50.4
    ## 16   J0016            0         1621         3296    JHS-Only  52.7  57.2  61.8
    ## 17   J0017            0         1968         3375    JHS-Only  44.0  49.3  53.2
    ## 18   J0018            0         1841         2951    JHS-Only  54.2  59.2  62.2
    ## 19   J0019            0         1554         2907    JHS-Only  50.9  55.2  58.9
    ## 20   J0020            0           NA           NA    JHS-Only  42.3    NA    NA
    ## 21   J0021            0         1549         2727 Shared ARIC  71.9  76.2  79.4
    ## 22   J0022            0           NA           NA    JHS-Only  55.3    NA    NA
    ## 23   J0023            0         1751         3602 Shared ARIC  73.1  77.9  83.0
    ## 24   J0024            0         1617         2583    JHS-Only  57.5  62.0  64.6
    ## 25   J0025            0         1544         2558    JHS-Only  81.4  85.6  88.4
    ## 26   J0026            0         1563         2767 Shared ARIC  75.0  79.2  82.5
    ## 27   J0027            0         1987         3380    JHS-Only  39.8  45.2  49.0
    ## 28   J0028            0         1550         2650    JHS-Only  45.0  49.3  52.3
    ## 29   J0029            0         1771         2968 Shared ARIC  64.1  68.9  72.2
    ## 30   J0030            0         1659         3106    JHS-Only  51.1  55.7  59.7
    ## 31   J0031            0           NA           NA    JHS-Only  51.3    NA    NA
    ## 32   J0032            0         1471         2843 Shared ARIC  61.6  65.6  69.4
    ## 33   J0033            0         1672         2923    JHS-Only  44.1  48.7  52.1
    ## 34   J0034            0         1853         2972    JHS-Only  49.4  54.5  57.6
    ## 35   J0035            0         1571         2940    JHS-Only  57.6  61.9  65.7
    ## 36   J0036            0         1853         2931    JHS-Only  80.5  85.6  88.5
    ## 37   J0037            0           NA           NA Shared ARIC  59.0    NA    NA
    ## 38   J0038            0           NA           NA    JHS-Only  35.2    NA    NA
    ## 39   J0039            0         1350         2651    JHS-Only  66.2  69.9  73.4
    ## 40   J0040            0         1758         3300    JHS-Only  28.4  33.2  37.5
    ## 41   J0041            0         1578         2713    JHS-Only  42.0  46.3  49.4
    ## 42   J0042            0         1580         2678    JHS-Only  54.2  58.5  61.5
    ## 43   J0043            0         1750         2988 Shared ARIC  74.8  79.6  83.0
    ## 44   J0044            0         1856         2926    JHS-Only  39.0  44.0  47.0
    ## 45   J0045            0           NA           NA    JHS-Only  43.9    NA    NA
    ## 46   J0046            0         1392         2583 Shared ARIC  80.0  83.8  87.1
    ## 47   J0047            0           NA         2787    JHS-Only  47.6    NA  55.2
    ## 48   J0048            0         1603         2667    JHS-Only  48.8  53.2  56.1
    ## 49   J0049            0         1774         2979 Shared ARIC  72.6  77.4  80.7
    ## 50   J0050            0         1647         2605    JHS-Only  56.1  60.6  63.2
    ## 51   J0051            0         1785         3004 Shared ARIC  66.9  71.7  75.1
    ## 52   J0052            0         1515         2654 Shared ARIC  64.3  68.4  71.5
    ## 53   J0053            0         1554         2384    JHS-Only  36.7  41.0  43.3
    ## 54   J0054            0         1569         2952    JHS-Only  54.2  58.5  62.3
    ## 55   J0055            0         1791         2999    JHS-Only  49.6  54.5  57.8
    ## 56   J0056            0         1563         2668    JHS-Only  55.7  60.0  63.0
    ## 57   J0057            0         1707         3052 Shared ARIC  75.7  80.4  84.1
    ## 58   J0058            0         1389         2585    JHS-Only  44.2  48.0  51.2
    ## 59   J0059            0         2090         3186 Shared ARIC  63.9  69.6  72.6
    ## 60   J0060            0         1545         2947    JHS-Only  49.4  53.6  57.5
    ## 61   J0061            0         2744         4060    JHS-Only  40.3  47.8  51.4
    ## 62   J0062            0         1769         2927    JHS-Only  51.2  56.0  59.2
    ## 63   J0063            0         1671         2786    JHS-Only  55.0  59.6  62.7
    ## 64   J0064            0         1711         2936    JHS-Only  46.7  51.3  54.7
    ## 65   J0065            0         1385         2574    JHS-Only  31.6  35.4  38.6
    ## 66   J0066            0         1794         3056 Shared ARIC  58.8  63.7  67.2
    ## 67   J0067            0         2918         3658    JHS-Only  38.5  46.5  48.5
    ## 68   J0068            0         1570         2667    JHS-Only  43.3  47.6  50.6
    ## 69   J0069            0         1640         3047    JHS-Only  45.0  49.5  53.3
    ## 70   J0070            0         1528         2928    JHS-Only  50.1  54.3  58.1
    ## 71   J0071            0         1599         2707 Shared ARIC  66.6  71.0  74.0
    ## 72   J0072            0         1574         3022    JHS-Only  42.6  46.9  50.9
    ## 73   J0073            0         1453         2576    JHS-Only  53.2  57.2  60.3
    ## 74   J0074            0         1808         2691    JHS-Only  48.9  53.9  56.3
    ## 75   J0075            0         1594           NA    JHS-Only  38.8  43.2    NA
    ## 76   J0076            0         1479         2576 Shared ARIC  60.7  64.8  67.8
    ## 77   J0077            0         1513         2697    JHS-Only  47.7  51.8  55.0
    ## 78   J0078            0         1541         2643    JHS-Only  43.0  47.2  50.3
    ## 79   J0079            0         1522         2757    JHS-Only  45.1  49.2  52.6
    ## 80   J0080            0         1667         2767 Shared ARIC  78.2  82.8  85.8
    ## 81   J0081            0         1607         2730    JHS-Only  63.1  67.5  70.6
    ## 82   J0082            0         1720         2981 Shared ARIC  61.5  66.2  69.6
    ## 83   J0083            0         1612         2843    JHS-Only  42.3  46.7  50.1
    ## 84   J0084            0         2011         2964    JHS-Only  28.6  34.1  36.7
    ## 85   J0085            0         1603         2706    JHS-Only  40.3  44.6  47.7
    ## 86   J0086            0         1730         2935    JHS-Only  43.5  48.2  51.5
    ## 87   J0087            0         1601         2941    JHS-Only  56.0  60.3  64.0
    ## 88   J0088            0         1571         2670    JHS-Only  46.5  50.8  53.8
    ## 89   J0089            0         1558         2654    JHS-Only  57.4  61.7  64.7
    ## 90   J0090            0         1585         2697 Shared ARIC  65.3  69.7  72.7
    ## 91   J0091            0         1791         2913    JHS-Only  57.4  62.3  65.4
    ## 92   J0092            0         1848         2986    JHS-Only  40.2  45.3  48.4
    ## 93   J0093            0         1810         3310    JHS-Only  50.1  55.0  59.1
    ## 94   J0094            0         1561         2558    JHS-Only  55.5  59.8  62.6
    ## 95   J0095            0         2171         3235    JHS-Only  45.7  51.7  54.6
    ## 96   J0096            0         1763         2947    JHS-Only  55.5  60.3  63.5
    ## 97   J0097            0         1622         2924    JHS-Only  50.3  54.7  58.3
    ## 98   J0098            0         2145         3321    JHS-Only  33.0  38.9  42.1
    ## 99   J0099            0         1609         3205    JHS-Only  62.0  66.4  70.8
    ## 100  J0100            0           NA         3014    JHS-Only  63.6    NA  71.9
    ## 101  J0101            0         2076         3375    JHS-Only  52.8  58.5  62.0
    ## 102  J0102            0         1470         2583    JHS-Only  44.6  48.6  51.6
    ## 103  J0103            0           NA           NA Shared ARIC  76.8    NA    NA
    ## 104  J0104            0         1735         2833 Shared ARIC  64.1  68.8  71.8
    ## 105  J0105            0         1756           NA Shared ARIC  63.3  68.1    NA
    ## 106  J0106            0         1637         2921    JHS-Only  38.8  43.3  46.8
    ## 107  J0107            0         1928         3652    JHS-Only  55.4  60.7  65.4
    ## 108  J0108            0         1727         2784    JHS-Only  48.7  53.4  56.3
    ## 109  J0109            0         1938         3393 Shared ARIC  63.8  69.1  73.1
    ## 110  J0110            0         1499         2766    JHS-Only  48.7  52.8  56.2
    ## 111  J0111            0         1841         2940    JHS-Only  78.7  83.7  86.7
    ## 112  J0112            0         1725         2999    JHS-Only  42.8  47.5  51.0
    ## 113  J0113            0         1562         2661    JHS-Only  60.6  64.9  67.9
    ## 114  J0114            0         1707         3304    JHS-Only  50.3  55.0  59.3
    ## 115  J0115            0         1624         2725 Shared ARIC  61.0  65.5  68.5
    ## 116  J0116            0         2240         2947    JHS-Only  53.4  59.6  61.5
    ## 117  J0117            0         1585         2936    JHS-Only  49.9  54.3  58.0
    ## 118  J0118            0         1510         2650    JHS-Only  43.8  47.9  51.0
    ## 119  J0119            0         1703         3265    JHS-Only  33.6  38.2  42.5
    ## 120  J0120            0         1568         2669 Shared ARIC  66.4  70.6  73.7
    ## 121  J0121            0         1589         2690 Shared ARIC  63.6  67.9  70.9
    ## 122  J0122            0         1675         2799    JHS-Only  50.4  55.0  58.0
    ## 123  J0123            0         1686         3030 Shared ARIC  64.8  69.4  73.1
    ## 124  J0124            0         1653         2574    JHS-Only  47.1  51.6  54.2
    ## 125  J0125            0         1646         2599    JHS-Only  49.1  53.6  56.2
    ## 126  J0126            0         1935         3192 Shared ARIC  62.3  67.6  71.0
    ## 127  J0127            0         1885         2978    JHS-Only  47.2  52.3  55.3
    ## 128  J0128            0         1577         2871    JHS-Only  56.0  60.4  63.9
    ## 129  J0129            0         1407         2583    JHS-Only  45.4  49.2  52.4
    ## 130  J0130            0         1639         3317    JHS-Only  63.3  67.8  72.4
    ## 131  J0131            0         1994         3288    JHS-Only  29.5  35.0  38.5
    ## 132  J0132            0         1592         2703 Shared ARIC  75.4  79.8  82.8
    ## 133  J0133            0         2059         3333 Shared ARIC  58.4  64.0  67.5
    ## 134  J0134            0         1449         2525    JHS-Only  47.2  51.1  54.1
    ## 135  J0135            0         1846         2762    JHS-Only  40.4  45.5  48.0
    ## 136  J0136            0         1824         2904    JHS-Only  55.7  60.7  63.7
    ## 137  J0137            0         1656         2634    JHS-Only  43.4  48.0  50.6
    ## 138  J0138            0         1704         2856    JHS-Only  39.9  44.6  47.7
    ## 139  J0139            0         1841         2938    JHS-Only  54.8  59.8  62.8
    ## 140  J0140            0         1612         2507    JHS-Only  44.7  49.1  51.6
    ## 141  J0141            0         1863         3024    JHS-Only  48.4  53.5  56.7
    ## 142  J0142            0         1517         2633    JHS-Only  54.2  58.4  61.4
    ## 143  J0143            0         1671         2947 Shared ARIC  72.6  77.2  80.7
    ## 144  J0144            0         1603         2903 Shared ARIC  68.0  72.3  75.9
    ## 145  J0145            0         1845         2967    JHS-Only  41.9  47.0  50.1
    ## 146  J0146            0         1597         2695    JHS-Only  54.0  58.4  61.4
    ## 147  J0147            0           NA           NA    JHS-Only  46.3    NA    NA
    ## 148  J0148            0         1606         2706 Shared ARIC  71.5  75.9  78.9
    ## 149  J0149            0         1881         3265    JHS-Only  45.9  51.0  54.8
    ## 150  J0150            0         1845         2570    JHS-Only  39.7  44.7  46.7
    ## 151  J0151            0         1547         2632    JHS-Only  45.8  50.0  53.0
    ## 152  J0152            0         1785         3149 Shared ARIC  65.1  70.0  73.7
    ## 153  J0153            0         1613           NA Shared ARIC  76.4  80.8    NA
    ## 154  J0154            0         1631         3288    JHS-Only  43.4  47.8  52.4
    ## 155  J0155            0           NA         3118 Shared ARIC  63.7    NA  72.2
    ## 156  J0156            0         1714         2903 Shared ARIC  67.0  71.7  74.9
    ## 157  J0157            0         1492         2636    JHS-Only  53.6  57.6  60.8
    ## 158  J0158            0         1594         2690 Shared ARIC  59.3  63.7  66.7
    ## 159  J0159            0         1637         2952    JHS-Only  43.0  47.5  51.1
    ## 160  J0160            0         1483         2582    JHS-Only  70.7  74.8  77.8
    ## 161  J0161            0         1509         2605    JHS-Only  40.8  45.0  48.0
    ## 162  J0162            0         1382         2564    JHS-Only  60.7  64.5  67.7
    ## 163  J0163            0         1610         2556    JHS-Only  55.3  59.7  62.3
    ## 164  J0164            0         1671           NA    JHS-Only  70.3  74.9    NA
    ## 165  J0165            0         1750           NA    JHS-Only  58.9  63.7    NA
    ## 166  J0166            0         1702         3002    JHS-Only  46.6  51.2  54.8
    ## 167  J0167            0         1600         2561    JHS-Only  39.4  43.7  46.4
    ## 168  J0168            0         1637         2634 Shared ARIC  61.5  66.0  68.7
    ## 169  J0169            0         2028         3381    JHS-Only  38.4  43.9  47.6
    ## 170  J0170            0         1914         2990    JHS-Only  32.5  37.7  40.7
    ## 171  J0171            0         1821         2920    JHS-Only  54.2  59.2  62.2
    ## 172  J0172            0         1647         2661    JHS-Only  41.9  46.4  49.2
    ## 173  J0173            0         1481         2586 Shared ARIC  61.2  65.3  68.3
    ## 174  J0174            0         1576         2674    JHS-Only  64.4  68.7  71.7
    ## 175  J0175            0         1626         2653    JHS-Only  51.0  55.5  58.3
    ## 176  J0176            0         2182         2902 Shared ARIC  66.9  72.9  74.9
    ## 177  J0177            0         1028         2967    JHS-Only  44.4  47.2  52.5
    ## 178  J0178            0         1576         2675    JHS-Only  60.7  65.0  68.1
    ## 179  J0179            0           NA         4085    JHS-Only  55.1    NA  66.2
    ## 180  J0180            0         2023         3198    JHS-Only  56.0  61.5  64.7
    ## 181  J0181            0         2053         2913    JHS-Only  52.1  57.8  60.1
    ## 182  J0182            0         1595         2800    JHS-Only  55.5  59.9  63.2
    ## 183  J0183            0         1634         2937    JHS-Only  36.9  41.4  44.9
    ## 184  J0184            0         1824         3162 Shared ARIC  70.4  75.4  79.0
    ## 185  J0185            0         2105         3246    JHS-Only  33.6  39.4  42.5
    ## 186  J0186            0         1603         2715 Shared ARIC  63.4  67.8  70.8
    ## 187  J0187            0         2559         3350 Shared ARIC  63.2  70.2  72.4
    ## 188  J0188            0         1579         2717 Shared ARIC  66.2  70.5  73.6
    ## 189  J0189            0         1523         2621    JHS-Only  58.5  62.7  65.7
    ## 190  J0190            0           NA           NA Shared ARIC  74.8    NA    NA
    ## 191  J0191            0         1588         3116 Shared ARIC  78.7  83.0  87.2
    ## 192  J0192            0         1728         3423    JHS-Only  40.3  45.1  49.7
    ## 193  J0193            0         1694         2940    JHS-Only  45.0  49.6  53.0
    ## 194  J0194            0         1583         2681    JHS-Only  43.1  47.4  50.4
    ## 195  J0195            0         2259         3667    JHS-Only  59.2  65.4  69.3
    ## 196  J0196            0         1616         2690    JHS-Only  44.0  48.4  51.3
    ## 197  J0197            0         1379         2479 Shared ARIC  64.4  68.1  71.1
    ## 198  J0198            0         1629         2870    JHS-Only  70.7  75.1  78.5
    ## 199  J0199            0         1883         2632    JHS-Only  45.0  50.2  52.2
    ## 200  J0200            0         2674         3712 Shared ARIC  66.6  73.9  76.7
    ## 201  J0201            0         1659         2702 Shared ARIC  58.3  62.8  65.7
    ## 202  J0202            0         1816         3045    JHS-Only  51.4  56.3  59.7
    ## 203  J0203            0         1715         2788    JHS-Only  45.9  50.6  53.6
    ## 204  J0204            0         1813         2940    JHS-Only  52.4  57.4  60.5
    ## 205  J0205            0         1743         4028 Shared ARIC  65.0  69.8  76.0
    ## 206  J0206            0         1703         3771 Shared ARIC  61.0  65.7  71.3
    ## 207  J0207            0         1493         2670    JHS-Only  64.1  68.2  71.4
    ## 208  J0208            0         1713         2952 Shared ARIC  62.6  67.3  70.7
    ## 209  J0209            0         1708         2808    JHS-Only  70.5  75.2  78.2
    ## 210  J0210            0         1659         2826 Shared ARIC  69.8  74.4  77.5
    ## 211  J0211            0           NA           NA    JHS-Only  40.1    NA    NA
    ## 212  J0212            0         1774         2748    JHS-Only  21.6  26.5  29.1
    ## 213  J0213            0         1598         3412 Shared ARIC  63.1  67.5  72.5
    ## 214  J0214            0         1687         2800    JHS-Only  36.9  41.5  44.5
    ## 215  J0215            0         2032         3134    JHS-Only  40.6  46.1  49.1
    ## 216  J0216            0         1865         2940    JHS-Only  36.3  41.4  44.4
    ## 217  J0217            0         1726         2981    JHS-Only  30.5  35.2  38.7
    ## 218  J0218            0         1926         2927    JHS-Only  45.4  50.7  53.4
    ## 219  J0219            0         1734         2798    JHS-Only  66.5  71.2  74.2
    ## 220  J0220            0         1806         3031    JHS-Only  39.2  44.1  47.5
    ## 221  J0221            0         1607         2703 Shared ARIC  68.4  72.8  75.8
    ## 222  J0222            0         1711         3049    JHS-Only  32.8  37.5  41.2
    ## 223  J0223            0         1675         3263 Shared ARIC  64.3  68.9  73.2
    ## 224  J0224            0         1474         2530    JHS-Only  38.5  42.5  45.4
    ## 225  J0225            0           NA           NA Shared ARIC  62.5    NA    NA
    ## 226  J0226            0           NA           NA    JHS-Only  25.2    NA    NA
    ## 227  J0227            0         1543         3174    JHS-Only  54.5  58.7  63.2
    ## 228  J0228            0         1672         2580    JHS-Only  50.6  55.2  57.7
    ## 229  J0229            0         1596         2944    JHS-Only  55.5  59.9  63.6
    ## 230  J0230            0         2498         3828 Shared ARIC  66.6  73.5  77.1
    ## 231  J0231            0         1891         2987    JHS-Only  44.5  49.7  52.7
    ## 232  J0232            0         1833         2968 Shared ARIC  72.4  77.4  80.5
    ## 233  J0233            0         1792         3304 Shared ARIC  58.9  63.8  67.9
    ## 234  J0234            0         1603         2952    JHS-Only  31.2  35.6  39.3
    ## 235  J0235            0         1519         2609    JHS-Only  56.1  60.3  63.3
    ## 236  J0236            0         1519         2674    JHS-Only  38.0  42.2  45.4
    ## 237  J0237            0         1710         3033    JHS-Only  41.8  46.5  50.2
    ## 238  J0238            0         1622         2924    JHS-Only  50.7  55.1  58.7
    ## 239  J0239            0         2120         3340 Shared ARIC  60.0  65.8  69.1
    ## 240  J0240            0         1605         2702    JHS-Only  59.5  63.9  66.9
    ##     maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1     Male      94   91.44   97.79 110.99 109.16   116     8.3     7.4     8.2
    ## 2   Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 3     Male     114  106.68  101.70 132.08 122.00   130     6.0     6.0     5.9
    ## 4     Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 5   Female     153  109.22  118.00 114.66 108.00   116     7.3     7.2     7.4
    ## 6   Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ## 7   Female     118  116.84  126.30 141.25 154.00   143     6.9     7.2     8.2
    ## 8     Male     111  105.41  111.00 133.00 175.18   114     5.9     6.6    10.2
    ## 9     Male      80   85.09   88.00 107.33 119.00   130     8.8     8.4     8.5
    ## 10  Female      97   93.98      NA 124.75 117.00   123     5.7     6.1     6.4
    ## 11  Female     137  140.97  139.70 113.74 115.58   104     6.2     5.9     6.4
    ## 12    Male     102  104.14  102.87 129.33 143.08   140     6.1      NA     5.9
    ## 13    Male     108  106.68  110.40 100.91 116.00   123     6.0     5.9     6.2
    ## 14    Male     128  127.00  121.92 124.75 154.00   138     5.8     6.2     6.3
    ## 15  Female      85  111.76  111.30 110.99 109.00   139     5.1     4.8     5.4
    ## 16  Female     116   88.90  109.20 110.99 122.00   121     8.9    11.5     6.5
    ## 17    Male      85   88.90   91.50 116.49 124.00   113     5.4     5.8     6.1
    ## 18  Female     132  129.54  134.00 133.00 110.00   141     5.6     5.6     5.6
    ## 19    Male     122  119.38  124.40 136.67  93.00   148     6.5     6.4    10.1
    ## 20  Female      91      NA      NA 120.16     NA    NA     4.8      NA      NA
    ## 21  Female      91   91.44   93.00 117.41 139.00   135     5.8     6.1     6.3
    ## 22    Male     115      NA      NA 116.49     NA    NA     4.7      NA      NA
    ## 23  Female     107  101.60   97.00 142.17 141.25   177     5.5     6.0     6.1
    ## 24  Female      96  109.22  109.22 111.91 122.00   111     5.4     6.7     5.5
    ## 25    Male      96   99.06   95.25 133.92 127.00   145     5.5     5.7     5.8
    ## 26    Male     103  101.60   99.06 122.91 129.33   139     6.0      NA     5.6
    ## 27  Female      89  114.30   94.00 122.91 130.00   110     4.9     6.2     6.2
    ## 28  Female      72   81.28   83.82 118.33 123.00   138     5.3     5.7     5.9
    ## 29  Female      93   91.44   88.90 122.00 120.16   113     7.8      NA     6.8
    ## 30  Female      91   96.52   97.00 122.00 105.00   104     6.2     6.4     6.5
    ## 31    Male      94      NA      NA 124.75     NA    NA     8.4      NA      NA
    ## 32  Female      70   73.66   77.50 127.50 118.00   126     5.6     5.9     6.0
    ## 33    Male      89   86.36   88.90 133.00 115.00   139     4.4     4.6     4.7
    ## 34  Female      86   86.36   82.55 117.41 143.00   109     6.3     6.7     6.6
    ## 35    Male     115  123.19  120.65 133.00 126.58   129     5.8      NA     6.1
    ## 36  Female     112  109.22  106.00 137.58 144.00   149     5.8     5.9     5.9
    ## 37    Male      76      NA      NA 133.00     NA    NA     4.8      NA      NA
    ## 38    Male      97      NA      NA 125.66     NA    NA     5.3      NA      NA
    ## 39  Female     127  132.08  134.60 110.99 139.00   122     6.3     7.6     6.7
    ## 40  Female     144  134.62  139.70 132.08 118.00   131     6.7     5.9     6.3
    ## 41  Female      90   96.52  101.60 133.92 120.16   122     5.1     5.8     5.7
    ## 42    Male      95   97.79  101.60 113.74 136.67   147     6.1     6.2     6.1
    ## 43  Female     107  106.68  109.20 138.50 126.00   117     7.2     7.7    10.0
    ## 44  Female      90   88.90   93.90 118.33 105.00   100     4.5     5.3     5.3
    ## 45  Female     103      NA      NA 134.83     NA    NA      NA      NA      NA
    ## 46    Male      91   93.98   78.74 155.00 155.00   151     5.8     5.7     5.5
    ## 47    Male     104      NA      NA 168.76     NA   195     9.5      NA     7.1
    ## 48  Female     108  106.68  116.84 125.66  89.00    93     5.3     5.6     5.6
    ## 49    Male      96   90.17   92.50 153.17 113.00   132     5.6     6.1     6.0
    ## 50    Male     107  109.22  106.68 132.08 132.00   137     5.6     5.8     5.7
    ## 51  Female     114  101.60  111.76 136.67 133.92   121     6.2      NA     6.3
    ## 52  Female      95  101.60  106.70 162.34 151.00   140     5.2     5.6     5.7
    ## 53  Female      96   99.06   91.44 117.41 114.00   121     5.8     6.3     6.1
    ## 54    Male     113  109.22  111.76 152.25 144.00   174     6.1     6.9     7.6
    ## 55    Male      97  101.60   93.98 141.25 125.66   134     4.7      NA     5.0
    ## 56  Female      84   86.36   87.63 108.24 122.91   112     5.4     5.4     5.6
    ## 57  Female      79   86.36   81.28 156.84 153.17   183    10.8      NA     9.0
    ## 58  Female     102  106.68  115.50 122.00 134.00   130     5.5     6.0     6.5
    ## 59    Male      91   90.17   87.63 118.33 115.58   116     7.3     5.3     5.1
    ## 60    Male     104  109.22  106.60 114.66 129.00   124     5.4     5.6     5.7
    ## 61  Female      81   86.36   91.40 111.91 145.00   139     4.8     5.6     5.8
    ## 62  Female      98   95.76   86.36 139.42 122.00   121     6.7     5.7     6.0
    ## 63    Male      96   96.52   91.44 133.92 153.17   140     8.3     7.4     7.2
    ## 64  Female     106  119.38  114.30 110.99 127.00   112     5.4     5.4     5.6
    ## 65  Female     135  127.00  130.00 140.33 136.67   126     6.6     8.4     6.8
    ## 66  Female     118  116.84  111.76 161.42 128.41   115     6.9     7.3     6.2
    ## 67    Male      77   80.01   72.39 119.25 134.00   152     4.9     5.4     5.3
    ## 68  Female      89   88.90   83.82 113.74 113.74   112     5.2     5.6     5.9
    ## 69    Male     112  113.03  114.30 134.83 110.00   133     6.1     6.1     6.3
    ## 70    Male     113  119.38  120.60 128.41 142.00   132     5.6     5.9     5.9
    ## 71  Female     106   97.79  102.87 141.25 126.00   138     6.2     6.3     6.2
    ## 72    Male     107  110.49  116.00 122.91 121.00   107     5.4     5.7     6.0
    ## 73    Male     105  104.14  105.41  99.99  96.32   101     7.5     7.2     7.9
    ## 74    Male     117  116.84  128.00 116.49 120.00   136     5.1     5.3     5.7
    ## 75  Female     106  121.92      NA 131.17 134.83    NA     5.0     5.3      NA
    ## 76    Male      94   93.98   93.98 124.75 121.08   121     5.2     4.7     5.2
    ## 77  Female     109  111.76  118.00 126.58 132.00   115     5.5     7.2     7.2
    ## 78  Female     102  104.14  102.87 133.00 129.00   132     5.2     5.6     5.6
    ## 79    Male     103  106.68  111.00 110.08 114.00   114     5.6     6.0     6.5
    ## 80  Female     106  111.76  114.30 130.25 148.59   141     5.9      NA     6.0
    ## 81    Male     107  124.46   96.52 144.92 139.00   120     5.2     5.4     5.5
    ## 82  Female      81   96.52   99.06 111.91 118.33   110     5.2     5.7     5.7
    ## 83  Female      94   88.90   93.98 119.25 126.58   162     5.2     5.8     6.0
    ## 84  Female      79   86.36   91.00 104.58 104.00   122     4.9     5.2     5.5
    ## 85    Male     129  121.92  129.54 118.33 106.00   114     7.1     9.8     9.6
    ## 86    Male      89   96.52   90.00 108.24 145.00   130     5.4     5.4     5.4
    ## 87  Female     106   93.98  106.70 106.41 115.00   130     5.2     5.3     5.9
    ## 88  Female     102  100.33  101.60 120.16 132.00   121     7.8     6.9     7.7
    ## 89  Female     106  106.68  106.68 127.50 156.00   143     5.6     5.6     5.6
    ## 90    Male     116  114.30  128.27 134.83 151.34   123     5.1     7.5     7.5
    ## 91    Male     114  121.92  125.73 125.66 121.00    95     5.3     5.6     6.1
    ## 92  Female      90   80.01   90.00 117.41 139.00   135     5.1     5.4     5.3
    ## 93  Female      90   91.44   88.00 120.16 160.00   124     6.7     7.1     6.5
    ## 94  Female     106  109.22  104.14 116.49 123.00   124     4.7     5.5     6.4
    ## 95  Female      95  106.68  116.84  98.16  98.00   113     4.8     5.4     6.1
    ## 96  Female     107  109.22  116.84 122.91 132.08   122     6.8     6.3     6.4
    ## 97  Female      82   81.28   78.74  99.07 131.17   134     6.3     6.4     6.6
    ## 98  Female     122  121.92  132.00 108.24 142.00   107     4.7     5.2     5.6
    ## 99    Male     113  116.84  125.00 139.42 142.00   132    11.8    13.7     8.4
    ## 100 Female     149      NA  108.00 125.66     NA   137     6.3      NA     6.1
    ## 101 Female     106  119.38  118.00 160.51 135.00   172     5.5     5.6     5.8
    ## 102   Male      98  101.60  111.70 125.66 131.00   133     5.6     5.6     5.7
    ## 103 Female      78      NA      NA 141.25     NA    NA     5.5      NA      NA
    ## 104 Female     131  149.86  152.40 147.67 152.25   128     5.5     5.8     5.6
    ## 105 Female      79   91.44      NA 137.58 124.75    NA     6.5     6.0      NA
    ## 106 Female      98   93.98   93.98 135.75 114.00   133     4.9     5.4     5.1
    ## 107 Female      87   86.36   91.00 124.75 129.33   139     4.9     5.3     5.3
    ## 108   Male      97  100.33  100.40 109.16 114.00   126     5.6     5.6     5.8
    ## 109   Male      91  106.68  102.00 100.91 142.00   131     6.8     6.8     6.4
    ## 110   Male      82   86.36   83.80 113.74 108.00   112     5.4     5.5     5.3
    ## 111 Female     102  104.14   93.98 142.17 149.50   170     5.7     6.4     7.0
    ## 112 Female      93   93.98   88.90 138.50 135.75   158     5.8     6.2     6.0
    ## 113   Male     104  104.14  106.68 132.08 119.00   131     5.7     5.7     5.9
    ## 114   Male      87   91.44   97.79 129.33 130.25   128     5.9      NA     6.4
    ## 115 Female      91   81.28   86.36 121.08 119.25   121     5.6      NA     6.1
    ## 116   Male      87   93.98  100.33 125.66 120.00   120     5.0     5.3     5.4
    ## 117 Female      86   83.82   90.17 133.00 138.50   123     5.5     6.1     6.2
    ## 118 Female     121  101.60  116.84 116.49 109.00   132     5.5     5.7     6.2
    ## 119 Female     107  124.46  119.20 110.99 118.00   118     5.1     5.2     5.2
    ## 120 Female      82   86.36   86.36 145.84 139.00   117     5.5     5.7     5.8
    ## 121   Male      81   88.90   83.82 122.91 131.17   137     5.5     5.4     5.6
    ## 122 Female     118   99.06  119.38 130.25 130.25   117     5.4     5.6     5.7
    ## 123 Female      87   85.09   84.50 128.41 130.00   123     6.0     6.1     6.4
    ## 124 Female     101  104.14  113.03 119.25 119.00   117     5.5     6.6     6.5
    ## 125   Male     119  127.00  127.00 140.33 105.00   135     5.7     6.3     6.5
    ## 126 Female      78   85.09   83.80 120.16 122.00   117     5.6     6.0     5.9
    ## 127 Female     123  116.84  115.57 132.08 100.00   116     5.4     5.7     5.8
    ## 128   Male     105  115.57  115.00 110.99 128.00   118     6.9     7.7     7.7
    ## 129 Female      86   93.98   98.50 110.08 123.00   142     4.6     5.0     5.2
    ## 130 Female      95   91.44   89.30 148.59 149.50   146     5.3     5.6     5.4
    ## 131   Male     109   95.25  107.00 118.33 119.00   127     4.4     4.5     4.7
    ## 132 Female      84   86.36   91.44 130.25 135.75   147     5.0     5.9     6.0
    ## 133 Female     132  121.92  116.80 126.58 158.00   126     7.0     7.4     7.7
    ## 134   Male     108  106.68  110.49 122.00 121.00   133     5.8     6.0     6.5
    ## 135   Male      90   96.52   85.00 125.66 104.00   131     4.9     5.1     5.0
    ## 136   Male     102   91.44   99.80 113.74 124.00   124     5.5     5.3     5.3
    ## 137 Female     108  120.65  121.92 105.49 106.00   114     5.5     5.9     5.9
    ## 138 Female     121  114.30  113.03 122.00 128.41   123     5.8     5.8     5.4
    ## 139 Female     114  116.84  119.38 109.16 143.08   110     5.7      NA     6.0
    ## 140 Female      80   86.36   88.90 121.08 117.00   137     5.4     5.7     5.4
    ## 141 Female      83   93.98   90.17 110.08 117.00   119     5.7     5.7     5.8
    ## 142   Male      96   99.06   99.06 108.24 112.00   102     5.8     5.6     5.8
    ## 143 Female     101  104.14   99.06 123.83 131.17   140     6.4     5.8     6.0
    ## 144 Female     104   99.06  101.60 126.58 121.00   132     7.5     7.8     7.8
    ## 145 Female     118  121.92  121.92 144.92 109.00   102     5.5     6.1     6.3
    ## 146   Male      98  110.49  115.57 119.25 117.41   146     9.2     9.5     7.9
    ## 147   Male      96      NA      NA 149.50     NA    NA     6.0      NA      NA
    ## 148 Female     119  114.30  114.30 113.74 118.33   124     6.1     6.3     6.3
    ## 149 Female      76   85.09   86.00 144.92 139.00   132     5.5     5.8     6.1
    ## 150   Male      83   88.90   93.98 110.08 109.00   117     6.1     6.6     6.6
    ## 151   Male      84   83.82   87.90 117.41 114.00   108     4.7     4.8     5.0
    ## 152 Female      84   90.17   83.82 163.26 166.01   129     5.0     5.8     5.6
    ## 153 Female     123  119.38      NA 137.58 149.50    NA     8.5      NA      NA
    ## 154 Female      97  101.60  100.30 117.41  97.00   126     5.9     6.1     6.0
    ## 155   Male      81      NA   99.06 123.83     NA   128     5.6      NA     7.6
    ## 156 Female      76   78.74   73.66 113.74 110.08   105     5.4      NA      NA
    ## 157   Male      70   78.74   78.74 138.50 172.00   116     6.0     6.0     6.2
    ## 158 Female     116  147.32  137.16 123.83 121.08   128     6.3     8.3     8.7
    ## 159 Female      90   96.52   97.79 174.26 231.11   166     5.6      NA     5.5
    ## 160 Female      81   76.20   81.28 107.33 149.50   113     5.5     5.6     5.8
    ## 161 Female     108  106.68  114.30 125.66 107.00   116     5.7     5.8     6.1
    ## 162 Female      81  106.68   83.00 134.83 170.00   133     6.0     5.8     6.3
    ## 163 Female     113  111.76  121.92 113.74 122.00   127     7.8     8.3     9.6
    ## 164   Male      93   97.79      NA 138.50 127.50    NA     7.7      NA      NA
    ## 165   Male      89   81.28      NA 136.67 130.00    NA     5.8     6.0      NA
    ## 166 Female      76   78.74   93.00  93.57 105.00   102     5.3     5.5     5.6
    ## 167   Male     102  107.95  107.00 124.75 108.00   109     4.6     4.7     4.8
    ## 168 Female      80   83.82   76.20 130.25 128.00   124     5.8     5.9     6.0
    ## 169 Female      85   81.28   84.00 105.49 110.00   104     5.6     5.5     5.3
    ## 170 Female      94  104.14  104.20 111.91 106.00   123     5.5     5.5     5.5
    ## 171 Female      87   93.98   91.44 127.50 131.17   106     5.5     5.8     5.7
    ## 172   Male      93   99.06  103.00 110.99 109.00   108     5.6     5.8     5.9
    ## 173 Female     101  101.60  106.68 131.17 122.00   109     5.7     5.9     6.1
    ## 174   Male      91   88.90   83.82 149.50 101.00   114     5.1     5.8     5.7
    ## 175 Female      84   81.28   98.00 119.25 143.00   125     4.6     5.3     5.6
    ## 176 Female     129  132.08  130.81 152.25 136.00   140     5.7     6.7     7.2
    ## 177 Female      75   83.82   85.00 109.16 116.49   140     5.3     5.9      NA
    ## 178 Female     102  106.68  105.41 135.75 163.26   123     5.7     5.6     5.9
    ## 179 Female     110      NA  114.00 112.83     NA   116     8.9      NA     6.7
    ## 180 Female     104  105.41  113.03 122.91 155.00   120     5.9     6.7     6.4
    ## 181   Male     104   91.44  104.14 166.92 133.00   143     5.7     5.6     5.5
    ## 182   Male     103  109.22  114.30 128.41 108.00   116     5.4     5.5     5.9
    ## 183   Male     127  127.00  127.00 133.92 118.00   116     6.7     7.9    12.9
    ## 184 Female     110  106.68  101.60 155.92 147.00   137     7.0     8.1     6.4
    ## 185   Male      98  101.60  101.60 129.33 124.00   117     5.4     5.4     5.7
    ## 186 Female      90   99.06   96.52 146.75 146.00   143     6.0     5.8     6.2
    ## 187   Male     103   99.06  109.00 130.25 108.00   135     5.3     6.4     5.8
    ## 188 Female     122  119.38  101.60 130.25 133.92   113     5.7     6.1     6.1
    ## 189 Female      90   90.17   93.98 135.75 122.00    96     5.5     5.5     5.7
    ## 190 Female     107      NA      NA 140.33     NA    NA     6.6      NA      NA
    ## 191 Female      91  106.68   98.00 115.58 114.66   126     5.4     5.5     5.4
    ## 192 Female     113  109.22  117.00 142.17 139.00   157     5.1     5.0     5.1
    ## 193 Female      99   93.98   97.80 108.24 147.00   127     5.6     6.0     6.1
    ## 194   Male     107   99.06   96.52 131.17 130.25   124     6.8     6.9     6.6
    ## 195   Male     103   97.79   95.30 131.17 127.00   168     7.0     5.3     6.2
    ## 196 Female      87   83.82   88.40 102.74 112.00   106     5.8     5.6     5.6
    ## 197 Female     103   97.79   96.52 124.75 112.83   102     5.3     5.6     5.8
    ## 198 Female     114  116.84  121.92 116.49 164.17   138     5.4     5.5     5.4
    ## 199   Male      87   88.90   93.98 111.91 107.00   126     5.1     5.4     5.4
    ## 200 Female      85   71.12   89.00 136.67 180.00   136     5.4     5.7     5.4
    ## 201 Female     101  106.68  105.41 110.99 122.00   121     5.6     5.9     6.0
    ## 202 Female      82   81.28   88.90 120.16 127.50   129     5.8     6.3     6.3
    ## 203   Male      99  101.60  106.68 136.67 123.00   145     5.4     5.7     5.8
    ## 204   Male     105  104.14  112.00 112.83 116.00   128     4.8     4.8     5.0
    ## 205 Female      89   92.71   90.10 116.49 119.25   139     5.8      NA     6.6
    ## 206 Female     137  160.02  147.00 108.24 110.99    98     5.8      NA     5.8
    ## 207 Female     116   93.98  105.70 135.75 123.00   134     4.6     4.5     4.6
    ## 208 Female      78   83.82   81.28 121.08 128.41   133     5.2     5.5     5.3
    ## 209 Female     106  104.14  111.76 163.26 134.83   139     7.7     7.8     8.9
    ## 210 Female     110  127.00  119.38 127.50 118.33   123     6.7     7.0     6.5
    ## 211 Female      86      NA      NA 101.82     NA    NA     5.2      NA      NA
    ## 212 Female      78   78.74   86.30 101.82 130.00   119     5.2     5.3     5.2
    ## 213   Male     125  129.03  118.50 128.41 124.75   121     5.4      NA     5.6
    ## 214   Male      71   73.66   79.00 107.33 108.00   125     4.8     4.9     4.9
    ## 215 Female      79   78.74   88.00 132.08 102.00   105     4.7     5.4     5.6
    ## 216 Female     126  127.00  111.76 120.16  95.00   106     5.7     5.5     5.9
    ## 217   Male     107  111.76  109.20 117.41 112.00   121     5.2     5.6     5.5
    ## 218 Female      93   90.17   86.36 120.16 153.00   127     5.9     6.3     6.5
    ## 219 Female     103  109.22  106.68 135.75 157.00   165     5.7     6.0     6.1
    ## 220 Female      75   76.20   83.82 126.58 138.50   112     4.9      NA     5.4
    ## 221 Female     114  104.14   99.06 144.92 150.00   181     6.0     6.1     6.0
    ## 222 Female     108  111.76  115.00 104.58  93.00    97     5.3     5.6     5.9
    ## 223   Male      90   95.25   96.52 127.50 128.41   134     6.4     6.2     6.3
    ## 224 Female      99  106.68  109.50 125.66 123.00   121     5.6     6.0     6.5
    ## 225   Male      95      NA      NA 145.84     NA    NA     5.6      NA      NA
    ## 226   Male     132      NA      NA 118.33     NA    NA     6.0      NA      NA
    ## 227 Female     114  121.92  135.00 123.83 111.00   121     6.1     6.1     6.4
    ## 228 Female     109   99.06  113.03 127.50 140.00   115     5.7     5.9     6.2
    ## 229 Female      90  106.68  101.60 112.83 129.33   141     5.7     6.0     6.2
    ## 230   Male      99   96.52   97.20 120.16 128.00   136     5.7     5.9     6.2
    ## 231   Male      80   85.09   88.90 122.00 121.00   126     5.0     5.3     5.5
    ## 232   Male      98  101.60   97.79 130.25 125.66   129     5.8     6.0     6.1
    ## 233 Female      83   96.52   93.98 144.92 144.00   135     5.2     5.6     5.8
    ## 234   Male     112  111.76  114.30 132.08 148.59   134     5.7     6.4     6.5
    ## 235   Male     109  106.68  111.76 128.41 111.00   117     5.4     5.5     6.0
    ## 236   Male      78   86.36   86.40 103.66 120.00   110     5.1     5.4     5.5
    ## 237   Male     129  114.30  111.76 109.16 124.75   114     5.5     5.7     5.8
    ## 238   Male      97   96.52   97.79 146.75 115.58   134     6.2     6.7     6.8
    ## 239   Male     108  109.22  114.30 110.99 141.00   125     5.5     5.8     6.0
    ## 240 Female     109  115.57  106.68 164.17 118.33   119     6.6     6.1     6.2
    ##     eGFRV1 eGFRV3
    ## 1   102.16  75.13
    ## 2    92.67  79.14
    ## 3    92.36  98.37
    ## 4   101.83  99.44
    ## 5   120.07 111.42
    ## 6   108.67 101.31
    ## 7    47.57  33.20
    ## 8    96.99  82.75
    ## 9    96.00  88.68
    ## 10   73.95  56.54
    ## 11   94.80  84.69
    ## 12   87.30  78.63
    ## 13  113.41  84.42
    ## 14  101.86  93.59
    ## 15  113.03 119.68
    ## 16   91.27  79.55
    ## 17   90.46  81.05
    ## 18  104.66  51.79
    ## 19   95.94  73.35
    ## 20  113.72     NA
    ## 21   92.39  84.88
    ## 22   63.44     NA
    ## 23   79.09  64.19
    ## 24  113.63  87.27
    ## 25   87.17  66.71
    ## 26   55.26  52.73
    ## 27  115.79 119.02
    ## 28   96.36  91.12
    ## 29   53.46  34.76
    ## 30  106.90 112.11
    ## 31   95.70     NA
    ## 32   85.77  91.06
    ## 33  113.26 103.52
    ## 34   93.43  97.42
    ## 35   67.97  68.60
    ## 36  101.95  92.49
    ## 37  102.04     NA
    ## 38   96.23     NA
    ## 39   72.87  58.80
    ## 40  125.40 129.10
    ## 41   98.46  83.41
    ## 42   93.76  95.70
    ## 43   78.17  81.49
    ## 44  100.55  94.58
    ## 45  112.49     NA
    ## 46   58.08  48.99
    ## 47   88.20  81.59
    ## 48  120.82  88.71
    ## 49   74.00  60.55
    ## 50   83.07  78.76
    ## 51  106.43  52.76
    ## 52   84.17  85.64
    ## 53  118.28 118.64
    ## 54   76.24  76.81
    ## 55  109.00  90.39
    ## 56   89.38  84.49
    ## 57   68.15  40.57
    ## 58  112.28 100.28
    ## 59   98.56  72.96
    ## 60   71.99  72.66
    ## 61  128.25 108.40
    ## 62  106.89  68.02
    ## 63  104.88 107.21
    ## 64  110.33 114.93
    ## 65  122.67 109.59
    ## 66  101.28  24.89
    ## 67  117.79  93.22
    ## 68  112.98  62.67
    ## 69   89.81  86.26
    ## 70  108.58  98.01
    ## 71   82.79  77.10
    ## 72   75.50  88.69
    ## 73  115.30 112.30
    ## 74   87.36  83.57
    ## 75  116.55     NA
    ## 76   72.86  69.60
    ## 77  121.79 114.65
    ## 78   76.19  77.82
    ## 79   89.77  83.97
    ## 80   76.32  71.00
    ## 81   79.07  89.79
    ## 82   66.92  52.63
    ## 83   98.22  93.87
    ## 84  125.23  93.59
    ## 85  103.39  99.32
    ## 86  123.48 109.38
    ## 87  103.35 106.61
    ## 88   83.69  74.08
    ## 89  102.28  94.10
    ## 90   86.69  71.40
    ## 91   82.32  65.58
    ## 92   87.43  88.57
    ## 93   93.01  89.38
    ## 94  103.65  84.78
    ## 95   74.76  71.09
    ## 96   78.55  75.51
    ## 97   92.88 110.48
    ## 98  134.99 115.72
    ## 99   99.88  84.44
    ## 100 108.88  84.18
    ## 101  80.05  72.54
    ## 102 112.89  97.74
    ## 103  89.29     NA
    ## 104  97.61  89.50
    ## 105  98.17     NA
    ## 106 116.57 100.37
    ## 107  78.60  81.93
    ## 108  87.49  91.31
    ## 109  71.30  61.55
    ## 110 109.69  98.11
    ## 111  76.08  61.75
    ## 112  97.89  93.27
    ## 113  80.46  80.45
    ## 114 108.44  72.41
    ## 115  86.10 102.81
    ## 116  64.29  71.31
    ## 117  93.10  78.54
    ## 118 112.60 117.38
    ## 119 134.45 123.41
    ## 120  82.95  75.19
    ## 121  71.42  66.75
    ## 122 107.49 110.67
    ## 123  73.56  70.60
    ## 124  83.31  73.88
    ## 125  87.28  74.76
    ## 126 109.91 101.99
    ## 127 122.21 112.79
    ## 128  83.11  74.42
    ## 129  96.12  82.71
    ## 130  84.74  75.86
    ## 131 100.11  85.45
    ## 132  68.28  59.69
    ## 133  87.74  47.66
    ## 134  98.50  90.68
    ## 135 103.28  89.56
    ## 136 104.39  90.90
    ## 137  97.45  90.88
    ## 138 115.69 116.95
    ## 139  89.98  93.88
    ## 140 111.86  91.58
    ## 141  73.36  74.39
    ## 142  76.25  69.33
    ## 143  91.94  64.43
    ## 144 111.34 101.05
    ## 145  76.77  96.66
    ## 146 105.66  81.52
    ## 147 121.05     NA
    ## 148  70.18  62.08
    ## 149  74.67  72.66
    ## 150  93.24  76.90
    ## 151  89.31  87.40
    ## 152  96.93  84.35
    ## 153  77.32     NA
    ## 154  97.49  84.98
    ## 155 107.13  86.62
    ## 156  82.60     NA
    ## 157 105.98  83.66
    ## 158  76.44  58.99
    ## 159  76.17  95.94
    ## 160  93.16  62.58
    ## 161 114.95 111.07
    ## 162 117.14  92.12
    ## 163  56.87  28.46
    ## 164  37.78     NA
    ## 165 102.08     NA
    ## 166  95.31  90.81
    ## 167 104.05 116.42
    ## 168  99.39  88.69
    ## 169  88.58 101.30
    ## 170 121.87 101.74
    ## 171 104.64  85.00
    ## 172 102.23  86.94
    ## 173  75.43  70.27
    ## 174  98.21  93.69
    ## 175  92.39 112.08
    ## 176 106.37  95.02
    ## 177 124.61     NA
    ## 178 111.10  99.69
    ## 179  62.87  39.65
    ## 180 103.35  74.90
    ## 181  85.41 102.96
    ## 182  83.43  71.88
    ## 183 105.87 107.52
    ## 184  62.86  36.52
    ## 185  88.13  79.95
    ## 186  59.29  55.50
    ## 187  99.02  58.61
    ## 188  96.20  75.23
    ## 189 118.96  92.00
    ## 190  78.16     NA
    ## 191  76.09  76.73
    ## 192 135.16 127.01
    ## 193  96.40  71.86
    ## 194  82.48  87.11
    ## 195  67.19  51.26
    ## 196 112.43 116.00
    ## 197  97.43  95.93
    ## 198  70.59  50.96
    ## 199 100.00 100.91
    ## 200  72.67  62.30
    ## 201 101.67  98.06
    ## 202 106.74 109.38
    ## 203 121.38 102.47
    ## 204 106.84  92.96
    ## 205  58.62  50.32
    ## 206  99.76  85.78
    ## 207  97.61 101.71
    ## 208  98.62  81.21
    ## 209  93.33 101.63
    ## 210 109.89  96.49
    ## 211 128.43     NA
    ## 212 131.56 137.55
    ## 213  99.09 102.05
    ## 214 105.89  93.77
    ## 215 128.02 113.85
    ## 216 131.88  86.40
    ## 217 124.62 107.06
    ## 218 111.28 121.58
    ## 219  72.70  74.93
    ## 220 100.39 104.56
    ## 221  94.71  72.09
    ## 222 121.59  99.94
    ## 223  98.27  82.02
    ## 224 100.87  96.99
    ## 225 120.19     NA
    ## 226 129.36     NA
    ## 227  90.16  80.93
    ## 228  81.29  84.14
    ## 229  78.52  85.37
    ## 230  69.90  61.53
    ## 231 100.33 114.96
    ## 232  82.49  63.62
    ## 233  76.70 101.48
    ## 234 110.17 111.86
    ## 235  92.49  90.07
    ## 236  94.31  85.64
    ## 237 102.25 110.43
    ## 238 117.37 117.26
    ## 239  80.82  68.92
    ## 240 100.80 106.01

``` r
#  match patterns with ?grep
grep(pattern="J0050", x=dat$SUBJID)
```

    ## [1] 50

``` r
grep(pattern="J0100", x=dat$SUBJID)
```

    ## [1] 100

``` r
grep(pattern="J0200", x=dat$SUBJID)
```

    ## [1] 200

``` r
# match multiple patterns using the | operator
grep(pattern="J0050|J0100|J0200", x=dat$SUBJID, fixed=FALSE)
```

    ## [1]  50 100 200

``` r
# "grepl" will provide indicator
dat[grep(pattern="J0050|J0100|J0200", x=dat$SUBJID, fixed=FALSE),]
```

    ##     SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 50   J0050            0         1647         2605    JHS-Only  56.1  60.6  63.2
    ## 100  J0100            0           NA         3014    JHS-Only  63.6    NA  71.9
    ## 200  J0200            0         2674         3712 Shared ARIC  66.6  73.9  76.7
    ##     maleV1 waistV1 waistV2 waistV3  sbpV1 sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 50    Male     107  109.22  106.68 132.08   132   137     5.6     5.8     5.7
    ## 100 Female     149      NA  108.00 125.66    NA   137     6.3      NA     6.1
    ## 200 Female      85   71.12   89.00 136.67   180   136     5.4     5.7     5.4
    ##     eGFRV1 eGFRV3
    ## 50   83.07  78.76
    ## 100 108.88  84.18
    ## 200  72.67  62.30

``` r
# use which when logical vector will return NA
dat$sbpV3>140
```

    ##   [1] FALSE  TRUE FALSE  TRUE FALSE  TRUE  TRUE FALSE FALSE FALSE FALSE FALSE
    ##  [13] FALSE FALSE FALSE FALSE FALSE  TRUE  TRUE    NA FALSE    NA  TRUE FALSE
    ##  [25]  TRUE FALSE FALSE FALSE FALSE FALSE    NA FALSE FALSE FALSE FALSE  TRUE
    ##  [37]    NA    NA FALSE FALSE FALSE  TRUE FALSE FALSE    NA  TRUE  TRUE FALSE
    ##  [49] FALSE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE  TRUE FALSE FALSE FALSE
    ##  [61] FALSE FALSE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE FALSE
    ##  [73] FALSE FALSE    NA FALSE FALSE FALSE FALSE  TRUE FALSE FALSE  TRUE FALSE
    ##  [85] FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ##  [97] FALSE FALSE FALSE FALSE  TRUE FALSE    NA FALSE    NA FALSE FALSE FALSE
    ## [109] FALSE FALSE  TRUE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [121] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE  TRUE  TRUE FALSE  TRUE
    ## [133] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [145] FALSE  TRUE    NA FALSE FALSE FALSE FALSE FALSE    NA FALSE FALSE FALSE
    ## [157] FALSE FALSE  TRUE FALSE FALSE FALSE FALSE    NA    NA FALSE FALSE FALSE
    ## [169] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
    ## [181]  TRUE FALSE FALSE FALSE FALSE  TRUE FALSE FALSE FALSE    NA FALSE  TRUE
    ## [193] FALSE FALSE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE  TRUE FALSE
    ## [205] FALSE FALSE FALSE FALSE FALSE FALSE    NA FALSE FALSE FALSE FALSE FALSE
    ## [217] FALSE FALSE  TRUE FALSE  TRUE FALSE FALSE FALSE    NA    NA FALSE FALSE
    ## [229]  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE

``` r
dat[dat$sbpV3>140,]  # create NA row;
```

    ##       SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2
    ## 2      J0002            0         1537         2569    JHS-Only  71.5  75.7
    ## 4      J0004            0         1597         3493 Shared ARIC  59.2  63.6
    ## 6      J0006            0         1618         2703    JHS-Only  63.9  68.3
    ## 7      J0007            0         1642         3303 Shared ARIC  67.7  72.2
    ## 18     J0018            0         1841         2951    JHS-Only  54.2  59.2
    ## 19     J0019            0         1554         2907    JHS-Only  50.9  55.2
    ## NA      <NA>           NA           NA           NA        <NA>    NA    NA
    ## NA.1    <NA>           NA           NA           NA        <NA>    NA    NA
    ## 23     J0023            0         1751         3602 Shared ARIC  73.1  77.9
    ## 25     J0025            0         1544         2558    JHS-Only  81.4  85.6
    ## NA.2    <NA>           NA           NA           NA        <NA>    NA    NA
    ## 36     J0036            0         1853         2931    JHS-Only  80.5  85.6
    ## NA.3    <NA>           NA           NA           NA        <NA>    NA    NA
    ## NA.4    <NA>           NA           NA           NA        <NA>    NA    NA
    ## 42     J0042            0         1580         2678    JHS-Only  54.2  58.5
    ## NA.5    <NA>           NA           NA           NA        <NA>    NA    NA
    ## 46     J0046            0         1392         2583 Shared ARIC  80.0  83.8
    ## 47     J0047            0           NA         2787    JHS-Only  47.6    NA
    ## 54     J0054            0         1569         2952    JHS-Only  54.2  58.5
    ## 57     J0057            0         1707         3052 Shared ARIC  75.7  80.4
    ## 67     J0067            0         2918         3658    JHS-Only  38.5  46.5
    ## NA.6    <NA>           NA           NA           NA        <NA>    NA    NA
    ## 80     J0080            0         1667         2767 Shared ARIC  78.2  82.8
    ## 83     J0083            0         1612         2843    JHS-Only  42.3  46.7
    ## 89     J0089            0         1558         2654    JHS-Only  57.4  61.7
    ## 101    J0101            0         2076         3375    JHS-Only  52.8  58.5
    ## NA.7    <NA>           NA           NA           NA        <NA>    NA    NA
    ## NA.8    <NA>           NA           NA           NA        <NA>    NA    NA
    ## 111    J0111            0         1841         2940    JHS-Only  78.7  83.7
    ## 112    J0112            0         1725         2999    JHS-Only  42.8  47.5
    ## 129    J0129            0         1407         2583    JHS-Only  45.4  49.2
    ## 130    J0130            0         1639         3317    JHS-Only  63.3  67.8
    ## 132    J0132            0         1592         2703 Shared ARIC  75.4  79.8
    ## 146    J0146            0         1597         2695    JHS-Only  54.0  58.4
    ## NA.9    <NA>           NA           NA           NA        <NA>    NA    NA
    ## NA.10   <NA>           NA           NA           NA        <NA>    NA    NA
    ## 159    J0159            0         1637         2952    JHS-Only  43.0  47.5
    ## NA.11   <NA>           NA           NA           NA        <NA>    NA    NA
    ## NA.12   <NA>           NA           NA           NA        <NA>    NA    NA
    ## 181    J0181            0         2053         2913    JHS-Only  52.1  57.8
    ## 186    J0186            0         1603         2715 Shared ARIC  63.4  67.8
    ## NA.13   <NA>           NA           NA           NA        <NA>    NA    NA
    ## 192    J0192            0         1728         3423    JHS-Only  40.3  45.1
    ## 195    J0195            0         2259         3667    JHS-Only  59.2  65.4
    ## 203    J0203            0         1715         2788    JHS-Only  45.9  50.6
    ## NA.14   <NA>           NA           NA           NA        <NA>    NA    NA
    ## 219    J0219            0         1734         2798    JHS-Only  66.5  71.2
    ## 221    J0221            0         1607         2703 Shared ARIC  68.4  72.8
    ## NA.15   <NA>           NA           NA           NA        <NA>    NA    NA
    ## NA.16   <NA>           NA           NA           NA        <NA>    NA    NA
    ## 229    J0229            0         1596         2944    JHS-Only  55.5  59.9
    ##       ageV3 maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2
    ## 2      78.5 Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8
    ## 4      68.8   Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5
    ## 6      71.3 Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7
    ## 7      76.7 Female     118  116.84  126.30 141.25 154.00   143     6.9     7.2
    ## 18     62.2 Female     132  129.54  134.00 133.00 110.00   141     5.6     5.6
    ## 19     58.9   Male     122  119.38  124.40 136.67  93.00   148     6.5     6.4
    ## NA       NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## NA.1     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 23     83.0 Female     107  101.60   97.00 142.17 141.25   177     5.5     6.0
    ## 25     88.4   Male      96   99.06   95.25 133.92 127.00   145     5.5     5.7
    ## NA.2     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 36     88.5 Female     112  109.22  106.00 137.58 144.00   149     5.8     5.9
    ## NA.3     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## NA.4     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 42     61.5   Male      95   97.79  101.60 113.74 136.67   147     6.1     6.2
    ## NA.5     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 46     87.1   Male      91   93.98   78.74 155.00 155.00   151     5.8     5.7
    ## 47     55.2   Male     104      NA      NA 168.76     NA   195     9.5      NA
    ## 54     62.3   Male     113  109.22  111.76 152.25 144.00   174     6.1     6.9
    ## 57     84.1 Female      79   86.36   81.28 156.84 153.17   183    10.8      NA
    ## 67     48.5   Male      77   80.01   72.39 119.25 134.00   152     4.9     5.4
    ## NA.6     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 80     85.8 Female     106  111.76  114.30 130.25 148.59   141     5.9      NA
    ## 83     50.1 Female      94   88.90   93.98 119.25 126.58   162     5.2     5.8
    ## 89     64.7 Female     106  106.68  106.68 127.50 156.00   143     5.6     5.6
    ## 101    62.0 Female     106  119.38  118.00 160.51 135.00   172     5.5     5.6
    ## NA.7     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## NA.8     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 111    86.7 Female     102  104.14   93.98 142.17 149.50   170     5.7     6.4
    ## 112    51.0 Female      93   93.98   88.90 138.50 135.75   158     5.8     6.2
    ## 129    52.4 Female      86   93.98   98.50 110.08 123.00   142     4.6     5.0
    ## 130    72.4 Female      95   91.44   89.30 148.59 149.50   146     5.3     5.6
    ## 132    82.8 Female      84   86.36   91.44 130.25 135.75   147     5.0     5.9
    ## 146    61.4   Male      98  110.49  115.57 119.25 117.41   146     9.2     9.5
    ## NA.9     NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## NA.10    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 159    51.1 Female      90   96.52   97.79 174.26 231.11   166     5.6      NA
    ## NA.11    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## NA.12    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 181    60.1   Male     104   91.44  104.14 166.92 133.00   143     5.7     5.6
    ## 186    70.8 Female      90   99.06   96.52 146.75 146.00   143     6.0     5.8
    ## NA.13    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 192    49.7 Female     113  109.22  117.00 142.17 139.00   157     5.1     5.0
    ## 195    69.3   Male     103   97.79   95.30 131.17 127.00   168     7.0     5.3
    ## 203    53.6   Male      99  101.60  106.68 136.67 123.00   145     5.4     5.7
    ## NA.14    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 219    74.2 Female     103  109.22  106.68 135.75 157.00   165     5.7     6.0
    ## 221    75.8 Female     114  104.14   99.06 144.92 150.00   181     6.0     6.1
    ## NA.15    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## NA.16    NA   <NA>      NA      NA      NA     NA     NA    NA      NA      NA
    ## 229    63.6 Female      90  106.68  101.60 112.83 129.33   141     5.7     6.0
    ##       HbA1cV3 eGFRV1 eGFRV3
    ## 2         6.1  92.67  79.14
    ## 4         4.7 101.83  99.44
    ## 6         5.9 108.67 101.31
    ## 7         8.2  47.57  33.20
    ## 18        5.6 104.66  51.79
    ## 19       10.1  95.94  73.35
    ## NA         NA     NA     NA
    ## NA.1       NA     NA     NA
    ## 23        6.1  79.09  64.19
    ## 25        5.8  87.17  66.71
    ## NA.2       NA     NA     NA
    ## 36        5.9 101.95  92.49
    ## NA.3       NA     NA     NA
    ## NA.4       NA     NA     NA
    ## 42        6.1  93.76  95.70
    ## NA.5       NA     NA     NA
    ## 46        5.5  58.08  48.99
    ## 47        7.1  88.20  81.59
    ## 54        7.6  76.24  76.81
    ## 57        9.0  68.15  40.57
    ## 67        5.3 117.79  93.22
    ## NA.6       NA     NA     NA
    ## 80        6.0  76.32  71.00
    ## 83        6.0  98.22  93.87
    ## 89        5.6 102.28  94.10
    ## 101       5.8  80.05  72.54
    ## NA.7       NA     NA     NA
    ## NA.8       NA     NA     NA
    ## 111       7.0  76.08  61.75
    ## 112       6.0  97.89  93.27
    ## 129       5.2  96.12  82.71
    ## 130       5.4  84.74  75.86
    ## 132       6.0  68.28  59.69
    ## 146       7.9 105.66  81.52
    ## NA.9       NA     NA     NA
    ## NA.10      NA     NA     NA
    ## 159       5.5  76.17  95.94
    ## NA.11      NA     NA     NA
    ## NA.12      NA     NA     NA
    ## 181       5.5  85.41 102.96
    ## 186       6.2  59.29  55.50
    ## NA.13      NA     NA     NA
    ## 192       5.1 135.16 127.01
    ## 195       6.2  67.19  51.26
    ## 203       5.8 121.38 102.47
    ## NA.14      NA     NA     NA
    ## 219       6.1  72.70  74.93
    ## 221       6.0  94.71  72.09
    ## NA.15      NA     NA     NA
    ## NA.16      NA     NA     NA
    ## 229       6.2  78.52  85.37

``` r
which(dat$sbpV3>140)  #pair the logic to choose what you want 
```

    ##  [1]   2   4   6   7  18  19  23  25  36  42  46  47  54  57  67  80  83  89 101
    ## [20] 111 112 129 130 132 146 159 181 186 192 195 203 219 221 229

``` r
dat[which(dat$sbpV3>140),]# can be paired with which
```

    ##     SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 2    J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 4    J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 6    J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ## 7    J0007            0         1642         3303 Shared ARIC  67.7  72.2  76.7
    ## 18   J0018            0         1841         2951    JHS-Only  54.2  59.2  62.2
    ## 19   J0019            0         1554         2907    JHS-Only  50.9  55.2  58.9
    ## 23   J0023            0         1751         3602 Shared ARIC  73.1  77.9  83.0
    ## 25   J0025            0         1544         2558    JHS-Only  81.4  85.6  88.4
    ## 36   J0036            0         1853         2931    JHS-Only  80.5  85.6  88.5
    ## 42   J0042            0         1580         2678    JHS-Only  54.2  58.5  61.5
    ## 46   J0046            0         1392         2583 Shared ARIC  80.0  83.8  87.1
    ## 47   J0047            0           NA         2787    JHS-Only  47.6    NA  55.2
    ## 54   J0054            0         1569         2952    JHS-Only  54.2  58.5  62.3
    ## 57   J0057            0         1707         3052 Shared ARIC  75.7  80.4  84.1
    ## 67   J0067            0         2918         3658    JHS-Only  38.5  46.5  48.5
    ## 80   J0080            0         1667         2767 Shared ARIC  78.2  82.8  85.8
    ## 83   J0083            0         1612         2843    JHS-Only  42.3  46.7  50.1
    ## 89   J0089            0         1558         2654    JHS-Only  57.4  61.7  64.7
    ## 101  J0101            0         2076         3375    JHS-Only  52.8  58.5  62.0
    ## 111  J0111            0         1841         2940    JHS-Only  78.7  83.7  86.7
    ## 112  J0112            0         1725         2999    JHS-Only  42.8  47.5  51.0
    ## 129  J0129            0         1407         2583    JHS-Only  45.4  49.2  52.4
    ## 130  J0130            0         1639         3317    JHS-Only  63.3  67.8  72.4
    ## 132  J0132            0         1592         2703 Shared ARIC  75.4  79.8  82.8
    ## 146  J0146            0         1597         2695    JHS-Only  54.0  58.4  61.4
    ## 159  J0159            0         1637         2952    JHS-Only  43.0  47.5  51.1
    ## 181  J0181            0         2053         2913    JHS-Only  52.1  57.8  60.1
    ## 186  J0186            0         1603         2715 Shared ARIC  63.4  67.8  70.8
    ## 192  J0192            0         1728         3423    JHS-Only  40.3  45.1  49.7
    ## 195  J0195            0         2259         3667    JHS-Only  59.2  65.4  69.3
    ## 203  J0203            0         1715         2788    JHS-Only  45.9  50.6  53.6
    ## 219  J0219            0         1734         2798    JHS-Only  66.5  71.2  74.2
    ## 221  J0221            0         1607         2703 Shared ARIC  68.4  72.8  75.8
    ## 229  J0229            0         1596         2944    JHS-Only  55.5  59.9  63.6
    ##     maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 2   Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 4     Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 6   Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ## 7   Female     118  116.84  126.30 141.25 154.00   143     6.9     7.2     8.2
    ## 18  Female     132  129.54  134.00 133.00 110.00   141     5.6     5.6     5.6
    ## 19    Male     122  119.38  124.40 136.67  93.00   148     6.5     6.4    10.1
    ## 23  Female     107  101.60   97.00 142.17 141.25   177     5.5     6.0     6.1
    ## 25    Male      96   99.06   95.25 133.92 127.00   145     5.5     5.7     5.8
    ## 36  Female     112  109.22  106.00 137.58 144.00   149     5.8     5.9     5.9
    ## 42    Male      95   97.79  101.60 113.74 136.67   147     6.1     6.2     6.1
    ## 46    Male      91   93.98   78.74 155.00 155.00   151     5.8     5.7     5.5
    ## 47    Male     104      NA      NA 168.76     NA   195     9.5      NA     7.1
    ## 54    Male     113  109.22  111.76 152.25 144.00   174     6.1     6.9     7.6
    ## 57  Female      79   86.36   81.28 156.84 153.17   183    10.8      NA     9.0
    ## 67    Male      77   80.01   72.39 119.25 134.00   152     4.9     5.4     5.3
    ## 80  Female     106  111.76  114.30 130.25 148.59   141     5.9      NA     6.0
    ## 83  Female      94   88.90   93.98 119.25 126.58   162     5.2     5.8     6.0
    ## 89  Female     106  106.68  106.68 127.50 156.00   143     5.6     5.6     5.6
    ## 101 Female     106  119.38  118.00 160.51 135.00   172     5.5     5.6     5.8
    ## 111 Female     102  104.14   93.98 142.17 149.50   170     5.7     6.4     7.0
    ## 112 Female      93   93.98   88.90 138.50 135.75   158     5.8     6.2     6.0
    ## 129 Female      86   93.98   98.50 110.08 123.00   142     4.6     5.0     5.2
    ## 130 Female      95   91.44   89.30 148.59 149.50   146     5.3     5.6     5.4
    ## 132 Female      84   86.36   91.44 130.25 135.75   147     5.0     5.9     6.0
    ## 146   Male      98  110.49  115.57 119.25 117.41   146     9.2     9.5     7.9
    ## 159 Female      90   96.52   97.79 174.26 231.11   166     5.6      NA     5.5
    ## 181   Male     104   91.44  104.14 166.92 133.00   143     5.7     5.6     5.5
    ## 186 Female      90   99.06   96.52 146.75 146.00   143     6.0     5.8     6.2
    ## 192 Female     113  109.22  117.00 142.17 139.00   157     5.1     5.0     5.1
    ## 195   Male     103   97.79   95.30 131.17 127.00   168     7.0     5.3     6.2
    ## 203   Male      99  101.60  106.68 136.67 123.00   145     5.4     5.7     5.8
    ## 219 Female     103  109.22  106.68 135.75 157.00   165     5.7     6.0     6.1
    ## 221 Female     114  104.14   99.06 144.92 150.00   181     6.0     6.1     6.0
    ## 229 Female      90  106.68  101.60 112.83 129.33   141     5.7     6.0     6.2
    ##     eGFRV1 eGFRV3
    ## 2    92.67  79.14
    ## 4   101.83  99.44
    ## 6   108.67 101.31
    ## 7    47.57  33.20
    ## 18  104.66  51.79
    ## 19   95.94  73.35
    ## 23   79.09  64.19
    ## 25   87.17  66.71
    ## 36  101.95  92.49
    ## 42   93.76  95.70
    ## 46   58.08  48.99
    ## 47   88.20  81.59
    ## 54   76.24  76.81
    ## 57   68.15  40.57
    ## 67  117.79  93.22
    ## 80   76.32  71.00
    ## 83   98.22  93.87
    ## 89  102.28  94.10
    ## 101  80.05  72.54
    ## 111  76.08  61.75
    ## 112  97.89  93.27
    ## 129  96.12  82.71
    ## 130  84.74  75.86
    ## 132  68.28  59.69
    ## 146 105.66  81.52
    ## 159  76.17  95.94
    ## 181  85.41 102.96
    ## 186  59.29  55.50
    ## 192 135.16 127.01
    ## 195  67.19  51.26
    ## 203 121.38 102.47
    ## 219  72.70  74.93
    ## 221  94.71  72.09
    ## 229  78.52  85.37

``` r
# complete.cases returns logical indicator of non-missing rows
complete.cases(dat)  # to see whether there is NA items 
```

    ##   [1]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE FALSE
    ##  [13]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE FALSE  TRUE  TRUE
    ##  [25]  TRUE FALSE  TRUE  TRUE FALSE  TRUE FALSE  TRUE  TRUE  TRUE FALSE  TRUE
    ##  [37] FALSE FALSE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE FALSE  TRUE
    ##  [49]  TRUE  TRUE FALSE  TRUE  TRUE  TRUE FALSE  TRUE FALSE  TRUE  TRUE  TRUE
    ##  [61]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE
    ##  [73]  TRUE  TRUE FALSE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE  TRUE  TRUE  TRUE
    ##  [85]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE
    ##  [97]  TRUE  TRUE  TRUE FALSE  TRUE  TRUE FALSE  TRUE FALSE  TRUE  TRUE  TRUE
    ## [109]  TRUE  TRUE  TRUE  TRUE  TRUE FALSE FALSE  TRUE  TRUE  TRUE  TRUE  TRUE
    ## [121]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE
    ## [133]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE  TRUE  TRUE  TRUE  TRUE
    ## [145]  TRUE  TRUE FALSE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE FALSE FALSE
    ## [157]  TRUE  TRUE FALSE  TRUE  TRUE  TRUE  TRUE FALSE FALSE  TRUE  TRUE  TRUE
    ## [169]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE FALSE  TRUE
    ## [181]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE  TRUE
    ## [193]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE
    ## [205] FALSE FALSE  TRUE  TRUE  TRUE  TRUE FALSE  TRUE FALSE  TRUE  TRUE  TRUE
    ## [217]  TRUE  TRUE  TRUE FALSE  TRUE  TRUE  TRUE  TRUE FALSE FALSE  TRUE  TRUE
    ## [229]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE

``` r
which(complete.cases(dat))  
```

    ##   [1]   1   2   3   4   5   6   7   8   9  11  13  14  15  16  17  18  19  21
    ##  [19]  23  24  25  27  28  30  32  33  34  36  39  40  41  42  43  44  46  48
    ##  [37]  49  50  52  53  54  56  58  59  60  61  62  63  64  65  66  67  68  69
    ##  [55]  70  71  72  73  74  76  77  78  79  81  82  83  84  85  86  87  88  89
    ##  [73]  90  91  92  93  94  95  96  97  98  99 101 102 104 106 107 108 109 110
    ##  [91] 111 112 113 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130
    ## [109] 131 132 133 134 135 136 137 138 140 141 142 143 144 145 146 148 149 150
    ## [127] 151 152 154 157 158 160 161 162 163 166 167 168 169 170 171 172 173 174
    ## [145] 175 176 178 180 181 182 183 184 185 186 187 188 189 191 192 193 194 195
    ## [163] 196 197 198 199 200 201 202 203 204 207 208 209 210 212 214 215 216 217
    ## [181] 218 219 221 222 223 224 227 228 229 230 231 232 233 234 235 236 237 238
    ## [199] 239 240

``` r
datCC <- dat[complete.cases(dat), ]
datCC <- dat[which(complete.cases(dat)), ]
datCC <- na.omit(dat)
datCC <- dat[rowSums(is.na(dat))==0,]




#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# How to do in tidyverse
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

dat %>% 
  filter(SUBJID %in% c("J0050", "J0100", "J0200"))
```

    ##   SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1  J0050            0         1647         2605    JHS-Only  56.1  60.6  63.2
    ## 2  J0100            0           NA         3014    JHS-Only  63.6    NA  71.9
    ## 3  J0200            0         2674         3712 Shared ARIC  66.6  73.9  76.7
    ##   maleV1 waistV1 waistV2 waistV3  sbpV1 sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1   Male     107  109.22  106.68 132.08   132   137     5.6     5.8     5.7
    ## 2 Female     149      NA  108.00 125.66    NA   137     6.3      NA     6.1
    ## 3 Female      85   71.12   89.00 136.67   180   136     5.4     5.7     5.4
    ##   eGFRV1 eGFRV3
    ## 1  83.07  78.76
    ## 2 108.88  84.18
    ## 3  72.67  62.30

``` r
dat %>% 
  filter(sbpV3>140)
```

    ##    SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1   J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 2   J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 3   J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ## 4   J0007            0         1642         3303 Shared ARIC  67.7  72.2  76.7
    ## 5   J0018            0         1841         2951    JHS-Only  54.2  59.2  62.2
    ## 6   J0019            0         1554         2907    JHS-Only  50.9  55.2  58.9
    ## 7   J0023            0         1751         3602 Shared ARIC  73.1  77.9  83.0
    ## 8   J0025            0         1544         2558    JHS-Only  81.4  85.6  88.4
    ## 9   J0036            0         1853         2931    JHS-Only  80.5  85.6  88.5
    ## 10  J0042            0         1580         2678    JHS-Only  54.2  58.5  61.5
    ## 11  J0046            0         1392         2583 Shared ARIC  80.0  83.8  87.1
    ## 12  J0047            0           NA         2787    JHS-Only  47.6    NA  55.2
    ## 13  J0054            0         1569         2952    JHS-Only  54.2  58.5  62.3
    ## 14  J0057            0         1707         3052 Shared ARIC  75.7  80.4  84.1
    ## 15  J0067            0         2918         3658    JHS-Only  38.5  46.5  48.5
    ## 16  J0080            0         1667         2767 Shared ARIC  78.2  82.8  85.8
    ## 17  J0083            0         1612         2843    JHS-Only  42.3  46.7  50.1
    ## 18  J0089            0         1558         2654    JHS-Only  57.4  61.7  64.7
    ## 19  J0101            0         2076         3375    JHS-Only  52.8  58.5  62.0
    ## 20  J0111            0         1841         2940    JHS-Only  78.7  83.7  86.7
    ## 21  J0112            0         1725         2999    JHS-Only  42.8  47.5  51.0
    ## 22  J0129            0         1407         2583    JHS-Only  45.4  49.2  52.4
    ## 23  J0130            0         1639         3317    JHS-Only  63.3  67.8  72.4
    ## 24  J0132            0         1592         2703 Shared ARIC  75.4  79.8  82.8
    ## 25  J0146            0         1597         2695    JHS-Only  54.0  58.4  61.4
    ## 26  J0159            0         1637         2952    JHS-Only  43.0  47.5  51.1
    ## 27  J0181            0         2053         2913    JHS-Only  52.1  57.8  60.1
    ## 28  J0186            0         1603         2715 Shared ARIC  63.4  67.8  70.8
    ## 29  J0192            0         1728         3423    JHS-Only  40.3  45.1  49.7
    ## 30  J0195            0         2259         3667    JHS-Only  59.2  65.4  69.3
    ## 31  J0203            0         1715         2788    JHS-Only  45.9  50.6  53.6
    ## 32  J0219            0         1734         2798    JHS-Only  66.5  71.2  74.2
    ## 33  J0221            0         1607         2703 Shared ARIC  68.4  72.8  75.8
    ## 34  J0229            0         1596         2944    JHS-Only  55.5  59.9  63.6
    ##    maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1  Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 2    Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 3  Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ## 4  Female     118  116.84  126.30 141.25 154.00   143     6.9     7.2     8.2
    ## 5  Female     132  129.54  134.00 133.00 110.00   141     5.6     5.6     5.6
    ## 6    Male     122  119.38  124.40 136.67  93.00   148     6.5     6.4    10.1
    ## 7  Female     107  101.60   97.00 142.17 141.25   177     5.5     6.0     6.1
    ## 8    Male      96   99.06   95.25 133.92 127.00   145     5.5     5.7     5.8
    ## 9  Female     112  109.22  106.00 137.58 144.00   149     5.8     5.9     5.9
    ## 10   Male      95   97.79  101.60 113.74 136.67   147     6.1     6.2     6.1
    ## 11   Male      91   93.98   78.74 155.00 155.00   151     5.8     5.7     5.5
    ## 12   Male     104      NA      NA 168.76     NA   195     9.5      NA     7.1
    ## 13   Male     113  109.22  111.76 152.25 144.00   174     6.1     6.9     7.6
    ## 14 Female      79   86.36   81.28 156.84 153.17   183    10.8      NA     9.0
    ## 15   Male      77   80.01   72.39 119.25 134.00   152     4.9     5.4     5.3
    ## 16 Female     106  111.76  114.30 130.25 148.59   141     5.9      NA     6.0
    ## 17 Female      94   88.90   93.98 119.25 126.58   162     5.2     5.8     6.0
    ## 18 Female     106  106.68  106.68 127.50 156.00   143     5.6     5.6     5.6
    ## 19 Female     106  119.38  118.00 160.51 135.00   172     5.5     5.6     5.8
    ## 20 Female     102  104.14   93.98 142.17 149.50   170     5.7     6.4     7.0
    ## 21 Female      93   93.98   88.90 138.50 135.75   158     5.8     6.2     6.0
    ## 22 Female      86   93.98   98.50 110.08 123.00   142     4.6     5.0     5.2
    ## 23 Female      95   91.44   89.30 148.59 149.50   146     5.3     5.6     5.4
    ## 24 Female      84   86.36   91.44 130.25 135.75   147     5.0     5.9     6.0
    ## 25   Male      98  110.49  115.57 119.25 117.41   146     9.2     9.5     7.9
    ## 26 Female      90   96.52   97.79 174.26 231.11   166     5.6      NA     5.5
    ## 27   Male     104   91.44  104.14 166.92 133.00   143     5.7     5.6     5.5
    ## 28 Female      90   99.06   96.52 146.75 146.00   143     6.0     5.8     6.2
    ## 29 Female     113  109.22  117.00 142.17 139.00   157     5.1     5.0     5.1
    ## 30   Male     103   97.79   95.30 131.17 127.00   168     7.0     5.3     6.2
    ## 31   Male      99  101.60  106.68 136.67 123.00   145     5.4     5.7     5.8
    ## 32 Female     103  109.22  106.68 135.75 157.00   165     5.7     6.0     6.1
    ## 33 Female     114  104.14   99.06 144.92 150.00   181     6.0     6.1     6.0
    ## 34 Female      90  106.68  101.60 112.83 129.33   141     5.7     6.0     6.2
    ##    eGFRV1 eGFRV3
    ## 1   92.67  79.14
    ## 2  101.83  99.44
    ## 3  108.67 101.31
    ## 4   47.57  33.20
    ## 5  104.66  51.79
    ## 6   95.94  73.35
    ## 7   79.09  64.19
    ## 8   87.17  66.71
    ## 9  101.95  92.49
    ## 10  93.76  95.70
    ## 11  58.08  48.99
    ## 12  88.20  81.59
    ## 13  76.24  76.81
    ## 14  68.15  40.57
    ## 15 117.79  93.22
    ## 16  76.32  71.00
    ## 17  98.22  93.87
    ## 18 102.28  94.10
    ## 19  80.05  72.54
    ## 20  76.08  61.75
    ## 21  97.89  93.27
    ## 22  96.12  82.71
    ## 23  84.74  75.86
    ## 24  68.28  59.69
    ## 25 105.66  81.52
    ## 26  76.17  95.94
    ## 27  85.41 102.96
    ## 28  59.29  55.50
    ## 29 135.16 127.01
    ## 30  67.19  51.26
    ## 31 121.38 102.47
    ## 32  72.70  74.93
    ## 33  94.71  72.09
    ## 34  78.52  85.37

``` r
dat %>% na.omit()
```

    ##     SUBJID DaysFromV1V1 DaysFromV1V2 DaysFromV1V3      ARICV1 ageV1 ageV2 ageV3
    ## 1    J0001            0         1730         2981 Shared ARIC  58.8  63.5  66.9
    ## 2    J0002            0         1537         2569    JHS-Only  71.5  75.7  78.5
    ## 3    J0003            0         1526         2739 Shared ARIC  73.1  77.3  80.6
    ## 4    J0004            0         1597         3493 Shared ARIC  59.2  63.6  68.8
    ## 5    J0005            0         1883         2944    JHS-Only  49.7  54.8  57.7
    ## 6    J0006            0         1618         2703    JHS-Only  63.9  68.3  71.3
    ## 7    J0007            0         1642         3303 Shared ARIC  67.7  72.2  76.7
    ## 8    J0008            0         1607         3315    JHS-Only  34.0  38.4  43.1
    ## 9    J0009            0         1966         2933    JHS-Only  50.8  56.2  58.9
    ## 11   J0011            0         1596         2716    JHS-Only  47.4  51.7  54.8
    ## 13   J0013            0         1648         3162    JHS-Only  55.6  60.1  64.2
    ## 14   J0014            0         1662         2624    JHS-Only  42.4  46.9  49.6
    ## 15   J0015            0         1336         2611    JHS-Only  43.2  46.9  50.4
    ## 16   J0016            0         1621         3296    JHS-Only  52.7  57.2  61.8
    ## 17   J0017            0         1968         3375    JHS-Only  44.0  49.3  53.2
    ## 18   J0018            0         1841         2951    JHS-Only  54.2  59.2  62.2
    ## 19   J0019            0         1554         2907    JHS-Only  50.9  55.2  58.9
    ## 21   J0021            0         1549         2727 Shared ARIC  71.9  76.2  79.4
    ## 23   J0023            0         1751         3602 Shared ARIC  73.1  77.9  83.0
    ## 24   J0024            0         1617         2583    JHS-Only  57.5  62.0  64.6
    ## 25   J0025            0         1544         2558    JHS-Only  81.4  85.6  88.4
    ## 27   J0027            0         1987         3380    JHS-Only  39.8  45.2  49.0
    ## 28   J0028            0         1550         2650    JHS-Only  45.0  49.3  52.3
    ## 30   J0030            0         1659         3106    JHS-Only  51.1  55.7  59.7
    ## 32   J0032            0         1471         2843 Shared ARIC  61.6  65.6  69.4
    ## 33   J0033            0         1672         2923    JHS-Only  44.1  48.7  52.1
    ## 34   J0034            0         1853         2972    JHS-Only  49.4  54.5  57.6
    ## 36   J0036            0         1853         2931    JHS-Only  80.5  85.6  88.5
    ## 39   J0039            0         1350         2651    JHS-Only  66.2  69.9  73.4
    ## 40   J0040            0         1758         3300    JHS-Only  28.4  33.2  37.5
    ## 41   J0041            0         1578         2713    JHS-Only  42.0  46.3  49.4
    ## 42   J0042            0         1580         2678    JHS-Only  54.2  58.5  61.5
    ## 43   J0043            0         1750         2988 Shared ARIC  74.8  79.6  83.0
    ## 44   J0044            0         1856         2926    JHS-Only  39.0  44.0  47.0
    ## 46   J0046            0         1392         2583 Shared ARIC  80.0  83.8  87.1
    ## 48   J0048            0         1603         2667    JHS-Only  48.8  53.2  56.1
    ## 49   J0049            0         1774         2979 Shared ARIC  72.6  77.4  80.7
    ## 50   J0050            0         1647         2605    JHS-Only  56.1  60.6  63.2
    ## 52   J0052            0         1515         2654 Shared ARIC  64.3  68.4  71.5
    ## 53   J0053            0         1554         2384    JHS-Only  36.7  41.0  43.3
    ## 54   J0054            0         1569         2952    JHS-Only  54.2  58.5  62.3
    ## 56   J0056            0         1563         2668    JHS-Only  55.7  60.0  63.0
    ## 58   J0058            0         1389         2585    JHS-Only  44.2  48.0  51.2
    ## 59   J0059            0         2090         3186 Shared ARIC  63.9  69.6  72.6
    ## 60   J0060            0         1545         2947    JHS-Only  49.4  53.6  57.5
    ## 61   J0061            0         2744         4060    JHS-Only  40.3  47.8  51.4
    ## 62   J0062            0         1769         2927    JHS-Only  51.2  56.0  59.2
    ## 63   J0063            0         1671         2786    JHS-Only  55.0  59.6  62.7
    ## 64   J0064            0         1711         2936    JHS-Only  46.7  51.3  54.7
    ## 65   J0065            0         1385         2574    JHS-Only  31.6  35.4  38.6
    ## 66   J0066            0         1794         3056 Shared ARIC  58.8  63.7  67.2
    ## 67   J0067            0         2918         3658    JHS-Only  38.5  46.5  48.5
    ## 68   J0068            0         1570         2667    JHS-Only  43.3  47.6  50.6
    ## 69   J0069            0         1640         3047    JHS-Only  45.0  49.5  53.3
    ## 70   J0070            0         1528         2928    JHS-Only  50.1  54.3  58.1
    ## 71   J0071            0         1599         2707 Shared ARIC  66.6  71.0  74.0
    ## 72   J0072            0         1574         3022    JHS-Only  42.6  46.9  50.9
    ## 73   J0073            0         1453         2576    JHS-Only  53.2  57.2  60.3
    ## 74   J0074            0         1808         2691    JHS-Only  48.9  53.9  56.3
    ## 76   J0076            0         1479         2576 Shared ARIC  60.7  64.8  67.8
    ## 77   J0077            0         1513         2697    JHS-Only  47.7  51.8  55.0
    ## 78   J0078            0         1541         2643    JHS-Only  43.0  47.2  50.3
    ## 79   J0079            0         1522         2757    JHS-Only  45.1  49.2  52.6
    ## 81   J0081            0         1607         2730    JHS-Only  63.1  67.5  70.6
    ## 82   J0082            0         1720         2981 Shared ARIC  61.5  66.2  69.6
    ## 83   J0083            0         1612         2843    JHS-Only  42.3  46.7  50.1
    ## 84   J0084            0         2011         2964    JHS-Only  28.6  34.1  36.7
    ## 85   J0085            0         1603         2706    JHS-Only  40.3  44.6  47.7
    ## 86   J0086            0         1730         2935    JHS-Only  43.5  48.2  51.5
    ## 87   J0087            0         1601         2941    JHS-Only  56.0  60.3  64.0
    ## 88   J0088            0         1571         2670    JHS-Only  46.5  50.8  53.8
    ## 89   J0089            0         1558         2654    JHS-Only  57.4  61.7  64.7
    ## 90   J0090            0         1585         2697 Shared ARIC  65.3  69.7  72.7
    ## 91   J0091            0         1791         2913    JHS-Only  57.4  62.3  65.4
    ## 92   J0092            0         1848         2986    JHS-Only  40.2  45.3  48.4
    ## 93   J0093            0         1810         3310    JHS-Only  50.1  55.0  59.1
    ## 94   J0094            0         1561         2558    JHS-Only  55.5  59.8  62.6
    ## 95   J0095            0         2171         3235    JHS-Only  45.7  51.7  54.6
    ## 96   J0096            0         1763         2947    JHS-Only  55.5  60.3  63.5
    ## 97   J0097            0         1622         2924    JHS-Only  50.3  54.7  58.3
    ## 98   J0098            0         2145         3321    JHS-Only  33.0  38.9  42.1
    ## 99   J0099            0         1609         3205    JHS-Only  62.0  66.4  70.8
    ## 101  J0101            0         2076         3375    JHS-Only  52.8  58.5  62.0
    ## 102  J0102            0         1470         2583    JHS-Only  44.6  48.6  51.6
    ## 104  J0104            0         1735         2833 Shared ARIC  64.1  68.8  71.8
    ## 106  J0106            0         1637         2921    JHS-Only  38.8  43.3  46.8
    ## 107  J0107            0         1928         3652    JHS-Only  55.4  60.7  65.4
    ## 108  J0108            0         1727         2784    JHS-Only  48.7  53.4  56.3
    ## 109  J0109            0         1938         3393 Shared ARIC  63.8  69.1  73.1
    ## 110  J0110            0         1499         2766    JHS-Only  48.7  52.8  56.2
    ## 111  J0111            0         1841         2940    JHS-Only  78.7  83.7  86.7
    ## 112  J0112            0         1725         2999    JHS-Only  42.8  47.5  51.0
    ## 113  J0113            0         1562         2661    JHS-Only  60.6  64.9  67.9
    ## 116  J0116            0         2240         2947    JHS-Only  53.4  59.6  61.5
    ## 117  J0117            0         1585         2936    JHS-Only  49.9  54.3  58.0
    ## 118  J0118            0         1510         2650    JHS-Only  43.8  47.9  51.0
    ## 119  J0119            0         1703         3265    JHS-Only  33.6  38.2  42.5
    ## 120  J0120            0         1568         2669 Shared ARIC  66.4  70.6  73.7
    ## 121  J0121            0         1589         2690 Shared ARIC  63.6  67.9  70.9
    ## 122  J0122            0         1675         2799    JHS-Only  50.4  55.0  58.0
    ## 123  J0123            0         1686         3030 Shared ARIC  64.8  69.4  73.1
    ## 124  J0124            0         1653         2574    JHS-Only  47.1  51.6  54.2
    ## 125  J0125            0         1646         2599    JHS-Only  49.1  53.6  56.2
    ## 126  J0126            0         1935         3192 Shared ARIC  62.3  67.6  71.0
    ## 127  J0127            0         1885         2978    JHS-Only  47.2  52.3  55.3
    ## 128  J0128            0         1577         2871    JHS-Only  56.0  60.4  63.9
    ## 129  J0129            0         1407         2583    JHS-Only  45.4  49.2  52.4
    ## 130  J0130            0         1639         3317    JHS-Only  63.3  67.8  72.4
    ## 131  J0131            0         1994         3288    JHS-Only  29.5  35.0  38.5
    ## 132  J0132            0         1592         2703 Shared ARIC  75.4  79.8  82.8
    ## 133  J0133            0         2059         3333 Shared ARIC  58.4  64.0  67.5
    ## 134  J0134            0         1449         2525    JHS-Only  47.2  51.1  54.1
    ## 135  J0135            0         1846         2762    JHS-Only  40.4  45.5  48.0
    ## 136  J0136            0         1824         2904    JHS-Only  55.7  60.7  63.7
    ## 137  J0137            0         1656         2634    JHS-Only  43.4  48.0  50.6
    ## 138  J0138            0         1704         2856    JHS-Only  39.9  44.6  47.7
    ## 140  J0140            0         1612         2507    JHS-Only  44.7  49.1  51.6
    ## 141  J0141            0         1863         3024    JHS-Only  48.4  53.5  56.7
    ## 142  J0142            0         1517         2633    JHS-Only  54.2  58.4  61.4
    ## 143  J0143            0         1671         2947 Shared ARIC  72.6  77.2  80.7
    ## 144  J0144            0         1603         2903 Shared ARIC  68.0  72.3  75.9
    ## 145  J0145            0         1845         2967    JHS-Only  41.9  47.0  50.1
    ## 146  J0146            0         1597         2695    JHS-Only  54.0  58.4  61.4
    ## 148  J0148            0         1606         2706 Shared ARIC  71.5  75.9  78.9
    ## 149  J0149            0         1881         3265    JHS-Only  45.9  51.0  54.8
    ## 150  J0150            0         1845         2570    JHS-Only  39.7  44.7  46.7
    ## 151  J0151            0         1547         2632    JHS-Only  45.8  50.0  53.0
    ## 152  J0152            0         1785         3149 Shared ARIC  65.1  70.0  73.7
    ## 154  J0154            0         1631         3288    JHS-Only  43.4  47.8  52.4
    ## 157  J0157            0         1492         2636    JHS-Only  53.6  57.6  60.8
    ## 158  J0158            0         1594         2690 Shared ARIC  59.3  63.7  66.7
    ## 160  J0160            0         1483         2582    JHS-Only  70.7  74.8  77.8
    ## 161  J0161            0         1509         2605    JHS-Only  40.8  45.0  48.0
    ## 162  J0162            0         1382         2564    JHS-Only  60.7  64.5  67.7
    ## 163  J0163            0         1610         2556    JHS-Only  55.3  59.7  62.3
    ## 166  J0166            0         1702         3002    JHS-Only  46.6  51.2  54.8
    ## 167  J0167            0         1600         2561    JHS-Only  39.4  43.7  46.4
    ## 168  J0168            0         1637         2634 Shared ARIC  61.5  66.0  68.7
    ## 169  J0169            0         2028         3381    JHS-Only  38.4  43.9  47.6
    ## 170  J0170            0         1914         2990    JHS-Only  32.5  37.7  40.7
    ## 171  J0171            0         1821         2920    JHS-Only  54.2  59.2  62.2
    ## 172  J0172            0         1647         2661    JHS-Only  41.9  46.4  49.2
    ## 173  J0173            0         1481         2586 Shared ARIC  61.2  65.3  68.3
    ## 174  J0174            0         1576         2674    JHS-Only  64.4  68.7  71.7
    ## 175  J0175            0         1626         2653    JHS-Only  51.0  55.5  58.3
    ## 176  J0176            0         2182         2902 Shared ARIC  66.9  72.9  74.9
    ## 178  J0178            0         1576         2675    JHS-Only  60.7  65.0  68.1
    ## 180  J0180            0         2023         3198    JHS-Only  56.0  61.5  64.7
    ## 181  J0181            0         2053         2913    JHS-Only  52.1  57.8  60.1
    ## 182  J0182            0         1595         2800    JHS-Only  55.5  59.9  63.2
    ## 183  J0183            0         1634         2937    JHS-Only  36.9  41.4  44.9
    ## 184  J0184            0         1824         3162 Shared ARIC  70.4  75.4  79.0
    ## 185  J0185            0         2105         3246    JHS-Only  33.6  39.4  42.5
    ## 186  J0186            0         1603         2715 Shared ARIC  63.4  67.8  70.8
    ## 187  J0187            0         2559         3350 Shared ARIC  63.2  70.2  72.4
    ## 188  J0188            0         1579         2717 Shared ARIC  66.2  70.5  73.6
    ## 189  J0189            0         1523         2621    JHS-Only  58.5  62.7  65.7
    ## 191  J0191            0         1588         3116 Shared ARIC  78.7  83.0  87.2
    ## 192  J0192            0         1728         3423    JHS-Only  40.3  45.1  49.7
    ## 193  J0193            0         1694         2940    JHS-Only  45.0  49.6  53.0
    ## 194  J0194            0         1583         2681    JHS-Only  43.1  47.4  50.4
    ## 195  J0195            0         2259         3667    JHS-Only  59.2  65.4  69.3
    ## 196  J0196            0         1616         2690    JHS-Only  44.0  48.4  51.3
    ## 197  J0197            0         1379         2479 Shared ARIC  64.4  68.1  71.1
    ## 198  J0198            0         1629         2870    JHS-Only  70.7  75.1  78.5
    ## 199  J0199            0         1883         2632    JHS-Only  45.0  50.2  52.2
    ## 200  J0200            0         2674         3712 Shared ARIC  66.6  73.9  76.7
    ## 201  J0201            0         1659         2702 Shared ARIC  58.3  62.8  65.7
    ## 202  J0202            0         1816         3045    JHS-Only  51.4  56.3  59.7
    ## 203  J0203            0         1715         2788    JHS-Only  45.9  50.6  53.6
    ## 204  J0204            0         1813         2940    JHS-Only  52.4  57.4  60.5
    ## 207  J0207            0         1493         2670    JHS-Only  64.1  68.2  71.4
    ## 208  J0208            0         1713         2952 Shared ARIC  62.6  67.3  70.7
    ## 209  J0209            0         1708         2808    JHS-Only  70.5  75.2  78.2
    ## 210  J0210            0         1659         2826 Shared ARIC  69.8  74.4  77.5
    ## 212  J0212            0         1774         2748    JHS-Only  21.6  26.5  29.1
    ## 214  J0214            0         1687         2800    JHS-Only  36.9  41.5  44.5
    ## 215  J0215            0         2032         3134    JHS-Only  40.6  46.1  49.1
    ## 216  J0216            0         1865         2940    JHS-Only  36.3  41.4  44.4
    ## 217  J0217            0         1726         2981    JHS-Only  30.5  35.2  38.7
    ## 218  J0218            0         1926         2927    JHS-Only  45.4  50.7  53.4
    ## 219  J0219            0         1734         2798    JHS-Only  66.5  71.2  74.2
    ## 221  J0221            0         1607         2703 Shared ARIC  68.4  72.8  75.8
    ## 222  J0222            0         1711         3049    JHS-Only  32.8  37.5  41.2
    ## 223  J0223            0         1675         3263 Shared ARIC  64.3  68.9  73.2
    ## 224  J0224            0         1474         2530    JHS-Only  38.5  42.5  45.4
    ## 227  J0227            0         1543         3174    JHS-Only  54.5  58.7  63.2
    ## 228  J0228            0         1672         2580    JHS-Only  50.6  55.2  57.7
    ## 229  J0229            0         1596         2944    JHS-Only  55.5  59.9  63.6
    ## 230  J0230            0         2498         3828 Shared ARIC  66.6  73.5  77.1
    ## 231  J0231            0         1891         2987    JHS-Only  44.5  49.7  52.7
    ## 232  J0232            0         1833         2968 Shared ARIC  72.4  77.4  80.5
    ## 233  J0233            0         1792         3304 Shared ARIC  58.9  63.8  67.9
    ## 234  J0234            0         1603         2952    JHS-Only  31.2  35.6  39.3
    ## 235  J0235            0         1519         2609    JHS-Only  56.1  60.3  63.3
    ## 236  J0236            0         1519         2674    JHS-Only  38.0  42.2  45.4
    ## 237  J0237            0         1710         3033    JHS-Only  41.8  46.5  50.2
    ## 238  J0238            0         1622         2924    JHS-Only  50.7  55.1  58.7
    ## 239  J0239            0         2120         3340 Shared ARIC  60.0  65.8  69.1
    ## 240  J0240            0         1605         2702    JHS-Only  59.5  63.9  66.9
    ##     maleV1 waistV1 waistV2 waistV3  sbpV1  sbpV2 sbpV3 HbA1cV1 HbA1cV2 HbA1cV3
    ## 1     Male      94   91.44   97.79 110.99 109.16   116     8.3     7.4     8.2
    ## 2   Female      94   92.71   99.06 159.59 121.00   158     5.7     5.8     6.1
    ## 3     Male     114  106.68  101.70 132.08 122.00   130     6.0     6.0     5.9
    ## 4     Male     111  109.22  118.00 112.83 116.49   148     4.0     4.5     4.7
    ## 5   Female     153  109.22  118.00 114.66 108.00   116     7.3     7.2     7.4
    ## 6   Female      89   88.90   87.60 127.50 174.00   161     5.6     5.7     5.9
    ## 7   Female     118  116.84  126.30 141.25 154.00   143     6.9     7.2     8.2
    ## 8     Male     111  105.41  111.00 133.00 175.18   114     5.9     6.6    10.2
    ## 9     Male      80   85.09   88.00 107.33 119.00   130     8.8     8.4     8.5
    ## 11  Female     137  140.97  139.70 113.74 115.58   104     6.2     5.9     6.4
    ## 13    Male     108  106.68  110.40 100.91 116.00   123     6.0     5.9     6.2
    ## 14    Male     128  127.00  121.92 124.75 154.00   138     5.8     6.2     6.3
    ## 15  Female      85  111.76  111.30 110.99 109.00   139     5.1     4.8     5.4
    ## 16  Female     116   88.90  109.20 110.99 122.00   121     8.9    11.5     6.5
    ## 17    Male      85   88.90   91.50 116.49 124.00   113     5.4     5.8     6.1
    ## 18  Female     132  129.54  134.00 133.00 110.00   141     5.6     5.6     5.6
    ## 19    Male     122  119.38  124.40 136.67  93.00   148     6.5     6.4    10.1
    ## 21  Female      91   91.44   93.00 117.41 139.00   135     5.8     6.1     6.3
    ## 23  Female     107  101.60   97.00 142.17 141.25   177     5.5     6.0     6.1
    ## 24  Female      96  109.22  109.22 111.91 122.00   111     5.4     6.7     5.5
    ## 25    Male      96   99.06   95.25 133.92 127.00   145     5.5     5.7     5.8
    ## 27  Female      89  114.30   94.00 122.91 130.00   110     4.9     6.2     6.2
    ## 28  Female      72   81.28   83.82 118.33 123.00   138     5.3     5.7     5.9
    ## 30  Female      91   96.52   97.00 122.00 105.00   104     6.2     6.4     6.5
    ## 32  Female      70   73.66   77.50 127.50 118.00   126     5.6     5.9     6.0
    ## 33    Male      89   86.36   88.90 133.00 115.00   139     4.4     4.6     4.7
    ## 34  Female      86   86.36   82.55 117.41 143.00   109     6.3     6.7     6.6
    ## 36  Female     112  109.22  106.00 137.58 144.00   149     5.8     5.9     5.9
    ## 39  Female     127  132.08  134.60 110.99 139.00   122     6.3     7.6     6.7
    ## 40  Female     144  134.62  139.70 132.08 118.00   131     6.7     5.9     6.3
    ## 41  Female      90   96.52  101.60 133.92 120.16   122     5.1     5.8     5.7
    ## 42    Male      95   97.79  101.60 113.74 136.67   147     6.1     6.2     6.1
    ## 43  Female     107  106.68  109.20 138.50 126.00   117     7.2     7.7    10.0
    ## 44  Female      90   88.90   93.90 118.33 105.00   100     4.5     5.3     5.3
    ## 46    Male      91   93.98   78.74 155.00 155.00   151     5.8     5.7     5.5
    ## 48  Female     108  106.68  116.84 125.66  89.00    93     5.3     5.6     5.6
    ## 49    Male      96   90.17   92.50 153.17 113.00   132     5.6     6.1     6.0
    ## 50    Male     107  109.22  106.68 132.08 132.00   137     5.6     5.8     5.7
    ## 52  Female      95  101.60  106.70 162.34 151.00   140     5.2     5.6     5.7
    ## 53  Female      96   99.06   91.44 117.41 114.00   121     5.8     6.3     6.1
    ## 54    Male     113  109.22  111.76 152.25 144.00   174     6.1     6.9     7.6
    ## 56  Female      84   86.36   87.63 108.24 122.91   112     5.4     5.4     5.6
    ## 58  Female     102  106.68  115.50 122.00 134.00   130     5.5     6.0     6.5
    ## 59    Male      91   90.17   87.63 118.33 115.58   116     7.3     5.3     5.1
    ## 60    Male     104  109.22  106.60 114.66 129.00   124     5.4     5.6     5.7
    ## 61  Female      81   86.36   91.40 111.91 145.00   139     4.8     5.6     5.8
    ## 62  Female      98   95.76   86.36 139.42 122.00   121     6.7     5.7     6.0
    ## 63    Male      96   96.52   91.44 133.92 153.17   140     8.3     7.4     7.2
    ## 64  Female     106  119.38  114.30 110.99 127.00   112     5.4     5.4     5.6
    ## 65  Female     135  127.00  130.00 140.33 136.67   126     6.6     8.4     6.8
    ## 66  Female     118  116.84  111.76 161.42 128.41   115     6.9     7.3     6.2
    ## 67    Male      77   80.01   72.39 119.25 134.00   152     4.9     5.4     5.3
    ## 68  Female      89   88.90   83.82 113.74 113.74   112     5.2     5.6     5.9
    ## 69    Male     112  113.03  114.30 134.83 110.00   133     6.1     6.1     6.3
    ## 70    Male     113  119.38  120.60 128.41 142.00   132     5.6     5.9     5.9
    ## 71  Female     106   97.79  102.87 141.25 126.00   138     6.2     6.3     6.2
    ## 72    Male     107  110.49  116.00 122.91 121.00   107     5.4     5.7     6.0
    ## 73    Male     105  104.14  105.41  99.99  96.32   101     7.5     7.2     7.9
    ## 74    Male     117  116.84  128.00 116.49 120.00   136     5.1     5.3     5.7
    ## 76    Male      94   93.98   93.98 124.75 121.08   121     5.2     4.7     5.2
    ## 77  Female     109  111.76  118.00 126.58 132.00   115     5.5     7.2     7.2
    ## 78  Female     102  104.14  102.87 133.00 129.00   132     5.2     5.6     5.6
    ## 79    Male     103  106.68  111.00 110.08 114.00   114     5.6     6.0     6.5
    ## 81    Male     107  124.46   96.52 144.92 139.00   120     5.2     5.4     5.5
    ## 82  Female      81   96.52   99.06 111.91 118.33   110     5.2     5.7     5.7
    ## 83  Female      94   88.90   93.98 119.25 126.58   162     5.2     5.8     6.0
    ## 84  Female      79   86.36   91.00 104.58 104.00   122     4.9     5.2     5.5
    ## 85    Male     129  121.92  129.54 118.33 106.00   114     7.1     9.8     9.6
    ## 86    Male      89   96.52   90.00 108.24 145.00   130     5.4     5.4     5.4
    ## 87  Female     106   93.98  106.70 106.41 115.00   130     5.2     5.3     5.9
    ## 88  Female     102  100.33  101.60 120.16 132.00   121     7.8     6.9     7.7
    ## 89  Female     106  106.68  106.68 127.50 156.00   143     5.6     5.6     5.6
    ## 90    Male     116  114.30  128.27 134.83 151.34   123     5.1     7.5     7.5
    ## 91    Male     114  121.92  125.73 125.66 121.00    95     5.3     5.6     6.1
    ## 92  Female      90   80.01   90.00 117.41 139.00   135     5.1     5.4     5.3
    ## 93  Female      90   91.44   88.00 120.16 160.00   124     6.7     7.1     6.5
    ## 94  Female     106  109.22  104.14 116.49 123.00   124     4.7     5.5     6.4
    ## 95  Female      95  106.68  116.84  98.16  98.00   113     4.8     5.4     6.1
    ## 96  Female     107  109.22  116.84 122.91 132.08   122     6.8     6.3     6.4
    ## 97  Female      82   81.28   78.74  99.07 131.17   134     6.3     6.4     6.6
    ## 98  Female     122  121.92  132.00 108.24 142.00   107     4.7     5.2     5.6
    ## 99    Male     113  116.84  125.00 139.42 142.00   132    11.8    13.7     8.4
    ## 101 Female     106  119.38  118.00 160.51 135.00   172     5.5     5.6     5.8
    ## 102   Male      98  101.60  111.70 125.66 131.00   133     5.6     5.6     5.7
    ## 104 Female     131  149.86  152.40 147.67 152.25   128     5.5     5.8     5.6
    ## 106 Female      98   93.98   93.98 135.75 114.00   133     4.9     5.4     5.1
    ## 107 Female      87   86.36   91.00 124.75 129.33   139     4.9     5.3     5.3
    ## 108   Male      97  100.33  100.40 109.16 114.00   126     5.6     5.6     5.8
    ## 109   Male      91  106.68  102.00 100.91 142.00   131     6.8     6.8     6.4
    ## 110   Male      82   86.36   83.80 113.74 108.00   112     5.4     5.5     5.3
    ## 111 Female     102  104.14   93.98 142.17 149.50   170     5.7     6.4     7.0
    ## 112 Female      93   93.98   88.90 138.50 135.75   158     5.8     6.2     6.0
    ## 113   Male     104  104.14  106.68 132.08 119.00   131     5.7     5.7     5.9
    ## 116   Male      87   93.98  100.33 125.66 120.00   120     5.0     5.3     5.4
    ## 117 Female      86   83.82   90.17 133.00 138.50   123     5.5     6.1     6.2
    ## 118 Female     121  101.60  116.84 116.49 109.00   132     5.5     5.7     6.2
    ## 119 Female     107  124.46  119.20 110.99 118.00   118     5.1     5.2     5.2
    ## 120 Female      82   86.36   86.36 145.84 139.00   117     5.5     5.7     5.8
    ## 121   Male      81   88.90   83.82 122.91 131.17   137     5.5     5.4     5.6
    ## 122 Female     118   99.06  119.38 130.25 130.25   117     5.4     5.6     5.7
    ## 123 Female      87   85.09   84.50 128.41 130.00   123     6.0     6.1     6.4
    ## 124 Female     101  104.14  113.03 119.25 119.00   117     5.5     6.6     6.5
    ## 125   Male     119  127.00  127.00 140.33 105.00   135     5.7     6.3     6.5
    ## 126 Female      78   85.09   83.80 120.16 122.00   117     5.6     6.0     5.9
    ## 127 Female     123  116.84  115.57 132.08 100.00   116     5.4     5.7     5.8
    ## 128   Male     105  115.57  115.00 110.99 128.00   118     6.9     7.7     7.7
    ## 129 Female      86   93.98   98.50 110.08 123.00   142     4.6     5.0     5.2
    ## 130 Female      95   91.44   89.30 148.59 149.50   146     5.3     5.6     5.4
    ## 131   Male     109   95.25  107.00 118.33 119.00   127     4.4     4.5     4.7
    ## 132 Female      84   86.36   91.44 130.25 135.75   147     5.0     5.9     6.0
    ## 133 Female     132  121.92  116.80 126.58 158.00   126     7.0     7.4     7.7
    ## 134   Male     108  106.68  110.49 122.00 121.00   133     5.8     6.0     6.5
    ## 135   Male      90   96.52   85.00 125.66 104.00   131     4.9     5.1     5.0
    ## 136   Male     102   91.44   99.80 113.74 124.00   124     5.5     5.3     5.3
    ## 137 Female     108  120.65  121.92 105.49 106.00   114     5.5     5.9     5.9
    ## 138 Female     121  114.30  113.03 122.00 128.41   123     5.8     5.8     5.4
    ## 140 Female      80   86.36   88.90 121.08 117.00   137     5.4     5.7     5.4
    ## 141 Female      83   93.98   90.17 110.08 117.00   119     5.7     5.7     5.8
    ## 142   Male      96   99.06   99.06 108.24 112.00   102     5.8     5.6     5.8
    ## 143 Female     101  104.14   99.06 123.83 131.17   140     6.4     5.8     6.0
    ## 144 Female     104   99.06  101.60 126.58 121.00   132     7.5     7.8     7.8
    ## 145 Female     118  121.92  121.92 144.92 109.00   102     5.5     6.1     6.3
    ## 146   Male      98  110.49  115.57 119.25 117.41   146     9.2     9.5     7.9
    ## 148 Female     119  114.30  114.30 113.74 118.33   124     6.1     6.3     6.3
    ## 149 Female      76   85.09   86.00 144.92 139.00   132     5.5     5.8     6.1
    ## 150   Male      83   88.90   93.98 110.08 109.00   117     6.1     6.6     6.6
    ## 151   Male      84   83.82   87.90 117.41 114.00   108     4.7     4.8     5.0
    ## 152 Female      84   90.17   83.82 163.26 166.01   129     5.0     5.8     5.6
    ## 154 Female      97  101.60  100.30 117.41  97.00   126     5.9     6.1     6.0
    ## 157   Male      70   78.74   78.74 138.50 172.00   116     6.0     6.0     6.2
    ## 158 Female     116  147.32  137.16 123.83 121.08   128     6.3     8.3     8.7
    ## 160 Female      81   76.20   81.28 107.33 149.50   113     5.5     5.6     5.8
    ## 161 Female     108  106.68  114.30 125.66 107.00   116     5.7     5.8     6.1
    ## 162 Female      81  106.68   83.00 134.83 170.00   133     6.0     5.8     6.3
    ## 163 Female     113  111.76  121.92 113.74 122.00   127     7.8     8.3     9.6
    ## 166 Female      76   78.74   93.00  93.57 105.00   102     5.3     5.5     5.6
    ## 167   Male     102  107.95  107.00 124.75 108.00   109     4.6     4.7     4.8
    ## 168 Female      80   83.82   76.20 130.25 128.00   124     5.8     5.9     6.0
    ## 169 Female      85   81.28   84.00 105.49 110.00   104     5.6     5.5     5.3
    ## 170 Female      94  104.14  104.20 111.91 106.00   123     5.5     5.5     5.5
    ## 171 Female      87   93.98   91.44 127.50 131.17   106     5.5     5.8     5.7
    ## 172   Male      93   99.06  103.00 110.99 109.00   108     5.6     5.8     5.9
    ## 173 Female     101  101.60  106.68 131.17 122.00   109     5.7     5.9     6.1
    ## 174   Male      91   88.90   83.82 149.50 101.00   114     5.1     5.8     5.7
    ## 175 Female      84   81.28   98.00 119.25 143.00   125     4.6     5.3     5.6
    ## 176 Female     129  132.08  130.81 152.25 136.00   140     5.7     6.7     7.2
    ## 178 Female     102  106.68  105.41 135.75 163.26   123     5.7     5.6     5.9
    ## 180 Female     104  105.41  113.03 122.91 155.00   120     5.9     6.7     6.4
    ## 181   Male     104   91.44  104.14 166.92 133.00   143     5.7     5.6     5.5
    ## 182   Male     103  109.22  114.30 128.41 108.00   116     5.4     5.5     5.9
    ## 183   Male     127  127.00  127.00 133.92 118.00   116     6.7     7.9    12.9
    ## 184 Female     110  106.68  101.60 155.92 147.00   137     7.0     8.1     6.4
    ## 185   Male      98  101.60  101.60 129.33 124.00   117     5.4     5.4     5.7
    ## 186 Female      90   99.06   96.52 146.75 146.00   143     6.0     5.8     6.2
    ## 187   Male     103   99.06  109.00 130.25 108.00   135     5.3     6.4     5.8
    ## 188 Female     122  119.38  101.60 130.25 133.92   113     5.7     6.1     6.1
    ## 189 Female      90   90.17   93.98 135.75 122.00    96     5.5     5.5     5.7
    ## 191 Female      91  106.68   98.00 115.58 114.66   126     5.4     5.5     5.4
    ## 192 Female     113  109.22  117.00 142.17 139.00   157     5.1     5.0     5.1
    ## 193 Female      99   93.98   97.80 108.24 147.00   127     5.6     6.0     6.1
    ## 194   Male     107   99.06   96.52 131.17 130.25   124     6.8     6.9     6.6
    ## 195   Male     103   97.79   95.30 131.17 127.00   168     7.0     5.3     6.2
    ## 196 Female      87   83.82   88.40 102.74 112.00   106     5.8     5.6     5.6
    ## 197 Female     103   97.79   96.52 124.75 112.83   102     5.3     5.6     5.8
    ## 198 Female     114  116.84  121.92 116.49 164.17   138     5.4     5.5     5.4
    ## 199   Male      87   88.90   93.98 111.91 107.00   126     5.1     5.4     5.4
    ## 200 Female      85   71.12   89.00 136.67 180.00   136     5.4     5.7     5.4
    ## 201 Female     101  106.68  105.41 110.99 122.00   121     5.6     5.9     6.0
    ## 202 Female      82   81.28   88.90 120.16 127.50   129     5.8     6.3     6.3
    ## 203   Male      99  101.60  106.68 136.67 123.00   145     5.4     5.7     5.8
    ## 204   Male     105  104.14  112.00 112.83 116.00   128     4.8     4.8     5.0
    ## 207 Female     116   93.98  105.70 135.75 123.00   134     4.6     4.5     4.6
    ## 208 Female      78   83.82   81.28 121.08 128.41   133     5.2     5.5     5.3
    ## 209 Female     106  104.14  111.76 163.26 134.83   139     7.7     7.8     8.9
    ## 210 Female     110  127.00  119.38 127.50 118.33   123     6.7     7.0     6.5
    ## 212 Female      78   78.74   86.30 101.82 130.00   119     5.2     5.3     5.2
    ## 214   Male      71   73.66   79.00 107.33 108.00   125     4.8     4.9     4.9
    ## 215 Female      79   78.74   88.00 132.08 102.00   105     4.7     5.4     5.6
    ## 216 Female     126  127.00  111.76 120.16  95.00   106     5.7     5.5     5.9
    ## 217   Male     107  111.76  109.20 117.41 112.00   121     5.2     5.6     5.5
    ## 218 Female      93   90.17   86.36 120.16 153.00   127     5.9     6.3     6.5
    ## 219 Female     103  109.22  106.68 135.75 157.00   165     5.7     6.0     6.1
    ## 221 Female     114  104.14   99.06 144.92 150.00   181     6.0     6.1     6.0
    ## 222 Female     108  111.76  115.00 104.58  93.00    97     5.3     5.6     5.9
    ## 223   Male      90   95.25   96.52 127.50 128.41   134     6.4     6.2     6.3
    ## 224 Female      99  106.68  109.50 125.66 123.00   121     5.6     6.0     6.5
    ## 227 Female     114  121.92  135.00 123.83 111.00   121     6.1     6.1     6.4
    ## 228 Female     109   99.06  113.03 127.50 140.00   115     5.7     5.9     6.2
    ## 229 Female      90  106.68  101.60 112.83 129.33   141     5.7     6.0     6.2
    ## 230   Male      99   96.52   97.20 120.16 128.00   136     5.7     5.9     6.2
    ## 231   Male      80   85.09   88.90 122.00 121.00   126     5.0     5.3     5.5
    ## 232   Male      98  101.60   97.79 130.25 125.66   129     5.8     6.0     6.1
    ## 233 Female      83   96.52   93.98 144.92 144.00   135     5.2     5.6     5.8
    ## 234   Male     112  111.76  114.30 132.08 148.59   134     5.7     6.4     6.5
    ## 235   Male     109  106.68  111.76 128.41 111.00   117     5.4     5.5     6.0
    ## 236   Male      78   86.36   86.40 103.66 120.00   110     5.1     5.4     5.5
    ## 237   Male     129  114.30  111.76 109.16 124.75   114     5.5     5.7     5.8
    ## 238   Male      97   96.52   97.79 146.75 115.58   134     6.2     6.7     6.8
    ## 239   Male     108  109.22  114.30 110.99 141.00   125     5.5     5.8     6.0
    ## 240 Female     109  115.57  106.68 164.17 118.33   119     6.6     6.1     6.2
    ##     eGFRV1 eGFRV3
    ## 1   102.16  75.13
    ## 2    92.67  79.14
    ## 3    92.36  98.37
    ## 4   101.83  99.44
    ## 5   120.07 111.42
    ## 6   108.67 101.31
    ## 7    47.57  33.20
    ## 8    96.99  82.75
    ## 9    96.00  88.68
    ## 11   94.80  84.69
    ## 13  113.41  84.42
    ## 14  101.86  93.59
    ## 15  113.03 119.68
    ## 16   91.27  79.55
    ## 17   90.46  81.05
    ## 18  104.66  51.79
    ## 19   95.94  73.35
    ## 21   92.39  84.88
    ## 23   79.09  64.19
    ## 24  113.63  87.27
    ## 25   87.17  66.71
    ## 27  115.79 119.02
    ## 28   96.36  91.12
    ## 30  106.90 112.11
    ## 32   85.77  91.06
    ## 33  113.26 103.52
    ## 34   93.43  97.42
    ## 36  101.95  92.49
    ## 39   72.87  58.80
    ## 40  125.40 129.10
    ## 41   98.46  83.41
    ## 42   93.76  95.70
    ## 43   78.17  81.49
    ## 44  100.55  94.58
    ## 46   58.08  48.99
    ## 48  120.82  88.71
    ## 49   74.00  60.55
    ## 50   83.07  78.76
    ## 52   84.17  85.64
    ## 53  118.28 118.64
    ## 54   76.24  76.81
    ## 56   89.38  84.49
    ## 58  112.28 100.28
    ## 59   98.56  72.96
    ## 60   71.99  72.66
    ## 61  128.25 108.40
    ## 62  106.89  68.02
    ## 63  104.88 107.21
    ## 64  110.33 114.93
    ## 65  122.67 109.59
    ## 66  101.28  24.89
    ## 67  117.79  93.22
    ## 68  112.98  62.67
    ## 69   89.81  86.26
    ## 70  108.58  98.01
    ## 71   82.79  77.10
    ## 72   75.50  88.69
    ## 73  115.30 112.30
    ## 74   87.36  83.57
    ## 76   72.86  69.60
    ## 77  121.79 114.65
    ## 78   76.19  77.82
    ## 79   89.77  83.97
    ## 81   79.07  89.79
    ## 82   66.92  52.63
    ## 83   98.22  93.87
    ## 84  125.23  93.59
    ## 85  103.39  99.32
    ## 86  123.48 109.38
    ## 87  103.35 106.61
    ## 88   83.69  74.08
    ## 89  102.28  94.10
    ## 90   86.69  71.40
    ## 91   82.32  65.58
    ## 92   87.43  88.57
    ## 93   93.01  89.38
    ## 94  103.65  84.78
    ## 95   74.76  71.09
    ## 96   78.55  75.51
    ## 97   92.88 110.48
    ## 98  134.99 115.72
    ## 99   99.88  84.44
    ## 101  80.05  72.54
    ## 102 112.89  97.74
    ## 104  97.61  89.50
    ## 106 116.57 100.37
    ## 107  78.60  81.93
    ## 108  87.49  91.31
    ## 109  71.30  61.55
    ## 110 109.69  98.11
    ## 111  76.08  61.75
    ## 112  97.89  93.27
    ## 113  80.46  80.45
    ## 116  64.29  71.31
    ## 117  93.10  78.54
    ## 118 112.60 117.38
    ## 119 134.45 123.41
    ## 120  82.95  75.19
    ## 121  71.42  66.75
    ## 122 107.49 110.67
    ## 123  73.56  70.60
    ## 124  83.31  73.88
    ## 125  87.28  74.76
    ## 126 109.91 101.99
    ## 127 122.21 112.79
    ## 128  83.11  74.42
    ## 129  96.12  82.71
    ## 130  84.74  75.86
    ## 131 100.11  85.45
    ## 132  68.28  59.69
    ## 133  87.74  47.66
    ## 134  98.50  90.68
    ## 135 103.28  89.56
    ## 136 104.39  90.90
    ## 137  97.45  90.88
    ## 138 115.69 116.95
    ## 140 111.86  91.58
    ## 141  73.36  74.39
    ## 142  76.25  69.33
    ## 143  91.94  64.43
    ## 144 111.34 101.05
    ## 145  76.77  96.66
    ## 146 105.66  81.52
    ## 148  70.18  62.08
    ## 149  74.67  72.66
    ## 150  93.24  76.90
    ## 151  89.31  87.40
    ## 152  96.93  84.35
    ## 154  97.49  84.98
    ## 157 105.98  83.66
    ## 158  76.44  58.99
    ## 160  93.16  62.58
    ## 161 114.95 111.07
    ## 162 117.14  92.12
    ## 163  56.87  28.46
    ## 166  95.31  90.81
    ## 167 104.05 116.42
    ## 168  99.39  88.69
    ## 169  88.58 101.30
    ## 170 121.87 101.74
    ## 171 104.64  85.00
    ## 172 102.23  86.94
    ## 173  75.43  70.27
    ## 174  98.21  93.69
    ## 175  92.39 112.08
    ## 176 106.37  95.02
    ## 178 111.10  99.69
    ## 180 103.35  74.90
    ## 181  85.41 102.96
    ## 182  83.43  71.88
    ## 183 105.87 107.52
    ## 184  62.86  36.52
    ## 185  88.13  79.95
    ## 186  59.29  55.50
    ## 187  99.02  58.61
    ## 188  96.20  75.23
    ## 189 118.96  92.00
    ## 191  76.09  76.73
    ## 192 135.16 127.01
    ## 193  96.40  71.86
    ## 194  82.48  87.11
    ## 195  67.19  51.26
    ## 196 112.43 116.00
    ## 197  97.43  95.93
    ## 198  70.59  50.96
    ## 199 100.00 100.91
    ## 200  72.67  62.30
    ## 201 101.67  98.06
    ## 202 106.74 109.38
    ## 203 121.38 102.47
    ## 204 106.84  92.96
    ## 207  97.61 101.71
    ## 208  98.62  81.21
    ## 209  93.33 101.63
    ## 210 109.89  96.49
    ## 212 131.56 137.55
    ## 214 105.89  93.77
    ## 215 128.02 113.85
    ## 216 131.88  86.40
    ## 217 124.62 107.06
    ## 218 111.28 121.58
    ## 219  72.70  74.93
    ## 221  94.71  72.09
    ## 222 121.59  99.94
    ## 223  98.27  82.02
    ## 224 100.87  96.99
    ## 227  90.16  80.93
    ## 228  81.29  84.14
    ## 229  78.52  85.37
    ## 230  69.90  61.53
    ## 231 100.33 114.96
    ## 232  82.49  63.62
    ## 233  76.70 101.48
    ## 234 110.17 111.86
    ## 235  92.49  90.07
    ## 236  94.31  85.64
    ## 237 102.25 110.43
    ## 238 117.37 117.26
    ## 239  80.82  68.92
    ## 240 100.80 106.01

## Factors

``` r
set.seed(1)
myfac <- factor(sample(LETTERS[1:5], 20, replace=TRUE))
myfac[sample(1:20, 3)] <- NA  # introduce some NAs
myfac
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: A B C D E

``` r
table(myfac, exclude=NULL)  # how many of each?
```

    ## myfac
    ##    A    B    C    D    E <NA> 
    ##    5    4    3    0    5    3

``` r
table(myfac)
```

    ## myfac
    ## A B C D E 
    ## 5 4 3 0 5

``` r
relevel(myfac, ref="D")
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: D A B C E

``` r
myfac
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: A B C D E

``` r
factor(myfac, levels=levels(myfac)[c(2,1,4,5,3)])
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: B A D E C

``` r
myfac
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: A B C D E

``` r
# change names of levels

levels(myfac)[4] <- "XYZ"  # careful, this will change the myfac variable
myfac
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: A B C XYZ E

``` r
# as a factor, "NA" will not be counted. It is just missing

table(myfac, exclude=NULL)  # how many of each
```

    ## myfac
    ##    A    B    C  XYZ    E <NA> 
    ##    5    4    3    0    5    3

``` r
levels(myfac) <- list("ABC"=c("A", "B", "C"), "DE"=c("XYZ", "E"))
table(myfac, exclude=NULL)  # how many of each
```

    ## myfac
    ##  ABC   DE <NA> 
    ##   12    5    3

``` r
myfac
```

    ##  [1] ABC  <NA> ABC  ABC  <NA> ABC  ABC  ABC  ABC  <NA> DE   DE   ABC  ABC  ABC 
    ## [16] DE   DE   ABC  ABC  DE  
    ## Levels: ABC DE

``` r
set.seed(1)
myfac <- factor(sample(LETTERS[1:5], 20, replace=TRUE))
myfac[sample(1:20, 3)] <- NA  # introduce some NAs
myfacdup <- myfac

levels(myfacdup) <- list("ABC"=c("A", "B", "C"), "DE"=c("D", "E"))

table(myfacdup, myfac, exclude=NULL)  # how many of each
```

    ##         myfac
    ## myfacdup A B C D E <NA>
    ##     ABC  5 4 3 0 0    0
    ##     DE   0 0 0 0 5    0
    ##     <NA> 0 0 0 0 0    3

``` r
myfacNA <- addNA(myfac)
myfacNA
```

    ##  [1] A    <NA> A    B    <NA> C    B    C    C    <NA> E    E    B    B    A   
    ## [16] E    E    A    A    E   
    ## Levels: A B C D E <NA>

``` r
levels(myfacNA)
```

    ## [1] "A" "B" "C" "D" "E" NA

``` r
levels(myfac)
```

    ## [1] "A" "B" "C" "D" "E"

## Cut and cut2

``` r
table(dat$sbpV1>=140, exclude=NULL)
```

    ## 
    ## FALSE  TRUE 
    ##   201    39

``` r
dat$sbpV1c <- cut(dat$sbpV1, breaks=c(min(dat$sbpV1),140,max(dat$sbpV1)),
    include.lowest=TRUE, right=FALSE)
head(dat$sbpV1c)
```

    ## [1] [93.6,140) [140,174]  [93.6,140) [93.6,140) [93.6,140) [93.6,140)
    ## Levels: [93.6,140) [140,174]

``` r
# cut min at 140, but want max
levels(dat$sbpV1c)
```

    ## [1] "[93.6,140)" "[140,174]"

``` r
table(dat$sbpV1c, exclude=NULL)
```

    ## 
    ## [93.6,140)  [140,174] 
    ##        201         39

``` r
quantile(dat$sbpV1, p=(0:2)/2)
```

    ##     0%    50%   100% 
    ##  93.57 124.75 174.26

``` r
table(cut(dat$sbpV1, breaks=quantile(dat$sbpV1, p=(0:2)/2), incl=T))
```

    ## 
    ## [93.6,125]  (125,174] 
    ##        121        119

``` r
#incl is include.lowest
quantile(dat$sbpV1, p=(0:3)/3)
```

    ##        0% 33.33333% 66.66667%      100% 
    ##   93.5700  118.3300  131.4733  174.2600

``` r
table(cut(dat$sbpV1, breaks=quantile(dat$sbpV1, p=(0:3)/3), incl=T))
```

    ## 
    ## [93.6,118]  (118,131]  (131,174] 
    ##         82         78         80

``` r
quantile(dat$sbpV1, p=(0:4)/4)
```

    ##     0%    25%    50%    75%   100% 
    ##  93.57 114.66 124.75 134.83 174.26

``` r
table(cut(dat$sbpV1, breaks=quantile(dat$sbpV1, p=(0:4)/4), incl=T))
```

    ## 
    ## [93.6,115]  (115,125]  (125,135]  (135,174] 
    ##         61         60         61         58

``` r
range(dat$sbpV1)
```

    ## [1]  93.57 174.26

``` r
table(cut(dat$sbpV1, breaks=3, incl=T))
```

    ## 
    ## [93.5,120]  (120,147]  (147,174] 
    ##         95        125         20

``` r
dat$sbpV1c2 <- cut2(dat$sbpV1, cuts=c(min(dat$sbpV1),140,max(dat$sbpV1)))
table(dat$sbpV1c, dat$sbpV1c2, exclude=NULL)
```

    ##             
    ##              [ 93.6,140.0) [140.0,174.3]
    ##   [93.6,140)           201             0
    ##   [140,174]              0            39

``` r
table(cut2(dat$sbpV1, g=2))
```

    ## 
    ## [ 93.6,126) [125.7,174] 
    ##         121         119

``` r
table(cut2(dat$sbpV1, g=3))
```

    ## 
    ## [ 93.6,119) [119.2,132) [132.1,174] 
    ##          82          78          80

``` r
table(cut2(dat$sbpV1, g=4))
```

    ## 
    ## [ 93.6,116) [115.6,126) [125.7,136) [135.8,174] 
    ##          61          60          61          58

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# In tidyverse
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
  
datdat<-dat %>% 
  mutate(sbpV1c=cut(sbpV1,breaks=c(min(sbpV1),
                                   140,
                                   max(sbpV1)),
                    include.lowest=T,
                    right=F),
         sbpV1c2=cut2(sbpV1,cuts=c(min(sbpV1),
                                   140,
                                   max(sbpV1))))
```

## Apply functions

``` r
A1C <- dat[,grep("HbA1c", names(dat))] 
apply(A1C, MARGIN=1, mean, na.rm=T)
```

    ##   [1]  7.966667  5.866667  5.966667  4.400000  7.300000  5.733333  7.433333
    ##   [8]  7.566667  8.566667  6.066667  6.166667  6.000000  6.033333  6.100000
    ##  [15]  5.100000  8.966667  5.766667  5.600000  7.666667  4.800000  6.066667
    ##  [22]  4.700000  5.866667  5.866667  5.666667  5.800000  5.766667  5.633333
    ##  [29]  7.300000  6.366667  8.400000  5.833333  4.566667  6.533333  5.950000
    ##  [36]  5.866667  4.800000  5.300000  6.866667  6.300000  5.533333  6.133333
    ##  [43]  8.300000  5.033333       NaN  5.666667  8.300000  5.500000  5.900000
    ##  [50]  5.700000  6.250000  5.500000  6.066667  6.866667  4.850000  5.466667
    ##  [57]  9.900000  6.000000  5.900000  5.566667  5.400000  6.133333  7.633333
    ##  [64]  5.466667  7.266667  6.800000  5.200000  5.566667  6.166667  5.800000
    ##  [71]  6.233333  5.700000  7.533333  5.366667  5.150000  5.033333  6.633333
    ##  [78]  5.466667  6.033333  5.950000  5.366667  5.533333  5.666667  5.200000
    ##  [85]  8.833333  5.400000  5.466667  7.466667  5.600000  6.700000  5.666667
    ##  [92]  5.266667  6.766667  5.533333  5.433333  6.500000  6.433333  5.166667
    ##  [99] 11.300000  6.200000  5.633333  5.633333  5.500000  5.633333  6.250000
    ## [106]  5.133333  5.166667  5.666667  6.666667  5.400000  6.366667  6.000000
    ## [113]  5.766667  6.150000  5.850000  5.233333  5.933333  5.800000  5.166667
    ## [120]  5.666667  5.500000  5.566667  6.166667  6.200000  6.166667  5.833333
    ## [127]  5.633333  7.433333  4.933333  5.433333  4.533333  5.633333  7.366667
    ## [134]  6.100000  5.000000  5.366667  5.766667  5.666667  5.850000  5.500000
    ## [141]  5.733333  5.733333  6.066667  7.700000  5.966667  8.866667  6.000000
    ## [148]  6.233333  5.800000  6.433333  4.833333  5.466667  8.500000  6.000000
    ## [155]  6.600000  5.400000  6.066667  7.766667  5.550000  5.633333  5.866667
    ## [162]  6.033333  8.566667  7.700000  5.900000  5.466667  4.700000  5.900000
    ## [169]  5.466667  5.500000  5.666667  5.766667  5.900000  5.533333  5.166667
    ## [176]  6.533333  5.600000  5.733333  7.800000  6.333333  5.600000  5.600000
    ## [183]  9.166667  7.166667  5.500000  6.000000  5.833333  5.966667  5.566667
    ## [190]  6.600000  5.433333  5.066667  5.900000  6.766667  6.166667  5.666667
    ## [197]  5.566667  5.433333  5.300000  5.500000  5.833333  6.133333  5.633333
    ## [204]  4.866667  6.200000  5.800000  4.566667  5.333333  8.133333  6.733333
    ## [211]  5.200000  5.233333  5.500000  4.866667  5.233333  5.700000  5.433333
    ## [218]  6.233333  5.933333  5.150000  6.033333  5.600000  6.300000  6.033333
    ## [225]  5.600000  6.000000  6.200000  5.933333  5.966667  5.933333  5.266667
    ## [232]  5.966667  5.533333  6.200000  5.633333  5.333333  5.666667  6.566667
    ## [239]  5.766667  6.300000

``` r
# compared to
rowMeans(A1C, na.rm=T)
```

    ##   [1]  7.966667  5.866667  5.966667  4.400000  7.300000  5.733333  7.433333
    ##   [8]  7.566667  8.566667  6.066667  6.166667  6.000000  6.033333  6.100000
    ##  [15]  5.100000  8.966667  5.766667  5.600000  7.666667  4.800000  6.066667
    ##  [22]  4.700000  5.866667  5.866667  5.666667  5.800000  5.766667  5.633333
    ##  [29]  7.300000  6.366667  8.400000  5.833333  4.566667  6.533333  5.950000
    ##  [36]  5.866667  4.800000  5.300000  6.866667  6.300000  5.533333  6.133333
    ##  [43]  8.300000  5.033333       NaN  5.666667  8.300000  5.500000  5.900000
    ##  [50]  5.700000  6.250000  5.500000  6.066667  6.866667  4.850000  5.466667
    ##  [57]  9.900000  6.000000  5.900000  5.566667  5.400000  6.133333  7.633333
    ##  [64]  5.466667  7.266667  6.800000  5.200000  5.566667  6.166667  5.800000
    ##  [71]  6.233333  5.700000  7.533333  5.366667  5.150000  5.033333  6.633333
    ##  [78]  5.466667  6.033333  5.950000  5.366667  5.533333  5.666667  5.200000
    ##  [85]  8.833333  5.400000  5.466667  7.466667  5.600000  6.700000  5.666667
    ##  [92]  5.266667  6.766667  5.533333  5.433333  6.500000  6.433333  5.166667
    ##  [99] 11.300000  6.200000  5.633333  5.633333  5.500000  5.633333  6.250000
    ## [106]  5.133333  5.166667  5.666667  6.666667  5.400000  6.366667  6.000000
    ## [113]  5.766667  6.150000  5.850000  5.233333  5.933333  5.800000  5.166667
    ## [120]  5.666667  5.500000  5.566667  6.166667  6.200000  6.166667  5.833333
    ## [127]  5.633333  7.433333  4.933333  5.433333  4.533333  5.633333  7.366667
    ## [134]  6.100000  5.000000  5.366667  5.766667  5.666667  5.850000  5.500000
    ## [141]  5.733333  5.733333  6.066667  7.700000  5.966667  8.866667  6.000000
    ## [148]  6.233333  5.800000  6.433333  4.833333  5.466667  8.500000  6.000000
    ## [155]  6.600000  5.400000  6.066667  7.766667  5.550000  5.633333  5.866667
    ## [162]  6.033333  8.566667  7.700000  5.900000  5.466667  4.700000  5.900000
    ## [169]  5.466667  5.500000  5.666667  5.766667  5.900000  5.533333  5.166667
    ## [176]  6.533333  5.600000  5.733333  7.800000  6.333333  5.600000  5.600000
    ## [183]  9.166667  7.166667  5.500000  6.000000  5.833333  5.966667  5.566667
    ## [190]  6.600000  5.433333  5.066667  5.900000  6.766667  6.166667  5.666667
    ## [197]  5.566667  5.433333  5.300000  5.500000  5.833333  6.133333  5.633333
    ## [204]  4.866667  6.200000  5.800000  4.566667  5.333333  8.133333  6.733333
    ## [211]  5.200000  5.233333  5.500000  4.866667  5.233333  5.700000  5.433333
    ## [218]  6.233333  5.933333  5.150000  6.033333  5.600000  6.300000  6.033333
    ## [225]  5.600000  6.000000  6.200000  5.933333  5.966667  5.933333  5.266667
    ## [232]  5.966667  5.533333  6.200000  5.633333  5.333333  5.666667  6.566667
    ## [239]  5.766667  6.300000

``` r
apply(A1C, MARGIN=1, sum, na.rm=T)
```

    ##   [1] 23.9 17.6 17.9 13.2 21.9 17.2 22.3 22.7 25.7 18.2 18.5 12.0 18.1 18.3 15.3
    ##  [16] 26.9 17.3 16.8 23.0  4.8 18.2  4.7 17.6 17.6 17.0 11.6 17.3 16.9 14.6 19.1
    ##  [31]  8.4 17.5 13.7 19.6 11.9 17.6  4.8  5.3 20.6 18.9 16.6 18.4 24.9 15.1  0.0
    ##  [46] 17.0 16.6 16.5 17.7 17.1 12.5 16.5 18.2 20.6  9.7 16.4 19.8 18.0 17.7 16.7
    ##  [61] 16.2 18.4 22.9 16.4 21.8 20.4 15.6 16.7 18.5 17.4 18.7 17.1 22.6 16.1 10.3
    ##  [76] 15.1 19.9 16.4 18.1 11.9 16.1 16.6 17.0 15.6 26.5 16.2 16.4 22.4 16.8 20.1
    ##  [91] 17.0 15.8 20.3 16.6 16.3 19.5 19.3 15.5 33.9 12.4 16.9 16.9  5.5 16.9 12.5
    ## [106] 15.4 15.5 17.0 20.0 16.2 19.1 18.0 17.3 12.3 11.7 15.7 17.8 17.4 15.5 17.0
    ## [121] 16.5 16.7 18.5 18.6 18.5 17.5 16.9 22.3 14.8 16.3 13.6 16.9 22.1 18.3 15.0
    ## [136] 16.1 17.3 17.0 11.7 16.5 17.2 17.2 18.2 23.1 17.9 26.6  6.0 18.7 17.4 19.3
    ## [151] 14.5 16.4  8.5 18.0 13.2  5.4 18.2 23.3 11.1 16.9 17.6 18.1 25.7  7.7 11.8
    ## [166] 16.4 14.1 17.7 16.4 16.5 17.0 17.3 17.7 16.6 15.5 19.6 11.2 17.2 15.6 19.0
    ## [181] 16.8 16.8 27.5 21.5 16.5 18.0 17.5 17.9 16.7  6.6 16.3 15.2 17.7 20.3 18.5
    ## [196] 17.0 16.7 16.3 15.9 16.5 17.5 18.4 16.9 14.6 12.4 11.6 13.7 16.0 24.4 20.2
    ## [211]  5.2 15.7 11.0 14.6 15.7 17.1 16.3 18.7 17.8 10.3 18.1 16.8 18.9 18.1  5.6
    ## [226]  6.0 18.6 17.8 17.9 17.8 15.8 17.9 16.6 18.6 16.9 16.0 17.0 19.7 17.3 18.9

``` r
rowSums(A1C, na.rm=T)
```

    ##   [1] 23.9 17.6 17.9 13.2 21.9 17.2 22.3 22.7 25.7 18.2 18.5 12.0 18.1 18.3 15.3
    ##  [16] 26.9 17.3 16.8 23.0  4.8 18.2  4.7 17.6 17.6 17.0 11.6 17.3 16.9 14.6 19.1
    ##  [31]  8.4 17.5 13.7 19.6 11.9 17.6  4.8  5.3 20.6 18.9 16.6 18.4 24.9 15.1  0.0
    ##  [46] 17.0 16.6 16.5 17.7 17.1 12.5 16.5 18.2 20.6  9.7 16.4 19.8 18.0 17.7 16.7
    ##  [61] 16.2 18.4 22.9 16.4 21.8 20.4 15.6 16.7 18.5 17.4 18.7 17.1 22.6 16.1 10.3
    ##  [76] 15.1 19.9 16.4 18.1 11.9 16.1 16.6 17.0 15.6 26.5 16.2 16.4 22.4 16.8 20.1
    ##  [91] 17.0 15.8 20.3 16.6 16.3 19.5 19.3 15.5 33.9 12.4 16.9 16.9  5.5 16.9 12.5
    ## [106] 15.4 15.5 17.0 20.0 16.2 19.1 18.0 17.3 12.3 11.7 15.7 17.8 17.4 15.5 17.0
    ## [121] 16.5 16.7 18.5 18.6 18.5 17.5 16.9 22.3 14.8 16.3 13.6 16.9 22.1 18.3 15.0
    ## [136] 16.1 17.3 17.0 11.7 16.5 17.2 17.2 18.2 23.1 17.9 26.6  6.0 18.7 17.4 19.3
    ## [151] 14.5 16.4  8.5 18.0 13.2  5.4 18.2 23.3 11.1 16.9 17.6 18.1 25.7  7.7 11.8
    ## [166] 16.4 14.1 17.7 16.4 16.5 17.0 17.3 17.7 16.6 15.5 19.6 11.2 17.2 15.6 19.0
    ## [181] 16.8 16.8 27.5 21.5 16.5 18.0 17.5 17.9 16.7  6.6 16.3 15.2 17.7 20.3 18.5
    ## [196] 17.0 16.7 16.3 15.9 16.5 17.5 18.4 16.9 14.6 12.4 11.6 13.7 16.0 24.4 20.2
    ## [211]  5.2 15.7 11.0 14.6 15.7 17.1 16.3 18.7 17.8 10.3 18.1 16.8 18.9 18.1  5.6
    ## [226]  6.0 18.6 17.8 17.9 17.8 15.8 17.9 16.6 18.6 16.9 16.0 17.0 19.7 17.3 18.9

``` r
apply(A1C, MARGIN=2, max, na.rm=T)
```

    ## HbA1cV1 HbA1cV2 HbA1cV3 
    ##    11.8    13.7    12.9

``` r
sapply(A1C, max, na.rm=T)
```

    ## HbA1cV1 HbA1cV2 HbA1cV3 
    ##    11.8    13.7    12.9

``` r
# apply user created function
meanSD <- function(x){
    mnx <- round(mean(x, na.rm=T), 1)
    SDx <- round(sd(x, na.rm=T), 1)
    paste0(mnx, " (", SDx, ")")
}
apply(A1C, 2, meanSD)
```

    ##     HbA1cV1     HbA1cV2     HbA1cV3 
    ##   "5.8 (1)" "6.1 (1.1)"   "6.2 (1)"

``` r
# create an array and use apply
set.seed(5211)
Arr <- array(sample(1:10, 24, replace=T), c(2,3,4))

apply(Arr, c(1,2), mean) 
```

    ##      [,1] [,2] [,3]
    ## [1,] 3.75    5    4
    ## [2,] 3.00    7    5

``` r
# if A1C were a list, use lapply or sapply
A1Clist <- as.list(A1C)
A1Clist 
```

    ## $HbA1cV1
    ##   [1]  8.3  5.7  6.0  4.0  7.3  5.6  6.9  5.9  8.8  5.7  6.2  6.1  6.0  5.8  5.1
    ##  [16]  8.9  5.4  5.6  6.5  4.8  5.8  4.7  5.5  5.4  5.5  6.0  4.9  5.3  7.8  6.2
    ##  [31]  8.4  5.6  4.4  6.3  5.8  5.8  4.8  5.3  6.3  6.7  5.1  6.1  7.2  4.5   NA
    ##  [46]  5.8  9.5  5.3  5.6  5.6  6.2  5.2  5.8  6.1  4.7  5.4 10.8  5.5  7.3  5.4
    ##  [61]  4.8  6.7  8.3  5.4  6.6  6.9  4.9  5.2  6.1  5.6  6.2  5.4  7.5  5.1  5.0
    ##  [76]  5.2  5.5  5.2  5.6  5.9  5.2  5.2  5.2  4.9  7.1  5.4  5.2  7.8  5.6  5.1
    ##  [91]  5.3  5.1  6.7  4.7  4.8  6.8  6.3  4.7 11.8  6.3  5.5  5.6  5.5  5.5  6.5
    ## [106]  4.9  4.9  5.6  6.8  5.4  5.7  5.8  5.7  5.9  5.6  5.0  5.5  5.5  5.1  5.5
    ## [121]  5.5  5.4  6.0  5.5  5.7  5.6  5.4  6.9  4.6  5.3  4.4  5.0  7.0  5.8  4.9
    ## [136]  5.5  5.5  5.8  5.7  5.4  5.7  5.8  6.4  7.5  5.5  9.2  6.0  6.1  5.5  6.1
    ## [151]  4.7  5.0  8.5  5.9  5.6  5.4  6.0  6.3  5.6  5.5  5.7  6.0  7.8  7.7  5.8
    ## [166]  5.3  4.6  5.8  5.6  5.5  5.5  5.6  5.7  5.1  4.6  5.7  5.3  5.7  8.9  5.9
    ## [181]  5.7  5.4  6.7  7.0  5.4  6.0  5.3  5.7  5.5  6.6  5.4  5.1  5.6  6.8  7.0
    ## [196]  5.8  5.3  5.4  5.1  5.4  5.6  5.8  5.4  4.8  5.8  5.8  4.6  5.2  7.7  6.7
    ## [211]  5.2  5.2  5.4  4.8  4.7  5.7  5.2  5.9  5.7  4.9  6.0  5.3  6.4  5.6  5.6
    ## [226]  6.0  6.1  5.7  5.7  5.7  5.0  5.8  5.2  5.7  5.4  5.1  5.5  6.2  5.5  6.6
    ## 
    ## $HbA1cV2
    ##   [1]  7.4  5.8  6.0  4.5  7.2  5.7  7.2  6.6  8.4  6.1  5.9   NA  5.9  6.2  4.8
    ##  [16] 11.5  5.8  5.6  6.4   NA  6.1   NA  6.0  6.7  5.7   NA  6.2  5.7   NA  6.4
    ##  [31]   NA  5.9  4.6  6.7   NA  5.9   NA   NA  7.6  5.9  5.8  6.2  7.7  5.3   NA
    ##  [46]  5.7   NA  5.6  6.1  5.8   NA  5.6  6.3  6.9   NA  5.4   NA  6.0  5.3  5.6
    ##  [61]  5.6  5.7  7.4  5.4  8.4  7.3  5.4  5.6  6.1  5.9  6.3  5.7  7.2  5.3  5.3
    ##  [76]  4.7  7.2  5.6  6.0   NA  5.4  5.7  5.8  5.2  9.8  5.4  5.3  6.9  5.6  7.5
    ##  [91]  5.6  5.4  7.1  5.5  5.4  6.3  6.4  5.2 13.7   NA  5.6  5.6   NA  5.8  6.0
    ## [106]  5.4  5.3  5.6  6.8  5.5  6.4  6.2  5.7   NA   NA  5.3  6.1  5.7  5.2  5.7
    ## [121]  5.4  5.6  6.1  6.6  6.3  6.0  5.7  7.7  5.0  5.6  4.5  5.9  7.4  6.0  5.1
    ## [136]  5.3  5.9  5.8   NA  5.7  5.7  5.6  5.8  7.8  6.1  9.5   NA  6.3  5.8  6.6
    ## [151]  4.8  5.8   NA  6.1   NA   NA  6.0  8.3   NA  5.6  5.8  5.8  8.3   NA  6.0
    ## [166]  5.5  4.7  5.9  5.5  5.5  5.8  5.8  5.9  5.8  5.3  6.7  5.9  5.6   NA  6.7
    ## [181]  5.6  5.5  7.9  8.1  5.4  5.8  6.4  6.1  5.5   NA  5.5  5.0  6.0  6.9  5.3
    ## [196]  5.6  5.6  5.5  5.4  5.7  5.9  6.3  5.7  4.8   NA   NA  4.5  5.5  7.8  7.0
    ## [211]   NA  5.3   NA  4.9  5.4  5.5  5.6  6.3  6.0   NA  6.1  5.6  6.2  6.0   NA
    ## [226]   NA  6.1  5.9  6.0  5.9  5.3  6.0  5.6  6.4  5.5  5.4  5.7  6.7  5.8  6.1
    ## 
    ## $HbA1cV3
    ##   [1]  8.2  6.1  5.9  4.7  7.4  5.9  8.2 10.2  8.5  6.4  6.4  5.9  6.2  6.3  5.4
    ##  [16]  6.5  6.1  5.6 10.1   NA  6.3   NA  6.1  5.5  5.8  5.6  6.2  5.9  6.8  6.5
    ##  [31]   NA  6.0  4.7  6.6  6.1  5.9   NA   NA  6.7  6.3  5.7  6.1 10.0  5.3   NA
    ##  [46]  5.5  7.1  5.6  6.0  5.7  6.3  5.7  6.1  7.6  5.0  5.6  9.0  6.5  5.1  5.7
    ##  [61]  5.8  6.0  7.2  5.6  6.8  6.2  5.3  5.9  6.3  5.9  6.2  6.0  7.9  5.7   NA
    ##  [76]  5.2  7.2  5.6  6.5  6.0  5.5  5.7  6.0  5.5  9.6  5.4  5.9  7.7  5.6  7.5
    ##  [91]  6.1  5.3  6.5  6.4  6.1  6.4  6.6  5.6  8.4  6.1  5.8  5.7   NA  5.6   NA
    ## [106]  5.1  5.3  5.8  6.4  5.3  7.0  6.0  5.9  6.4  6.1  5.4  6.2  6.2  5.2  5.8
    ## [121]  5.6  5.7  6.4  6.5  6.5  5.9  5.8  7.7  5.2  5.4  4.7  6.0  7.7  6.5  5.0
    ## [136]  5.3  5.9  5.4  6.0  5.4  5.8  5.8  6.0  7.8  6.3  7.9   NA  6.3  6.1  6.6
    ## [151]  5.0  5.6   NA  6.0  7.6   NA  6.2  8.7  5.5  5.8  6.1  6.3  9.6   NA   NA
    ## [166]  5.6  4.8  6.0  5.3  5.5  5.7  5.9  6.1  5.7  5.6  7.2   NA  5.9  6.7  6.4
    ## [181]  5.5  5.9 12.9  6.4  5.7  6.2  5.8  6.1  5.7   NA  5.4  5.1  6.1  6.6  6.2
    ## [196]  5.6  5.8  5.4  5.4  5.4  6.0  6.3  5.8  5.0  6.6  5.8  4.6  5.3  8.9  6.5
    ## [211]   NA  5.2  5.6  4.9  5.6  5.9  5.5  6.5  6.1  5.4  6.0  5.9  6.3  6.5   NA
    ## [226]   NA  6.4  6.2  6.2  6.2  5.5  6.1  5.8  6.5  6.0  5.5  5.8  6.8  6.0  6.2

``` r
lapply(A1Clist, meanSD)
```

    ## $HbA1cV1
    ## [1] "5.8 (1)"
    ## 
    ## $HbA1cV2
    ## [1] "6.1 (1.1)"
    ## 
    ## $HbA1cV3
    ## [1] "6.2 (1)"

``` r
sapply(A1Clist, meanSD)
```

    ##     HbA1cV1     HbA1cV2     HbA1cV3 
    ##   "5.8 (1)" "6.1 (1.1)"   "6.2 (1)"

``` r
# a form of matlines when the data are in long format and the number of obs
# for each individual are different
set.seed(1)
obs <- sample(1:5, 8, replace=T)  # random number of obs per person, n=8
out <- mapply(rnorm, obs)         # random response per person
times <- mapply(sample, 5, obs)   # random times when response occured
times <- lapply(times, sort)      # sort the times

plot(0, 0, type="n", xlim=range(unlist(times)), ylim=range(unlist(out)),
    xlab="Time", ylab="Response", cex.axis=1.2, cex.lab=1.2,
    main="Spaghetti Plot of Responses by Subject")
  mapply(lines, times, out, col=1:8, lwd=4, type="b", pch=paste(1:8),
      cex=1.2)
```

![](intro_to_r_files/figure-markdown_github/apply,%20options-1.png)

    ## [[1]]
    ## NULL
    ## 
    ## [[2]]
    ## NULL
    ## 
    ## [[3]]
    ## NULL
    ## 
    ## [[4]]
    ## NULL
    ## 
    ## [[5]]
    ## NULL
    ## 
    ## [[6]]
    ## NULL
    ## 
    ## [[7]]
    ## NULL
    ## 
    ## [[8]]
    ## NULL

``` r
#dev.off()

tapply(dat$HbA1cV1, dat$maleV1, meanSD)                    # by sex
```

    ##      Female        Male 
    ## "5.8 (0.9)" "5.9 (1.2)"

``` r
tapply(dat$HbA1cV1, dat$sbpV1c, meanSD)                    # by SBP status
```

    ## [93.6,140)  [140,174] 
    ##  "5.8 (1)"  "6 (1.2)"

``` r
tapply(dat$HbA1cV1, list(dat$maleV1, dat$sbpV1c), meanSD)  # by both
```

    ##        [93.6,140)  [140,174]  
    ## Female "5.7 (0.8)" "6.1 (1.2)"
    ## Male   "5.9 (1.2)" "5.9 (1.2)"

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# How to do in tidyverse
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---


dat %>% 
  group_by(maleV1) %>% 
  summarize(The.Mean=meanSD(HbA1cV1))
```

    ## # A tibble: 2 × 2
    ##   maleV1 The.Mean 
    ##   <chr>  <chr>    
    ## 1 Female 5.8 (0.9)
    ## 2 Male   5.9 (1.2)

``` r
dat %>% 
  group_by(sbpV1c) %>% 
  summarize(The.Mean=meanSD(HbA1cV1))
```

    ## # A tibble: 2 × 2
    ##   sbpV1c     The.Mean
    ##   <fct>      <chr>   
    ## 1 [93.6,140) 5.8 (1) 
    ## 2 [140,174]  6 (1.2)

``` r
dat %>% 
  group_by(maleV1,sbpV1c) %>% 
  summarize(The.Mean=meanSD(HbA1cV1))
```

    ## `summarise()` has grouped output by 'maleV1'. You can override using the
    ## `.groups` argument.

    ## # A tibble: 4 × 3
    ## # Groups:   maleV1 [2]
    ##   maleV1 sbpV1c     The.Mean 
    ##   <chr>  <fct>      <chr>    
    ## 1 Female [93.6,140) 5.7 (0.8)
    ## 2 Female [140,174]  6.1 (1.2)
    ## 3 Male   [93.6,140) 5.9 (1.2)
    ## 4 Male   [140,174]  5.9 (1.2)

``` r
data.frame(Response=unlist(out),
           Time=unlist(times),
           ID=rep(1:length(times),obs)) %>% 
  mutate(ID=as.character(ID)) %>% 
  ggplot(aes(x=Time,y=Response))+
  geom_text(aes(label=ID))+
  geom_line(aes(group=ID,col=ID),linetype="longdash")+
  ggtitle("Spaghetti Plot of Responses by Subject")+
  theme(legend.position = "none")
```

![](intro_to_r_files/figure-markdown_github/apply,%20options-2.png)

## Dataset Manipulation

``` r
dat$eGFRV2 <- NA

datl <- reshape(dat,
    varying=list(c("DaysFromV1V1", "DaysFromV1V2", "DaysFromV1V3"),
        c("ageV1", "ageV2", "ageV3"),
        c("waistV1", "waistV2", "waistV3"),
        c("sbpV1","sbpV2","sbpV3"),
        c("HbA1cV1", "HbA1cV2", "HbA1cV3"),
        c("eGFRV1", "eGFRV2", "eGFRV3")), 
    v.names=c("Days", "Age", "WaistCM", "SBP", "A1C", "eGFR"),
    timevar="Visit",
    idvar = "SUBJID",
    direction="long")


  datl <- datl[order(datl$SUBJID, datl$Visit),]  # sort by ID and visit
  rownames(datl) <- NULL  

# write a shorthand function to pull variable names with grep


datw <- reshape(datl,
    v.names=c("Days", "Age", "WaistCM", "SBP", "A1C", "eGFR"),
    timevar="Visit",
    idvar="SUBJID",
    direction="wide",
    sep="")

# Reorder the columns to match the order of dat
mygrep <- function(char){  sort(grep(char, names(datw), value=TRUE)) }
datw <- datw[,c("SUBJID", mygrep("Days"), "ARICV1", mygrep("Age"),
    "maleV1",  mygrep("WaistCM"), mygrep("SBP"), mygrep("A1C"), "eGFR1",
    "eGFR3", "eGFR2")]

head(datw,3)
```

    ##   SUBJID Days1 Days2 Days3      ARICV1 Age1 Age2 Age3 maleV1 WaistCM1 WaistCM2
    ## 1  J0001     0  1730  2981 Shared ARIC 58.8 63.5 66.9   Male       94    91.44
    ## 4  J0002     0  1537  2569    JHS-Only 71.5 75.7 78.5 Female       94    92.71
    ## 7  J0003     0  1526  2739 Shared ARIC 73.1 77.3 80.6   Male      114   106.68
    ##   WaistCM3   SBP1   SBP2 SBP3 A1C1 A1C2 A1C3  eGFR1 eGFR3 eGFR2
    ## 1    97.79 110.99 109.16  116  8.3  7.4  8.2 102.16 75.13    NA
    ## 4    99.06 159.59 121.00  158  5.7  5.8  6.1  92.67 79.14    NA
    ## 7   101.70 132.08 122.00  130  6.0  6.0  5.9  92.36 98.37    NA

``` r
for(ii in 1:ncol(datw)){  # identical to dat?
    print(identical(dat[,ii], datw[,ii]))
}
```

    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] TRUE
    ## [1] FALSE

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# With Tidyverse pivot longer
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

Datl.tidy<-dat %>% 
  pivot_longer(cols=-c("SUBJID",
                       "ARICV1",
                       "maleV1",
                       "sbpV1c",
                       "sbpV1c2"),
               names_to = c(".value", "Visit"),
               names_pattern = "(.*)V(.*)") 

all_equal(
  Datl.tidy %>%
    rename(
      Days = DaysFromV1,
      Age = age,
      SBP = sbp,
      A1C = HbA1c,
      WaistCM = waist
    ) %>%
    mutate(Visit = as.integer(Visit)),
  datl
)
```

    ## [1] TRUE

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Tidyverse pivot wider
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

datw.tidy<-Datl.tidy %>% 
  pivot_wider(values_from=-c(SUBJID,
                             ARICV1,
                             maleV1,sbpV1c2,
                             sbpV1c,Visit),
              names_from=Visit,
              names_sep = "V")

all_equal(datw.tidy,dat)
```

    ## Different types for column `eGFRV2`: double vs logical.

## OLS regression

``` r
lmout <- lm(formula=HbA1cV1~ageV1+maleV1+I(waistV1/2.54)+sbpV1, data=dat)
names(lmout)
```

    ##  [1] "coefficients"  "residuals"     "effects"       "rank"         
    ##  [5] "fitted.values" "assign"        "qr"            "df.residual"  
    ##  [9] "na.action"     "contrasts"     "xlevels"       "call"         
    ## [13] "terms"         "model"

``` r
summary(lmout)
```

    ## 
    ## Call:
    ## lm(formula = HbA1cV1 ~ ageV1 + maleV1 + I(waistV1/2.54) + sbpV1, 
    ##     data = dat)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -2.1232 -0.5031 -0.2032  0.2485  5.4937 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.989919   0.651082   4.592 7.16e-06 ***
    ## ageV1           0.019380   0.005529   3.506 0.000546 ***
    ## maleV1Male      0.135666   0.130166   1.042 0.298368    
    ## I(waistV1/2.54) 0.032292   0.010333   3.125 0.002001 ** 
    ## sbpV1           0.003892   0.004553   0.855 0.393565    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.9769 on 234 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.1081, Adjusted R-squared:  0.0929 
    ## F-statistic: 7.093 on 4 and 234 DF,  p-value: 2.089e-05

``` r
names(summary(lmout))
```

    ##  [1] "call"          "terms"         "residuals"     "coefficients" 
    ##  [5] "aliased"       "sigma"         "df"            "r.squared"    
    ##  [9] "adj.r.squared" "fstatistic"    "cov.unscaled"  "na.action"

``` r
lmout <- lm(formula=A1C~Age+maleV1+
              I(WaistCM/2.54)+
              SBP, data=datl,
            subset=(Visit==1))

summary(lmout)
```

    ## 
    ## Call:
    ## lm(formula = A1C ~ Age + maleV1 + I(WaistCM/2.54) + SBP, data = datl, 
    ##     subset = (Visit == 1))
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -2.1232 -0.5031 -0.2032  0.2485  5.4937 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.989919   0.651082   4.592 7.16e-06 ***
    ## Age             0.019380   0.005529   3.506 0.000546 ***
    ## maleV1Male      0.135666   0.130166   1.042 0.298368    
    ## I(WaistCM/2.54) 0.032292   0.010333   3.125 0.002001 ** 
    ## SBP             0.003892   0.004553   0.855 0.393565    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.9769 on 234 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.1081, Adjusted R-squared:  0.0929 
    ## F-statistic: 7.093 on 4 and 234 DF,  p-value: 2.089e-05

``` r
# Naive approach for cross sectional analyses

lmlist <- vector("list", 3)  # initialize empty list to store results

  names(lmlist) <- paste0("Visit", 1:3)  # name the elements

for(ii in 1:3){
    lmlist[[ii]] <- lm(formula=A1C~Age+maleV1+I(WaistCM/2.54)+SBP,
        data=datl, subset=(Visit==ii))
    print(summary(lmlist[[ii]]))
}
```

    ## 
    ## Call:
    ## lm(formula = A1C ~ Age + maleV1 + I(WaistCM/2.54) + SBP, data = datl, 
    ##     subset = (Visit == ii))
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -2.1232 -0.5031 -0.2032  0.2485  5.4937 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.989919   0.651082   4.592 7.16e-06 ***
    ## Age             0.019380   0.005529   3.506 0.000546 ***
    ## maleV1Male      0.135666   0.130166   1.042 0.298368    
    ## I(WaistCM/2.54) 0.032292   0.010333   3.125 0.002001 ** 
    ## SBP             0.003892   0.004553   0.855 0.393565    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.9769 on 234 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.1081, Adjusted R-squared:  0.0929 
    ## F-statistic: 7.093 on 4 and 234 DF,  p-value: 2.089e-05
    ## 
    ## 
    ## Call:
    ## lm(formula = A1C ~ Age + maleV1 + I(WaistCM/2.54) + SBP, data = datl, 
    ##     subset = (Visit == ii))
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -1.7597 -0.4985 -0.2036  0.1865  7.1296 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.819788   0.774848   3.639 0.000348 ***
    ## Age             0.011344   0.006324   1.794 0.074343 .  
    ## maleV1Male      0.078605   0.148299   0.530 0.596670    
    ## I(WaistCM/2.54) 0.046628   0.012480   3.736 0.000244 ***
    ## SBP             0.005449   0.004418   1.233 0.218864    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 1.018 on 200 degrees of freedom
    ##   (35 observations deleted due to missingness)
    ## Multiple R-squared:  0.09242,    Adjusted R-squared:  0.07427 
    ## F-statistic: 5.091 on 4 and 200 DF,  p-value: 0.0006294
    ## 
    ## 
    ## Call:
    ## lm(formula = A1C ~ Age + maleV1 + I(WaistCM/2.54) + SBP, data = datl, 
    ##     subset = (Visit == ii))
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -1.9816 -0.5578 -0.2102  0.2569  6.3393 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     3.080504   0.779815   3.950 0.000106 ***
    ## Age             0.010623   0.005993   1.773 0.077718 .  
    ## maleV1Male      0.125093   0.140502   0.890 0.374288    
    ## I(WaistCM/2.54) 0.053472   0.011653   4.589 7.59e-06 ***
    ## SBP             0.001764   0.004436   0.398 0.691363    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 1.005 on 214 degrees of freedom
    ##   (21 observations deleted due to missingness)
    ## Multiple R-squared:  0.1032, Adjusted R-squared:  0.08644 
    ## F-statistic: 6.157 on 4 and 214 DF,  p-value: 0.0001045

``` r
# Use ?by instead
lmlist <- by(data=datl, INDICES=datl$Visit, FUN=lm,
    formula=A1C~Age+maleV1+I(WaistCM/2.54)+SBP)
lmlist
```

    ## datl$Visit: 1
    ## 
    ## Call:
    ## FUN(formula = ..1, data = data[x, , drop = FALSE])
    ## 
    ## Coefficients:
    ##     (Intercept)              Age       maleV1Male  I(WaistCM/2.54)  
    ##        2.989919         0.019380         0.135666         0.032292  
    ##             SBP  
    ##        0.003892  
    ## 
    ## ------------------------------------------------------------ 
    ## datl$Visit: 2
    ## 
    ## Call:
    ## FUN(formula = ..1, data = data[x, , drop = FALSE])
    ## 
    ## Coefficients:
    ##     (Intercept)              Age       maleV1Male  I(WaistCM/2.54)  
    ##        2.819788         0.011344         0.078605         0.046628  
    ##             SBP  
    ##        0.005449  
    ## 
    ## ------------------------------------------------------------ 
    ## datl$Visit: 3
    ## 
    ## Call:
    ## FUN(formula = ..1, data = data[x, , drop = FALSE])
    ## 
    ## Coefficients:
    ##     (Intercept)              Age       maleV1Male  I(WaistCM/2.54)  
    ##        3.080504         0.010623         0.125093         0.053472  
    ##             SBP  
    ##        0.001764

``` r
lapply(lmlist, summary)
```

    ## $`1`
    ## 
    ## Call:
    ## FUN(formula = ..1, data = data[x, , drop = FALSE])
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -2.1232 -0.5031 -0.2032  0.2485  5.4937 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.989919   0.651082   4.592 7.16e-06 ***
    ## Age             0.019380   0.005529   3.506 0.000546 ***
    ## maleV1Male      0.135666   0.130166   1.042 0.298368    
    ## I(WaistCM/2.54) 0.032292   0.010333   3.125 0.002001 ** 
    ## SBP             0.003892   0.004553   0.855 0.393565    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.9769 on 234 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.1081, Adjusted R-squared:  0.0929 
    ## F-statistic: 7.093 on 4 and 234 DF,  p-value: 2.089e-05
    ## 
    ## 
    ## $`2`
    ## 
    ## Call:
    ## FUN(formula = ..1, data = data[x, , drop = FALSE])
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -1.7597 -0.4985 -0.2036  0.1865  7.1296 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.819788   0.774848   3.639 0.000348 ***
    ## Age             0.011344   0.006324   1.794 0.074343 .  
    ## maleV1Male      0.078605   0.148299   0.530 0.596670    
    ## I(WaistCM/2.54) 0.046628   0.012480   3.736 0.000244 ***
    ## SBP             0.005449   0.004418   1.233 0.218864    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 1.018 on 200 degrees of freedom
    ##   (35 observations deleted due to missingness)
    ## Multiple R-squared:  0.09242,    Adjusted R-squared:  0.07427 
    ## F-statistic: 5.091 on 4 and 200 DF,  p-value: 0.0006294
    ## 
    ## 
    ## $`3`
    ## 
    ## Call:
    ## FUN(formula = ..1, data = data[x, , drop = FALSE])
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -1.9816 -0.5578 -0.2102  0.2569  6.3393 
    ## 
    ## Coefficients:
    ##                 Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     3.080504   0.779815   3.950 0.000106 ***
    ## Age             0.010623   0.005993   1.773 0.077718 .  
    ## maleV1Male      0.125093   0.140502   0.890 0.374288    
    ## I(WaistCM/2.54) 0.053472   0.011653   4.589 7.59e-06 ***
    ## SBP             0.001764   0.004436   0.398 0.691363    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 1.005 on 214 degrees of freedom
    ##   (21 observations deleted due to missingness)
    ## Multiple R-squared:  0.1032, Adjusted R-squared:  0.08644 
    ## F-statistic: 6.157 on 4 and 214 DF,  p-value: 0.0001045

``` r
sapply(lmlist, coef)
```

    ##                           1           2           3
    ## (Intercept)     2.989919202 2.819788092 3.080503649
    ## Age             0.019380391 0.011344499 0.010622944
    ## maleV1Male      0.135666218 0.078604846 0.125093025
    ## I(WaistCM/2.54) 0.032291697 0.046627933 0.053471790
    ## SBP             0.003891677 0.005449485 0.001763547

``` r
#  compute and plot the 95% CIs for waist at each visit
Summlm <- function(obj){  # obj is an object resulting from ?lm
    tbl <- data.frame(coef(summary(obj)))
    colnames(tbl)[-1] <- c("SE", "t value", "Pr(>|t|)")

    CIs <- round(data.frame(tbl[,1], confint.default(obj)),4)
    colnames(CIs) <- c("Estimate", "2.5%", "97.5%")

    as.matrix(cbind(CIs, round(tbl[,-1],3)))
}
Estlist <- lapply(lmlist, Summlm) # list of all the estimates


WaistRslt <- t(sapply(Estlist,
    function(x){ x[grep("WaistCM", rownames(x)),] } ))
WaistRslt 
```

    ##   Estimate   2.5%  97.5%    SE t value Pr(>|t|)
    ## 1   0.0323 0.0120 0.0525 0.010   3.125    0.002
    ## 2   0.0466 0.0222 0.0711 0.012   3.736    0.000
    ## 3   0.0535 0.0306 0.0763 0.012   4.589    0.000

``` r
plot(0, type="n", cex.lab=1.3, cex.axis=1.3, cex.main=1.5, 
    ylab="Estimate, 95%CI", xlab="", main="Waist CM Estimate by Visit",
    ylim=c(0,0.08), xlim=c(0.5,3.5), xaxt="n")
  axis(1, 1:3, labels=paste("Visit", 1:3), las=1, cex.axis=1.3) 
  segments(y0=WaistRslt[,2], x0=1:3, y1=WaistRslt[,3], lwd=3)
  points(1:3, WaistRslt[,1], cex=2, pch=16)
  abline(h=0, col="blue", lwd=3, lty=3)
```

![](intro_to_r_files/figure-markdown_github/old,%20options-1.png)

``` r
# Check model diagnostics!
lmout <- lm(formula=HbA1cV1~ageV1+maleV1+I(waistV1/2.54)+sbpV1, data=dat)
plot(lmout)
```

![](intro_to_r_files/figure-markdown_github/old,%20options-2.png)![](intro_to_r_files/figure-markdown_github/old,%20options-3.png)![](intro_to_r_files/figure-markdown_github/old,%20options-4.png)![](intro_to_r_files/figure-markdown_github/old,%20options-5.png)

# More Tidyverse and some regression

Now let’s work on some examples of Tidyverse using the mpg dataset so we
can compare from last weeks session.

``` r
mpg.base<-mpg

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Create the mpg.vol variable also set
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

      mpg$vol.per.cyl <- mpg$displ / mpg$cyl

      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      # In tidyverse
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

      mpg.base <- mpg.base %>%
        mutate(vol.per.cyl=displ/cyl)
```

## Mean of certain variables

Since we are in tidyverse, will be using the pipe command (`%>%`)

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Mean of the values 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---      

      mpg.mat <- as.matrix(mpg.base[, c("displ", "cyl",
                                        "cty", "hwy",
                                        "vol.per.cyl")])

      apply(mpg.mat, MARGIN = 2, FUN = function(x) mean(x))
```

    ##       displ         cyl         cty         hwy vol.per.cyl 
    ##   3.4717949   5.8888889  16.8589744  23.4401709   0.5779736

``` r
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      # In tidyverse or with pipe command 
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      
      mpg.base %>%
        select(c("displ", "cyl", "cty", 
                            "hwy", "vol.per.cyl")) %>% 
        summarize_all(mean)
```

    ## # A tibble: 1 × 5
    ##   displ   cyl   cty   hwy vol.per.cyl
    ##   <dbl> <dbl> <dbl> <dbl>       <dbl>
    ## 1  3.47  5.89  16.9  23.4       0.578

``` r
      mpg.base %>% 
        select(c("displ", "cyl", "cty", 
                 "hwy", "vol.per.cyl")) %>% 
        colMeans()
```

    ##       displ         cyl         cty         hwy vol.per.cyl 
    ##   3.4717949   5.8888889  16.8589744  23.4401709   0.5779736

``` r
      mpg.base %>% 
        select(c("displ", "cyl", "cty",
                 "hwy", "vol.per.cyl")) %>% 
        apply(.,2,mean)
```

    ##       displ         cyl         cty         hwy vol.per.cyl 
    ##   3.4717949   5.8888889  16.8589744  23.4401709   0.5779736

``` r
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      # Using matrices
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      
      O.xmat<-model.matrix(mpg.base[["displ"]]~1)
      
      Y.mat<-mpg.base[,c("displ", "cyl", "cty",
                         "hwy", "vol.per.cyl")]
      
      solve(crossprod(O.xmat)) %*% t(O.xmat) %*%
        as.matrix(Y.mat)
```

    ##                displ      cyl      cty      hwy vol.per.cyl
    ## (Intercept) 3.471795 5.888889 16.85897 23.44017   0.5779736

``` r
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      # Benchmark
      #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      
      benchmark(`Tidyverse summarize`=mpg.base %>% 
                  select(c("displ", "cyl",
                           "cty", "hwy", "vol.per.cyl")) %>%
                  summarize_all(mean),
                `Tidyverse Colmeans`=mpg.base %>% 
                  select(c("displ", "cyl",
                           "cty", "hwy", "vol.per.cyl")) %>% 
                  colMeans(),
                `Apply`=mpg.base %>% 
                  select(c("displ", "cyl",
                           "cty", "hwy", "vol.per.cyl")) %>% 
                  apply(.,2,mean),                
                `Colmeans`=colMeans(mpg.base[,c("displ", "cyl", 
                                                "cty", "hwy",
                                                "vol.per.cyl")]),
                `Matrices`=solve(crossprod(O.xmat)) %*% t(O.xmat) %*%
                  as.matrix(Y.mat),
          columns=c('test', 'elapsed', 'replications'),
          replications=1000)
```

    ##                  test elapsed replications
    ## 3               Apply   2.923         1000
    ## 4            Colmeans   0.079         1000
    ## 5            Matrices   0.067         1000
    ## 2  Tidyverse Colmeans   2.921         1000
    ## 1 Tidyverse summarize   8.331         1000

``` r
      microbenchmark::microbenchmark(
                `Tidyverse Summarize`=mpg.base %>% 
                  select(c("displ", "cyl", 
                           "cty", "hwy", "vol.per.cyl")) %>%
                  summarize_all(mean),
                `Tidyverse ColMeans`=mpg.base %>% 
                  select(c("displ", "cyl",
                           "cty", "hwy", "vol.per.cyl")) %>% 
                  colMeans(),
                `Apply`=mpg.base %>% 
                  select(c("displ", "cyl",
                           "cty", "hwy", "vol.per.cyl")) %>% 
                  apply(.,2,mean), 
                `Colmeans`=colMeans(mpg.base[,c("displ", 
                                                "cyl",
                                                "cty",
                                                "hwy",
                                          "vol.per.cyl")]),
                `Matrices`=solve(crossprod(O.xmat)) %*%
                  t(O.xmat) %*%
                  as.matrix(Y.mat),
                times=1000L,unit="s") 
```

    ## Unit: seconds
    ##                 expr         min          lq         mean       median
    ##  Tidyverse Summarize 0.007531940 0.007921398 0.0084330985 0.0080391415
    ##   Tidyverse ColMeans 0.002585278 0.002745049 0.0028466605 0.0027943260
    ##                Apply 0.002665303 0.002807059 0.0029170027 0.0028576765
    ##             Colmeans 0.000071620 0.000102781 0.0001165760 0.0001124820
    ##             Matrices 0.000060985 0.000096387 0.0001074321 0.0001070005
    ##            uq         max neval
    ##  0.0082218465 0.238283012  1000
    ##  0.0028604095 0.008946036  1000
    ##  0.0029166775 0.008948879  1000
    ##  0.0001295775 0.000269615  1000
    ##  0.0001213100 0.000222832  1000

## Average difference between auto and manual city by year.

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Average difference between auto and manual city by year 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---            
      sapply(unique(mpg$year), FUN = function(x) {
  
            #1st temporary subset of the data 
            #consisting only of the xth unique year
            temp.dat <- mpg[mpg$year == x ,]
            
            #Next, calculate the diff in city MPG 
            #between manual and automatic subsetting with grep
            mpg.diff <- mean(temp.dat$cty[grep("manual", 
                                               temp.dat$trans)],
                             na.rm = T) -
                        mean(temp.dat$cty[grep("auto", temp.dat$trans)],
                             na.rm = T)
            
            #Output the year (x) as an integer
            # and the MPG difference we calculated as columns
            return(cbind(as.integer(x), mpg.diff))
      })
```

    ##             [,1]        [,2]
    ## [1,] 1999.000000 2008.000000
    ## [2,]    2.914519    2.453225

``` r
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Tidyverse
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---      
      
      mpg %>% 
        mutate(manual=str_split_fixed(trans,
                                      "[[:punct:]]",
                                      2)[,1]) %>% 
        group_by(year,manual) %>% 
        summarize(MeanCity=mean(cty),.groups="drop") %>% 
        group_by(year) %>% 
        summarize(mpg.diff=diff(MeanCity))
```

    ## # A tibble: 2 × 2
    ##    year mpg.diff
    ##   <int>    <dbl>
    ## 1  1999     2.91
    ## 2  2008     2.45

``` r
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Matrices or LM
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
      mpg.for.reg<-mpg %>% 
        mutate(manual=str_split_fixed(trans,
                                      "[[:punct:]]",
                                      2)[,1]) %>% 
        mutate(A.1999=ifelse(year==1999,1,0),
               A.2008=1-A.1999,
               A.1999.manual=A.1999*(manual=="manual")*1L,
               A.2008.manual=A.2008*(manual=="manual")*1L)
      
      mpg.for.reg %>% 
        select(A.1999,A.2008,
               A.1999.manual,A.2008.manual) %>% 
        head()
```

    ## # A tibble: 6 × 4
    ##   A.1999 A.2008 A.1999.manual A.2008.manual
    ##    <dbl>  <dbl>         <dbl>         <dbl>
    ## 1      1      0             0             0
    ## 2      1      0             1             0
    ## 3      0      1             0             1
    ## 4      0      1             0             0
    ## 5      1      0             0             0
    ## 6      1      0             1             0

``` r
     #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
     # Interaction between year and manual yes no
     #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---    
      
     lm(cty~as.character(year)*manual-1,
         data=mpg.for.reg)
```

    ## 
    ## Call:
    ## lm(formula = cty ~ as.character(year) * manual - 1, data = mpg.for.reg)
    ## 
    ## Coefficients:
    ##              as.character(year)1999               as.character(year)2008  
    ##                             15.9459                              15.9880  
    ##                        manualmanual  as.character(year)2008:manualmanual  
    ##                              2.9145                              -0.4613

``` r
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Using the constructed variables we made 
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
     
     lm(cty~-1+A.1999+A.2008+A.1999.manual+A.2008.manual,
         data=mpg.for.reg)
```

    ## 
    ## Call:
    ## lm(formula = cty ~ -1 + A.1999 + A.2008 + A.1999.manual + A.2008.manual, 
    ##     data = mpg.for.reg)
    ## 
    ## Coefficients:
    ##        A.1999         A.2008  A.1999.manual  A.2008.manual  
    ##        15.946         15.988          2.915          2.453

``` r
     #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
     # Using matrices 
     #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
     
     X<-mpg.for.reg %>% 
       select(A.1999,A.2008,
              A.1999.manual,A.2008.manual) %>% as.matrix()
     
     solve(crossprod(X)) %*% crossprod(X,mpg.for.reg$cty)
```

    ##                    [,1]
    ## A.1999        15.945946
    ## A.2008        15.987952
    ## A.1999.manual  2.914519
    ## A.2008.manual  2.453225

Tidyverse is by far not the fastest, but often times for the type of
data we work with, that difference is not noticeable and what we would
lose say in time we maybe gain in readiblity of the code.

Next let’s compare the time over this.

``` r
      benchmark(`Sapply`= sapply(unique(mpg$year), 
                                 FUN = function(x) {
                                   temp.dat <- mpg[mpg$year == x ,]
                                   mpg.diff <- mean(temp.dat$cty[grep("manual",
                                                                      temp.dat$trans)], 
                                             na.rm = T) -
                                     mean(temp.dat$cty[grep("auto", temp.dat$trans)], 
                                          na.rm = T)
                                   return(cbind(as.integer(x), mpg.diff))
                                   }),
                `Tidyvese Summarize`=  mpg %>%   
                  mutate(manual=str_split_fixed(trans,"[[:punct:]]",2)[,1]) %>%
                                       group_by(year,manual) %>% 
                                       summarize(MeanCity=mean(cty),
                                                .groups="drop") %>% 
                                       group_by(year) %>% 
                                       summarize(mpg.diff=diff(MeanCity)),
                `Tidyverse`= mpg.for.reg %>% 
                             group_by(year,manual) %>% 
                             summarize(MeanCity=mean(cty),
                                      .groups="drop") %>% 
                             group_by(year) %>% 
                             summarize(mpg.diff=diff(MeanCity)),
                `LM` = lm(cty~as.character(year)*manual-1,data=mpg.for.reg),
                `LM2`= lm(cty~-1+A.1999+A.2008+A.1999.manual+A.2008.manual,
                         data=mpg.for.reg),
                `Matrices`=solve(crossprod(X)) %*% crossprod(X,mpg.for.reg$cty),
          columns=c('test', 'elapsed', 'replications'),
          replications=1000)
```

    ##                 test elapsed replications
    ## 4                 LM   1.274         1000
    ## 5                LM2   0.838         1000
    ## 6           Matrices   0.025         1000
    ## 1             Sapply   0.388         1000
    ## 3          Tidyverse   8.166         1000
    ## 2 Tidyvese Summarize  10.807         1000

## Test for differences in means of different columns via ANOVA

Now we could have done this within each of the child documents, but
where’s the fun in that. We’ll run an ANOVA for each of these columns
with Species via a few different approaches. Now normally, you would
perhaps check normality of these variables but for the sake of this
example we will skip those steps.

### Via Tidyverse and pivoting

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# With Tidyverse summarize, pivoted.
# 
# First what does pivot look like. 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

iris %>%  
      tidyr::pivot_longer(cols=c(-Species),
                          names_to="Variable") 
```

    ## # A tibble: 600 × 3
    ##    Species Variable     value
    ##    <fct>   <chr>        <dbl>
    ##  1 setosa  Sepal.Length   5.1
    ##  2 setosa  Sepal.Width    3.5
    ##  3 setosa  Petal.Length   1.4
    ##  4 setosa  Petal.Width    0.2
    ##  5 setosa  Sepal.Length   4.9
    ##  6 setosa  Sepal.Width    3  
    ##  7 setosa  Petal.Length   1.4
    ##  8 setosa  Petal.Width    0.2
    ##  9 setosa  Sepal.Length   4.7
    ## 10 setosa  Sepal.Width    3.2
    ## # … with 590 more rows

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Next run the summarize. 
# Group by: tells tidyverse to consider each of the rows with a shared value of Variable as a group
# Summarize will then summarize within each of those groups via a function you specify. 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

    Pval.Tidy<-iris %>%  
      tidyr::pivot_longer(cols=c(-Species),
                          names_to="Variable") %>% 
      dplyr::group_by(Variable) %>% 
      dplyr::summarize(Pvalue.tidy=anova(lm(value~Species))[1,5],
                       .groups="drop")

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# If hadn't specified [1,5]
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    iris %>%  
      tidyr::pivot_longer(cols=c(-Species),names_to="Variable") %>% 
      dplyr::group_by(Variable) %>% 
      dplyr::summarize(Pvalue.tidy=anova(lm(value~Species)),.groups="drop") 
```

    ## # A tibble: 8 × 2
    ##   Variable     Pvalue.tidy$Df $`Sum Sq` $`Mean Sq` $`F value` $`Pr(>F)`
    ##   <chr>                 <int>     <dbl>      <dbl>      <dbl>     <dbl>
    ## 1 Petal.Length              2    437.     219.         1180.   2.86e-91
    ## 2 Petal.Length            147     27.2      0.185        NA   NA       
    ## 3 Petal.Width               2     80.4     40.2         960.   4.17e-85
    ## 4 Petal.Width             147      6.16     0.0419       NA   NA       
    ## 5 Sepal.Length              2     63.2     31.6         119.   1.67e-31
    ## 6 Sepal.Length            147     39.0      0.265        NA   NA       
    ## 7 Sepal.Width               2     11.3      5.67         49.2  4.49e-17
    ## 8 Sepal.Width             147     17.0      0.115        NA   NA

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# What goes on
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    iris %>%  
      tidyr::pivot_longer(cols=c(-Species),names_to="Variable") %>% 
      dplyr::group_by(Variable) %>% 
      dplyr::summarize(Pvalue.tidy=anova(lm(value~Species)),.groups="drop")
```

    ## # A tibble: 8 × 2
    ##   Variable     Pvalue.tidy$Df $`Sum Sq` $`Mean Sq` $`F value` $`Pr(>F)`
    ##   <chr>                 <int>     <dbl>      <dbl>      <dbl>     <dbl>
    ## 1 Petal.Length              2    437.     219.         1180.   2.86e-91
    ## 2 Petal.Length            147     27.2      0.185        NA   NA       
    ## 3 Petal.Width               2     80.4     40.2         960.   4.17e-85
    ## 4 Petal.Width             147      6.16     0.0419       NA   NA       
    ## 5 Sepal.Length              2     63.2     31.6         119.   1.67e-31
    ## 6 Sepal.Length            147     39.0      0.265        NA   NA       
    ## 7 Sepal.Width               2     11.3      5.67         49.2  4.49e-17
    ## 8 Sepal.Width             147     17.0      0.115        NA   NA

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# What goes on
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    iris %>%  
      tidyr::pivot_longer(cols=c(-Species),names_to="Variable") %>% 
      dplyr::group_by(Variable) %>% 
      dplyr::summarize(Pvalue.tidy=anova(lm(value~Species)),.groups="drop") %>% 
      unpack(Pvalue.tidy)
```

    ## # A tibble: 8 × 6
    ##   Variable        Df `Sum Sq` `Mean Sq` `F value`  `Pr(>F)`
    ##   <chr>        <int>    <dbl>     <dbl>     <dbl>     <dbl>
    ## 1 Petal.Length     2   437.    219.        1180.   2.86e-91
    ## 2 Petal.Length   147    27.2     0.185       NA   NA       
    ## 3 Petal.Width      2    80.4    40.2        960.   4.17e-85
    ## 4 Petal.Width    147     6.16    0.0419      NA   NA       
    ## 5 Sepal.Length     2    63.2    31.6        119.   1.67e-31
    ## 6 Sepal.Length   147    39.0     0.265       NA   NA       
    ## 7 Sepal.Width      2    11.3     5.67        49.2  4.49e-17
    ## 8 Sepal.Width    147    17.0     0.115       NA   NA

### Via Tidyverse and across

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# With Tidyverse summarize across. We did not have to pivot
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

Pval.across <- iris %>%
  dplyr::summarize(dplyr::across(!Species,
                                 ~ anova(lm(. ~ Species))[1, 5]))
```

### With apply or mapply.

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# With apply
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
 
      
    Pval.apply<-base::apply(iris[,1:4],2,
                            function(x) stats::anova(stats::lm(x~iris$Species))[1,5])
  
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# with mapply 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

    Pval.mapply<-mapply(function(x){
      
           stats::anova(stats::lm(x~iris$Species))[1,5]
                        },
           iris[,1:4])
```

### Via Linear algebra

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# With matrices
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
my.Reg.funk<-function(Y,full.mat,redu.mat){

    t.full.beta <- base::solve(base::crossprod(full.mat)) %*% t(full.mat) %*% Y
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Full model sum of residual squared errors
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
    t.full.sse <- base::colSums((Y - full.mat %*% t.full.beta)^2)
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Null Model residual sum of squares
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    t.null.ressq<-(Y-redu.mat %*% base::solve(crossprod(redu.mat)) %*% t(redu.mat) %*% Y)^2
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Null Model sum of residual squared errors.
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
    t.null.sse <- base::colSums(t.null.ressq)
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Degrees of freedom
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    t.dfl <- list(df  = base::nrow(full.mat) - base::ncol(full.mat),
                  df0 = base:: nrow(redu.mat) - base::ncol(redu.mat))
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Test statistics
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
    t.test.stats <- (t.null.sse - t.full.sse)/t.full.sse * (t.dfl$df/(t.dfl$df0 - t.dfl$df))
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Pvalues using the F distribution and the corresponding df
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    return(stats::pf(t.test.stats,
                     df1 = (t.dfl$df0 - t.dfl$df),
                     df2 = t.dfl$df,
                     lower.tail = FALSE
    ))
  
  
}

    Y <- base::as.matrix(iris[, 1:4])
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Full Design matrix
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
    Xmat.full <- stats::model.matrix( ~ Species, data = iris)
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Null Design Matrix
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
    Xmat.null <- stats::model.matrix(Y[, 1] ~ 1)
    
    
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    # Run this
    #--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    
    Pvalues.matrix <- my.Reg.funk(Y, Xmat.full, Xmat.null)
```

### Compare the values

``` r
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
# Compare to make sure all give us the same exact thing. 
#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---    
      
as.data.frame(Pvalues.matrix) %>% 
  tibble::rownames_to_column("Variable") %>% 
  dplyr::left_join(Pval.Tidy) %>% 
  dplyr::left_join(as.data.frame(Pval.mapply) %>%
                     tibble::rownames_to_column("Variable")) %>% 
  dplyr::left_join(as.data.frame(Pval.apply)  %>% 
                     tibble::rownames_to_column("Variable")) %>%
  dplyr::left_join(Pval.across  %>% t() %>% 
                     as.data.frame() %>% 
                     dplyr::rename_all(~"Pval.across") %>% 
                     tibble::rownames_to_column("Variable")) 
```

    ## Joining, by = "Variable"
    ## Joining, by = "Variable"
    ## Joining, by = "Variable"
    ## Joining, by = "Variable"

    ##       Variable Pvalues.matrix  Pvalue.tidy  Pval.mapply   Pval.apply
    ## 1 Sepal.Length   1.669669e-31 1.669669e-31 1.669669e-31 1.669669e-31
    ## 2  Sepal.Width   4.492017e-17 4.492017e-17 4.492017e-17 4.492017e-17
    ## 3 Petal.Length   2.856777e-91 2.856777e-91 2.856777e-91 2.856777e-91
    ## 4  Petal.Width   4.169446e-85 4.169446e-85 4.169446e-85 4.169446e-85
    ##    Pval.across
    ## 1 1.669669e-31
    ## 2 4.492017e-17
    ## 3 2.856777e-91
    ## 4 4.169446e-85

### Benchmark the times

``` r
      rbenchmark::benchmark(
                `Matrices`=my.Reg.funk(Y,Xmat.full,Xmat.null),
                `Mapply`=  mapply(function(x) anova(lm(x~iris$Species))[1,5],
                                  iris[,1:4]),
                `Apply` = apply(iris[,1:4],2,
                                function(x) anova(lm(x~iris$Species))[1,5]),
                `Tidyverse across`= iris %>% 
                  dplyr::summarize(dplyr::across(!Species,
                                   ~anova(lm(.~Species))[1,5])),
                `Tidyverse pivot`=iris %>%  
                      tidyr::pivot_longer(cols=c(-Species),
                                          names_to="Variable") %>% 
                      dplyr::group_by(Variable) %>% 
                      dplyr::summarize(Pvalue.tidy=anova(lm(value~Species))[1,5]),
          columns=c('test', 'elapsed', 'replications'),
          replications=1000)
```

    ##               test elapsed replications
    ## 3            Apply   6.494         1000
    ## 2           Mapply   6.305         1000
    ## 1         Matrices   0.233         1000
    ## 4 Tidyverse across   9.568         1000
    ## 5  Tidyverse pivot  14.083         1000

# Session Info

``` r
sessionInfo()
```

    ## R version 4.1.3 (2022-03-10)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 20.04.4 LTS
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] forcats_0.5.1        stringr_1.4.0        dplyr_1.0.8         
    ##  [4] purrr_0.3.4          readr_2.1.2          tidyr_1.2.0         
    ##  [7] tibble_3.1.6         tidyverse_1.3.1      Hmisc_4.6-0         
    ## [10] ggplot2_3.3.5        Formula_1.2-4        survival_3.2-13     
    ## [13] lattice_0.20-45      microbenchmark_1.4.9 rbenchmark_1.0.0    
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] httr_1.4.2          jsonlite_1.8.0      splines_4.1.3      
    ##  [4] modelr_0.1.8        assertthat_0.2.1    highr_0.9          
    ##  [7] latticeExtra_0.6-29 cellranger_1.1.0    yaml_2.3.5         
    ## [10] pillar_1.7.0        backports_1.4.1     glue_1.6.2         
    ## [13] digest_0.6.29       RColorBrewer_1.1-3  checkmate_2.0.0    
    ## [16] rvest_1.0.2         colorspace_2.0-3    htmltools_0.5.2    
    ## [19] Matrix_1.4-1        pkgconfig_2.0.3     broom_0.8.0        
    ## [22] haven_2.4.3         scales_1.2.0        jpeg_0.1-9         
    ## [25] tzdb_0.3.0          htmlTable_2.4.0     farver_2.1.0       
    ## [28] generics_0.1.2      ellipsis_0.3.2      withr_2.5.0        
    ## [31] nnet_7.3-17         cli_3.2.0           magrittr_2.0.3     
    ## [34] crayon_1.5.1        readxl_1.4.0        evaluate_0.15      
    ## [37] fs_1.5.2            fansi_1.0.3         xml2_1.3.3         
    ## [40] foreign_0.8-82      tools_4.1.3         data.table_1.14.2  
    ## [43] hms_1.1.1           lifecycle_1.0.1     munsell_0.5.0      
    ## [46] reprex_2.0.1        cluster_2.1.3       compiler_4.1.3     
    ## [49] rlang_1.0.2         grid_4.1.3          rstudioapi_0.13    
    ## [52] htmlwidgets_1.5.4   labeling_0.4.2      base64enc_0.1-3    
    ## [55] rmarkdown_2.13      gtable_0.3.0        DBI_1.1.2          
    ## [58] R6_2.5.1            gridExtra_2.3       lubridate_1.8.0    
    ## [61] knitr_1.38          fastmap_1.1.0       utf8_1.2.2         
    ## [64] stringi_1.7.6       vctrs_0.4.1         rpart_4.1.16       
    ## [67] png_0.1-7           dbplyr_2.1.1        tidyselect_1.1.2   
    ## [70] xfun_0.30
